/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/cyw43_arch.h"

#include "main.h"
#include "types.h"
#include "storage.h"
#include "rnd.h"

#include "picow_http/log.h"

struct pl_state {
	unsigned	magic;
#define PL_STATE_MAGIC (0x5801a2ed)
	uint16_t	dur[PS_PER_PL];
	uint8_t		ps[PS_PER_PL];
	uint16_t	prev_dur;
	uint8_t		current;
	uint8_t		repetitions;
};

static void pl_work(async_context_t *context, async_at_time_worker_t *worker);
static struct pl_state pl_state;
static async_at_time_worker_t pl_runner = {
	.do_work = pl_work,
	.user_data = &pl_state,
};

static inline void
shuffle(struct pl_state *state, uint8_t n)
{
	CHECK_OBJ_NOTNULL(state, PL_STATE_MAGIC);

	for (unsigned i = n - 1; i >= 1; i--) {
		uint8_t j = (uint8_t)rnd32_nxt_bounded(i);
		uint16_t tmp = state->ps[i];

		state->ps[i] = state->ps[j];
		state->ps[j] = (uint8_t)tmp;
		tmp = state->dur[i];
		state->dur[i] = state->dur[j];
		state->dur[j] = tmp;
	}
}

static void
pl_work(async_context_t *context, async_at_time_worker_t *worker)
{
	struct pl_state *state;
	global_t cfg;
	struct playlist *playlist;
	struct preset *preset;
	uint16_t duration;
	int ps_idx;
	bool reps_complete = false;

	CHECK_OBJ_NOTNULL(global_cfg, GLOBAL_MAGIC);
	if (global_cfg->playlist >= MAX_PLAYLISTS)
		return;

	AN(plmap);
	AN(psmap);
	AN(*plmap & (1ULL << global_cfg->playlist));
	CHECK_OBJ_NOTNULL(pl + global_cfg->playlist, PLAYLIST_MAGIC);
	playlist = pl + global_cfg->playlist;
	AN(playlist->nps);
	AN(context);
	AN(worker);
	CAST_OBJ_NOTNULL(state, worker->user_data, PL_STATE_MAGIC);
	PICOW_HTTP_ASSERT(state->current <= playlist->nps);

	for (uint8_t i = 0; (*psmap & (1ULL << ps_idx)) == 0;
	     state->current++, i++) {
		if (i == playlist->nps)
			/* All presets in the list are deleted, give up. */
			return;
		ps_idx = NO_PRESET;
		if (state->current == playlist->nps) {
			if (playlist->repeat == 0) {
				state->current = 0;
			}
			else if (playlist->repeat == state->repetitions) {
				if (playlist->end == NO_PLAYLIST)
					return;
				ps_idx = playlist->end;
				reps_complete = true;
			}
			state->repetitions++;
			if (playlist->shuffle)
				shuffle(state, playlist->nps);
		}

		if (ps_idx == NO_PRESET) {
			ps_idx = state->ps[state->current];
			duration = state->dur[state->current];
		}
		if (duration == 0)
			duration = state->prev_dur;
		if (duration == 0)
			duration = 1;
		state->prev_dur = duration;
	}

	HTTP_LOG_DEBUG("pl_runner: ps_idx=%u psmap=0x%016x", ps_idx, *psmap);
	AN(*psmap & (1ULL << ps_idx));
	CHECK_OBJ_NOTNULL(ps + ps_idx, PRESET_MAGIC);

	memcpy(&cfg, global_cfg, sizeof(cfg));
	CHECK_OBJ(&cfg, GLOBAL_MAGIC);
	cfg.preset = ps_idx;

	/* XXX locking? */
	store_global(&cfg);
	notify_cfg_change();

	if (reps_complete)
		return;
	async_context_add_at_time_worker_in_ms(context, worker, duration * 100);
}

void
pl_runner_start(void)
{
	struct playlist *playlist;

	CHECK_OBJ_NOTNULL(global_cfg, GLOBAL_MAGIC);
	if (global_cfg->playlist >= MAX_PLAYLISTS)
		return;

	AN(plmap);
	if ((*plmap & (1ULL << global_cfg->playlist)) == 0)
		return;
	CHECK_OBJ_NOTNULL(pl + global_cfg->playlist, PLAYLIST_MAGIC);
	playlist = pl + global_cfg->playlist;
	if (playlist->nps == 0)
		return;

	INIT_OBJ(&pl_state, PL_STATE_MAGIC);
	memcpy(pl_state.ps, playlist->ps, playlist->nps * sizeof(uint8_t));
	memcpy(pl_state.dur, playlist->dur, playlist->nps * sizeof(uint16_t));
	AZ(pl_state.current);
	AZ(pl_state.repetitions);
	if (playlist->shuffle)
		shuffle(&pl_state, playlist->nps);

	async_context_add_at_time_worker_in_ms(cyw43_arch_async_context(),
					       &pl_runner, 0);
}

void
pl_runner_stop(void)
{
	(void)async_context_remove_at_time_worker(cyw43_arch_async_context(),
						  &pl_runner);
}
