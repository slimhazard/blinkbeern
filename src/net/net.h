/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "picow_http/http.h"

struct app_cfg {
	unsigned	magic;
#define APP_CFG_MAGIC (0xdeb3719e)
	int		mode;
	bool		reset;
};

err_t network_get_hndlr(struct http *http, void *priv);
err_t network_put_hndlr(struct http *http, void *priv);

err_t global_get_hndlr(struct http *http, void *priv);
err_t global_put_hndlr(struct http *http, void *priv);

err_t segments_get_hndlr(struct http *http, void *priv);
err_t segments_post_put_hndlr(struct http *http, void *priv);
err_t segments_delete_hndlr(struct http *http, void *priv);

err_t presets_get_hndlr(struct http *http, void *priv);
err_t presets_post_put_hndlr(struct http *http, void *priv);
err_t presets_delete_hndlr(struct http *http, void *priv);

err_t playlists_get_hndlr(struct http *http, void *priv);
err_t playlists_post_put_hndlr(struct http *http, void *priv);
err_t playlists_delete_hndlr(struct http *http, void *priv);

void pl_runner_start(void);
void pl_runner_stop(void);
