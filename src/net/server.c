/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/cyw43_arch.h"
#include "pico/async_context_threadsafe_background.h"
#include "pico/multicore.h"
#include "hardware/exception.h"

#include "lwip/ip_addr.h"

#include "main.h"
#include "types.h"
#include "storage.h"

#include "net.h"
#include "dhcpserver.h"
#include "net/handlers/handlers.h"

/*
 * XXX configurable
 */
#define STA_TMO_MS (30 * 1000)

#define SHORT_PULSE_MS	(250)
#define LONG_PULSE_MS	(500)
#define XLONG_PULSE_MS	(1250)

/* Casts the generic async context to the specific type */
#define BG_CTX(ctx)	((async_context_threadsafe_background_t *)ctx)

enum indicator_state_t {
	CONNECT_HI,
	CONNECT_LO,
	ERROR_HI,
	ERROR_LO,
	AP_HI_1,
	AP_HI_2,
	AP_LO_1,
	AP_LO_2,
};

static void led_worker(async_context_t *context,
		       async_at_time_worker_t *worker);
static enum indicator_state_t indicator_state;
static async_at_time_worker_t led_indicator = {
	.do_work = led_worker,
	.user_data = &indicator_state,
};

static void cx_tmo(async_context_t *context, async_at_time_worker_t *worker);
static bool cx_timed_out = false;
static async_at_time_worker_t connect_timeout = {
	.do_work = cx_tmo,
	.user_data = &cx_timed_out,
};

static void
led_worker(async_context_t *context, async_at_time_worker_t *worker)
{
	enum indicator_state_t *state;
	uint32_t tmo_ms;
	bool on;

	AN(worker);
	state = (enum indicator_state_t *)worker->user_data;

	switch(*state) {
	case CONNECT_HI:
		tmo_ms = LONG_PULSE_MS;
		on = false;
		*state = CONNECT_LO;
		break;
	case CONNECT_LO:
		tmo_ms = LONG_PULSE_MS;
		on = true;
		*state = CONNECT_HI;
		break;
	case ERROR_HI:
		tmo_ms = SHORT_PULSE_MS;
		on = false;
		*state = ERROR_LO;
		break;
	case ERROR_LO:
		tmo_ms = SHORT_PULSE_MS;
		on = true;
		*state = ERROR_HI;
		break;
	case AP_HI_1:
		tmo_ms = SHORT_PULSE_MS;
		on = false;
		*state = AP_LO_1;
		break;
	case AP_LO_1:
		tmo_ms = SHORT_PULSE_MS;
		on = true;
		*state = AP_HI_2;
		break;
	case AP_HI_2:
		tmo_ms = XLONG_PULSE_MS;
		on = false;
		*state = AP_LO_2;
		break;
	case AP_LO_2:
		tmo_ms = SHORT_PULSE_MS;
		on = true;
		*state = AP_HI_1;
		break;
	default:
		HTTP_LOG_ERROR("Illegal LED indicator state: %d", *state);
		cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, false);
		return;
	}
	cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, on);
	async_context_add_at_time_worker_in_ms(context, worker, tmo_ms);
}

static void
cx_tmo(async_context_t *context, async_at_time_worker_t *worker)
{
	(void)context;
	(void)worker;
	cx_timed_out = true;
}

static void
indicate_error(async_context_t *ctx)
{
	AN(ctx);

	if (indicator_state == ERROR_HI || indicator_state == ERROR_LO)
		return;

	(void)async_context_remove_at_time_worker(ctx, &led_indicator);
	cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, true);
	indicator_state = ERROR_HI;
	async_context_add_at_time_worker_in_ms(ctx, &led_indicator,
					       SHORT_PULSE_MS);
}

static int
get_link_status(void)
{
	int status = CYW43_LINK_DOWN;

	if ((status = cyw43_tcpip_link_status(&cyw43_state, CYW43_ITF_STA))
	    != CYW43_LINK_UP)
		if (status < 0)
			HTTP_LOG_ERROR("WiFi connect error status: %d", status);
	return status;
}

static inline void
log_timeout(void)
{
	HTTP_LOG_ERROR("WiFi connection timeout (%d seconds)",
		       STA_TMO_MS / 1000);
	HTTP_LOG_ERROR("Falling back to AP mode");
}

static int
run(int mode)
{
	struct server *srv;
	struct server_cfg cfg;
	struct app_cfg app_cfg;
	struct netif *netif;
	async_context_t *ctx;
	ip_addr_t gw, mask;
	dhcp_server_t dhcp_srv;
	uint8_t mac[6];
	uint32_t country = CYW43_COUNTRY_WORLDWIDE;
	bool reset = false;
	int err;
	char ssid[SSID_MAX_LEN + 1];
	char pass[WIFI_PW_MAX_LEN + 1];

	PICOW_HTTP_ASSERT((mode == CYW43_ITF_STA) || (mode == CYW43_ITF_AP));

	INIT_OBJ(&app_cfg, APP_CFG_MAGIC);
	app_cfg.mode = mode;
	AZ(app_cfg.reset);

	if (mode == CYW43_ITF_STA) {
		CHECK_OBJ_NOTNULL(network_cfg, NETWORK_CFG_MAGIC);
		country = network_cfg->wifi.country;
	}
	if ((err = cyw43_arch_init_with_country(country)) != PICO_OK) {
		/* XXX error handling */
		HTTP_LOG_ERROR("Network initialization failed: %d", err);
		exit(err);
	}

	/* If a playlist is set, start it. */
	CHECK_OBJ_NOTNULL(global_cfg, GLOBAL_MAGIC);
	if (global_cfg->playlist != NO_PLAYLIST)
		pl_runner_start();

	if ((err = cyw43_wifi_pm(&cyw43_state, CYW43_PERFORMANCE_PM))
	    != PICO_OK)
		HTTP_LOG_ERROR("Cannot set performance power management mode: "
			       "%d", err);
	cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, true);

	ctx = cyw43_arch_async_context();
	if (mode == CYW43_ITF_AP) {
		indicator_state = AP_HI_1;
		async_context_add_at_time_worker_in_ms(ctx, &led_indicator,
						       SHORT_PULSE_MS);
		memcpy(ssid, network_cfg->ap.ssid, network_cfg->ap.ssid_len);
		ssid[network_cfg->ap.ssid_len] = '\0';
		memcpy(pass, network_cfg->ap.pass, network_cfg->ap.pass_len);
		pass[network_cfg->ap.pass_len] = '\0';

		/* XXX TBD WPA2 auth is hard-wired */
		cyw43_arch_enable_ap_mode(ssid, pass, CYW43_AUTH_WPA2_AES_PSK );

		/*
		 * The gateway address and mask are hard-wired, since
		 * these are used universally by access points.
		 */
		IP4_ADDR(&gw, 192, 168, 4, 1);
		IP4_ADDR(&mask, 255, 255, 255, 0);
		dhcp_server_init(&dhcp_srv, &gw, &mask);
		HTTP_LOG_INFO("AP %s enabled", ssid);
	}
	else {
		int link_status = CYW43_LINK_DOWN;

		indicator_state = CONNECT_HI;
		async_context_add_at_time_worker_in_ms(ctx, &led_indicator,
						       LONG_PULSE_MS);
		memcpy(ssid, network_cfg->wifi.ssid,
		       network_cfg->wifi.ssid_len);
		ssid[network_cfg->wifi.ssid_len] = '\0';
		memcpy(pass, network_cfg->wifi.pass,
		       network_cfg->wifi.pass_len);
		pass[network_cfg->wifi.pass_len] = '\0';

		cyw43_arch_enable_sta_mode();
		HTTP_LOG_INFO("Connecting to %s ...", ssid);
		async_context_add_at_time_worker_in_ms(ctx, &connect_timeout,
						       STA_TMO_MS);
		do {
			if ((err = cyw43_arch_wifi_connect_async(
				     ssid, pass,
				     network_cfg->wifi.auth)) != 0) {
				indicate_error(ctx);
				HTTP_LOG_ERROR("wifi connect error: %d", err);
				if (cx_timed_out) {
					log_timeout();
					reset_network_cfg();
					app_cfg.mode = CYW43_ITF_AP;
					goto deinit;
				}
				continue;
			}
			do {
				if ((link_status = get_link_status()) < 0) {
					indicate_error(ctx);
					break;
				}
				if (cx_timed_out) {
					log_timeout();
					reset_network_cfg();
					app_cfg.mode = CYW43_ITF_AP;
					goto deinit;
				}
				sleep_ms(100);
			} while (link_status != CYW43_LINK_UP);
		} while (link_status != CYW43_LINK_UP);
		(void)async_context_remove_at_time_worker(ctx,
							  &connect_timeout);
		HTTP_LOG_INFO("Connected to %s", ssid);
	}

	netif = &cyw43_state.netif[mode];
	HTTP_LOG_INFO("Network initialized, IP=%s",
		      ipaddr_ntoa(netif_ip4_addr(netif)));
	if ((err = cyw43_wifi_get_mac(&cyw43_state, CYW43_ITF_STA, mac))
	    == PICO_OK)
		HTTP_LOG_INFO("MAC address %02x:%02x:%02x:%02x:%02x:%02x",
			      mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	else
		HTTP_LOG_ERROR("Could not get mac address: %d", err);

	/* XXX set NTP server from config */
	cfg = http_default_cfg();
	/*
	 * Use the alarm pool from the threadsafe background context
	 * object, so that NTP timing won't have to claim an unused
	 * hardware timer.
	 */
	cfg.ntp_cfg.alarm_pool = BG_CTX(ctx)->alarm_pool;

	err = register_hndlr_methods(&cfg, "/v1/network", network_get_hndlr,
				     HTTP_METHODS_GET_HEAD, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr(&cfg, "/v1/network", network_put_hndlr,
				  HTTP_METHOD_PUT, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(&cfg, "/v1/global", global_get_hndlr,
				     HTTP_METHODS_GET_HEAD, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr(&cfg, "/v1/global", global_put_hndlr,
			     HTTP_METHOD_PUT, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(&cfg, "/v1/segments", segments_get_hndlr,
				     HTTP_METHODS_GET_HEAD, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(
		&cfg, "/v1/segments", segments_post_put_hndlr,
		((1U << HTTP_METHOD_PUT) | (1U << HTTP_METHOD_POST)), NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr(&cfg, "/v1/segments", segments_delete_hndlr,
			     HTTP_METHOD_DELETE, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(&cfg, "/v1/presets", presets_get_hndlr,
				     HTTP_METHODS_GET_HEAD, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(
		&cfg, "/v1/presets", presets_post_put_hndlr,
		((1U << HTTP_METHOD_PUT) | (1U << HTTP_METHOD_POST)), NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr(&cfg, "/v1/presets", presets_delete_hndlr,
			     HTTP_METHOD_DELETE, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(&cfg, "/v1/playlists", playlists_get_hndlr,
				     HTTP_METHODS_GET_HEAD, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr_methods(
		&cfg, "/v1/playlists", playlists_post_put_hndlr,
		((1U << HTTP_METHOD_PUT) | (1U << HTTP_METHOD_POST)), NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_hndlr(&cfg, "/v1/playlists", playlists_delete_hndlr,
			     HTTP_METHOD_DELETE, NULL);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	while ((err = http_srv_init(&srv, &cfg)) != ERR_OK)
		HTTP_LOG_ERROR("http_init: %d\n", err);
	HTTP_LOG_INFO("http listening at: %s:%u", ipaddr_ntoa(http_srv_ip(srv)),
		      http_srv_port(srv));
	if (mode == CYW43_ITF_STA) {
		(void)async_context_remove_at_time_worker(ctx, &led_indicator);
		cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, true);
	}
	http_srv_set_priv(srv, &app_cfg);

	do {
		__wfi();
		/* XXX check link status at intervals */
	} while (!app_cfg.reset);

	while ((err = http_srv_fini(srv)) != ERR_OK) {
		HTTP_LOG_ERROR("Server stop error: %d", err);
		/* XXX stop at a max error threshold */
		if (err != ERR_MEM)
			break;
	}

 deinit:
	(void)async_context_remove_at_time_worker(ctx, &led_indicator);
	cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, false);
	if (mode == CYW43_ITF_AP)
		dhcp_server_deinit(&dhcp_srv);
	if ((err = cyw43_wifi_leave(&cyw43_state, mode)) != PICO_OK)
		HTTP_LOG_ERROR("Error leaving wifi: %d");
	cyw43_arch_deinit();

	return app_cfg.mode;
}

void
core1_main(void)
{
	int mode = CYW43_ITF_STA;

	exception_set_exclusive_handler(HARDFAULT_EXCEPTION, on_hardfault);

	if (!storage_inited()) {
		init_storage();
		mode = CYW43_ITF_AP;
	}
	check_storage();
	/* Notify the other core that storage is initialized. */
	notify_storage_inited();

	LWIP_MEMPOOL_INIT(small_pool);
	LWIP_MEMPOOL_INIT(large_pool);
	LWIP_MEMPOOL_INIT(pl_body_pool);
	LWIP_MEMPOOL_INIT(pl_json_pool);

	if (mode == CYW43_ITF_STA && !wifi_inited())
		mode = CYW43_ITF_AP;

	for (;;)
		mode = run(mode);
}
