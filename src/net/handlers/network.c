/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/cyw43_arch.h"
#include "json-maker/json-maker.h"
#include "tiny-json.h"

#include "handlers.h"
#include "types.h"
#include "net.h"
#include "storage.h"

err_t
network_get_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	int auth;
	size_t len = SMALL_BUF_SZ;
	unsigned char *p, *buf, country[3] = { '\0' };

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_GET ||
			  http_req_method(req) == HTTP_METHOD_HEAD);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);
	CHECK_OBJ_NOTNULL(network_cfg, NETWORK_CFG_MAGIC);

	if (!http_req_hdr_contains_ltrl(req, "Accept", "application/json"))
		return http_resp_err(http, HTTP_STATUS_NOT_ACCEPTABLE);

	buf = LWIP_MEMPOOL_ALLOC(small_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Small pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = json_objOpen(p, NULL, &len);
	AN(len);
	p = json_objOpen(p, "wifi", &len);
	AN(len);
	p = json_nstr(p, "ssid", network_cfg->wifi.ssid,
		      network_cfg->wifi.ssid_len, &len);
	AN(len);
	country[0] = (char)(network_cfg->wifi.country & 0x0ff);
	country[1] = (char)((network_cfg->wifi.country & 0x0ff00) >> 8);
	p = json_str(p, "country", country, &len);
	AN(len);
	switch (network_cfg->wifi.auth) {
	case CYW43_AUTH_OPEN:
		auth = 0;
		break;
	case CYW43_AUTH_WPA_TKIP_PSK:
		auth = 1;
		break;
	case CYW43_AUTH_WPA2_AES_PSK:
		auth = 2;
		break;
	case CYW43_AUTH_WPA2_MIXED_PSK:
		auth = 3;
		break;
	}
	PICOW_HTTP_ASSERT(auth >= 0 && auth <= 3);
	p = json_int(p, "auth", auth, &len);
	AN(len);
	p = json_objClose(p, &len);
	AN(len);
	p = json_objOpen(p, "ap", &len);
	AN(len);
	p = json_nstr(p, "ssid", network_cfg->ap.ssid,
		      network_cfg->ap.ssid_len, &len);
	AN(len);
	p = json_objClose(p, &len);
	AN(len);
	p = json_objClose(p, &len);
	AN(len);
	p = json_end(p, &len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(small_pool, buf);
	return err;
}

static bool
parse_wifi(struct http *http, network_cfg_t *cfg, json_t const *j, err_t *err)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(cfg);
	AN(j);

	for (; j != NULL; j = json_getSibling(j)) {
		size_t len;
		int64_t num;
		char const *val, *name = json_getName(j);
		AN(name);

		switch (*name) {
		case 's':
			if (strcmp(name + 1, "sid") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			if (json_getType(j) != JSON_TEXT) {
				*err = illegal_type(http, "wifi.ssid");
				return false;
			}
			val = json_getValue(j);
			AN(val);
			len = strlen(val);
			if (len < SSID_MIN_LEN || len > SSID_MAX_LEN) {
				*err = out_of_range(http, "wifi.ssid length");
				return false;
			}
			memcpy(cfg->wifi.ssid, val, len);
			cfg->wifi.ssid_len = len;
			break;
		case 'p':
			if (strcmp(name + 1, "ass") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			if (json_getType(j) != JSON_TEXT) {
				*err = illegal_type(http, "wifi.pass");
				return false;
			}
			val = json_getValue(j);
			AN(val);
			len = strlen(val);
			if (len < WIFI_PW_MIN_LEN || len > WIFI_PW_MAX_LEN)
				return out_of_range(http, "wifi.pass length");
			memcpy(cfg->wifi.pass, val, len);
			cfg->wifi.pass_len = len;
			break;
		case 'c':
			if (strcmp(name + 1, "ountry") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			if (json_getType(j) != JSON_TEXT) {
				*err = illegal_type(http, "wifi.pass");
				return false;
			}
			val = json_getValue(j);
			AN(val);
			len = strlen(val);
			if (len != 2) {
				*err = out_of_range(
					http, "wifi.country length");
				return false;
			}
			cfg->wifi.country = CYW43_COUNTRY(val[0], val[1], 0);
			break;
		case 'a':
			if (strcmp(name + 1, "uth") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			if (json_getType(j) != JSON_INTEGER) {
				*err = illegal_type(http, "wifi.auth");
				return false;
			}
			num = json_getInteger(j);
			switch (num) {
			case 0:
				cfg->wifi.auth = CYW43_AUTH_OPEN;
				break;
			case 1:
				cfg->wifi.auth = CYW43_AUTH_WPA_TKIP_PSK;
				break;
			case 2:
				cfg->wifi.auth = CYW43_AUTH_WPA2_AES_PSK;
				break;
			case 3:
				cfg->wifi.auth = CYW43_AUTH_WPA2_MIXED_PSK;
				break;
			default:
				*err = unprocessable(http,
						     "wifi.auth illegal value");
				return false;
			}
			break;
		default:
			*err = unknown_fld(http, name);
			return false;
		}
	}
	return true;
}

static bool
parse_ap(struct http *http, network_cfg_t *cfg, json_t const *j, err_t *err)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(cfg);
	AN(j);

	for (; j != NULL; j = json_getSibling(j)) {
		size_t len;
		int64_t num;
		char const *val, *name = json_getName(j);
		AN(name);

		switch (*name) {
		case 's':
			if (strcmp(name + 1, "sid") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			if (json_getType(j) != JSON_TEXT)
				return illegal_type(http, "ap.ssid");
			val = json_getValue(j);
			AN(val);
			len = strlen(val);
			if (len < SSID_MIN_LEN || len > SSID_MAX_LEN) {
				*err = out_of_range(http, "ap.ssid length");
				return false;
			}
			memcpy(cfg->ap.ssid, val, len);
			cfg->ap.ssid_len = len;
			break;
		case 'p':
			if (strcmp(name + 1, "ass") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			if (json_getType(j) != JSON_TEXT) {
				*err = illegal_type(http, "ap.pass");
				return false;
			}
			val = json_getValue(j);
			AN(val);
			len = strlen(val);
			if (len < WIFI_PW_MIN_LEN || len > WIFI_PW_MAX_LEN) {
				*err = out_of_range(http, "ap.ssid length");
				return false;
			}
			memcpy(cfg->ap.pass, val, len);
			cfg->ap.pass_len = len;
			break;
		default:
			*err = unknown_fld(http, name);
			return false;
		}
	}
	return true;
}

#define NETWORK_MAX_FLDS (10)

err_t
network_put_hndlr(struct http *http, void *priv)
{
	network_cfg_t cfg;
	struct app_cfg *app_cfg;
	struct req *req;
	struct resp *resp;
	json_t flds[NETWORK_MAX_FLDS];
	json_t const *j, *child;
	err_t err;
	unsigned char *buf;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_PUT);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);
	CAST_OBJ_NOTNULL(app_cfg, http_srv_priv(http), APP_CFG_MAGIC);

	if (!http_req_hdr_eq_ltrl(req, "Content-Type", "application/json"))
		return http_resp_err(http, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE);

	buf = LWIP_MEMPOOL_ALLOC(small_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Small pool allocation failed");
		return ERR_MEM;
	}

	if (!copy_req_body(http, buf, SMALL_BUF_SZ, &err)) {
		HTTP_LOG_ERROR("Failed to copy request body: %d", err);
		goto err_exit;
	}

	j = json_create(buf, flds, NETWORK_MAX_FLDS);
	if (j == NULL) {
		err = parse_fail(http);
		goto err_exit;
	}

	memcpy(&cfg, network_cfg, sizeof(cfg));
	for (j = json_getChild(j); j != NULL; j = json_getSibling(j)) {
		bool wifi = false;
		char const *name = json_getName(j);
		AN(name);

		if (strcmp(name, "wifi") == 0)
			wifi = true;
		else if (strcmp(name, "ap") != 0) {
			err = unknown_fld(http, name);
			goto err_exit;
		}
		if (json_getType(j) != JSON_OBJ) {
			err = illegal_type(http, name);
			goto err_exit;
		}
		child = json_getChild(j);
		if (child == NULL) {
			err = unprocessable(http, "field %s empty", name);
			goto err_exit;
		}
		if (wifi) {
			if (!parse_wifi(http, &cfg, child, &err))
				goto err_exit;
		}
		else if (!parse_ap(http, &cfg, child, &err))
			goto err_exit;
	}

	if (memcmp(&cfg, network_cfg, sizeof(cfg)) != 0) {
		store(network_cfg, &cfg, sizeof(cfg));
		app_cfg->mode = CYW43_ITF_STA;
		app_cfg->reset = true;
	}

	err = hndlr_no_content_uncacheable(http);
 err_exit:
	LWIP_MEMPOOL_FREE(small_pool, buf);
	return err;
}
