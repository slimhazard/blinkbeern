/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/cyw43_arch.h"
#include "json-maker/json-maker.h"
#include "tiny-json.h"

#include "main.h"
#include "handlers.h"
#include "types.h"
#include "net.h"
#include "storage.h"

static unsigned char *
seg2json(int id, unsigned char *p, size_t *len)
{
	const unsigned char *names[] = { "pri", "sec", "tert" };

	AN(p);
	CHECK_OBJ_NOTNULL(segs + id, SEG_MAGIC);
	AN(seg_names[id]);

	p = json_objOpen(p, NULL, len);
	AN(*len);
	p = json_int(p, "id", id, len);
	AN(*len);
	p = json_str(p, "name", seg_names[id], len);
	AN(*len);

	/* colors object */
	p = json_objOpen(p, "color", len);
	AN(*len);
	for (int i = 0; i < N_SEG_COLORS; i++) {
		union color_pun c = { .color = segs[id].color[i] };

		p = json_objOpen(p, names[i], len);
		AN(*len);
		p = json_uint(p, "r", c.rgbw.r, len);
		AN(*len);
		p = json_uint(p, "g", c.rgbw.g, len);
		AN(*len);
		p = json_uint(p, "b", c.rgbw.b, len);
		AN(*len);
		p = json_uint(p, "w", c.rgbw.w, len);
		AN(*len);
		p = json_objClose(p, len);
		AN(*len);
	}
	p = json_objClose(p, len);
	AN(*len);

	p = json_uint(p, "start", segs[id].start, len);
	AN(*len);
	p = json_uint(p, "len", segs[id].len, len);
	AN(*len);
	p = json_uint(p, "pal", segs[id].palette, len);
	AN(*len);
	p = json_int(p, "off", segs[id].offset, len);
	AN(*len);
	p = json_uint(p, "fx", segs[id].fx, len);
	AN(*len);
	p = json_uint(p, "grp", segs[id].group, len);
	AN(*len);
	p = json_uint(p, "spc", segs[id].space, len);
	AN(*len);
	p = json_uint(p, "sx", segs[id].speed_scale, len);
	AN(*len);
	p = json_uint(p, "ix", segs[id].intensity, len);
	AN(*len);
	p = json_uint(p, "bri", segs[id].bright_scale, len);
	AN(*len);
	p = json_bool(p, "sel", segs[id].selected, len);
	AN(*len);
	p = json_bool(p, "rev", segs[id].reverse, len);
	AN(*len);
	p = json_bool(p, "mi", segs[id].mirror, len);
	AN(*len);
	p = json_objClose(p, len);
	return p;
}

static err_t
get_segment(struct http *http, struct resp *resp, unsigned const char *query,
	    size_t qlen)
{
	err_t err;
	unsigned id;
	unsigned char *p, *buf;
	size_t len = LARGE_BUF_SZ;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AN(query);
	AN(segmap);

	if ((err = parse_id(query, qlen, &id, MAX_SEGMENTS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((*segmap & (1U << id)) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	buf = LWIP_MEMPOOL_ALLOC(large_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Large pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = seg2json(id, p, &len);
	AN(len);
	p = json_end(p, &len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(large_pool, buf);
	return err;
}

err_t
segments_get_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	size_t len = LARGE_BUF_SZ, qlen;
	unsigned char *p, *buf;
	unsigned const char *query;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_GET ||
			  http_req_method(req) == HTTP_METHOD_HEAD);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if (!http_req_hdr_contains_ltrl(req, "Accept", "application/json"))
		return http_resp_err(http, HTTP_STATUS_NOT_ACCEPTABLE);

	if ((query = http_req_query(req, &qlen)) != NULL)
		return get_segment(http, resp, query, qlen);

	buf = LWIP_MEMPOOL_ALLOC(large_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Large pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = json_arrOpen(p, NULL, &len);
	AN(len);
	for (int i = 0; i < MAX_SEGMENTS; i++) {
		if ((*segmap & (1U << i)) == 0)
			continue;
		p = seg2json(i, p, &len);
		AN(len);
	}
	p = json_arrClose(p, &len);
	AN(len);
	p = json_end(p, &len);
	AN(len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(large_pool, buf);
	return err;
}

static bool
parse_color(struct http *http, json_t const *parent, struct segment *sg,
	    err_t *err)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(sg, SEG_MAGIC);
	AN(parent);
	AN(err);

	for (json_t const *col_obj = json_getChild(parent); col_obj != NULL;
	     col_obj = json_getSibling(col_obj)) {
		int n;
		char const *name;
		union color_pun color = { .color = 0 };

		name = json_getName(col_obj);
		switch (*name) {
		case 'p':
			if (strcmp(name + 1, "ri") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			n = 0;
			break;
		case 's':
			if (strcmp(name + 1, "ec") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			n = 1;
			break;
		case 't':
			if (strcmp(name + 1, "ert") != 0) {
				*err = unknown_fld(http, name);
				return false;
			}
			n = 2;
			break;
		default:
			*err = unknown_fld(http, name);
			return false;
		}
		if (json_getType(col_obj) != JSON_OBJ) {
			*err = illegal_type(http, name);
			return false;
		}
		for (json_t const *c = json_getChild(col_obj); c != NULL;
		     c = json_getSibling(c)) {
			int32_t val;

			name = json_getName(c);
			if (name[1] != '\0') {
				*err = unknown_fld(http, name);
				return false;
			}
			val = parse_int(http, c, name, 0, UINT8_MAX, err);
			if (val == INT_INVALID)
				return false;
			switch (*name) {
			case 'r':
				color.rgbw.r = (uint8_t)val;
				break;
			case 'g':
				color.rgbw.g = (uint8_t)val;
				break;
			case 'b':
				color.rgbw.b = (uint8_t)val;
				break;
			case 'w':
				color.rgbw.w = (uint8_t)val;
				break;
			default:
				*err = unknown_fld(http, name);
				return false;
			}
		}
		sg->color[n] = color.color;
	}
	return true;
}

#define SEGMENTS_MAX_FLDS (32)

err_t
segments_post_put_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	unsigned id;
	size_t qlen;
	unsigned const char *query;
	unsigned char *buf;
	char sg_name[MAX_SEGMENT_NAME] = { 0 };
	json_t flds[SEGMENTS_MAX_FLDS];
	json_t const *j;
	struct segment sg, *sgp = NULL;
	global_t cfg, *cfgp = NULL;
	uint16_t sgmap, *sgmap_p = NULL;
	int sg_cmp;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_POST ||
			  http_req_method(req) == HTTP_METHOD_PUT);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if (!http_req_hdr_eq_ltrl(req, "Content-Type", "application/json"))
		return http_resp_err(http, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE);

	if ((query = http_req_query(req, &qlen)) == NULL)
		/* XXX bad request? */
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((err = parse_id(query, qlen, &id, MAX_SEGMENTS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	if ((*segmap & (1U << id)) == 0) {
		if (http_req_method(req) == HTTP_METHOD_PUT)
			return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	}
	else if (http_req_method(req) == HTTP_METHOD_POST)
		return http_resp_err(http, HTTP_STATUS_CONFLICT);

	buf = LWIP_MEMPOOL_ALLOC(large_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Large pool allocation failed");
		return ERR_MEM;
	}

	if (!copy_req_body(http, buf, LARGE_BUF_SZ, &err)) {
		HTTP_LOG_ERROR("Failed to copy request body: %d");
		goto err_exit;
	}

	CHECK_OBJ_NOTNULL(segs + id, SEG_MAGIC);
	AN(seg_names[id]);
	if (http_req_method(req) == HTTP_METHOD_POST) {
		INIT_OBJ(&sg, SEG_MAGIC);
		*sg_name = '\0';
	}
	else {
		memcpy(&sg, segs + id, sizeof(sg));
		strcpy(sg_name, seg_names[id]);
	}

	j = json_create(buf, flds, SEGMENTS_MAX_FLDS);
	if (j == NULL) {
		err = parse_fail(http);
		goto err_exit;
	}

	for (j = json_getChild(j); j != NULL; j = json_getSibling(j)) {
		int32_t val;
		char const *name = json_getName(j), *s;
		AN(name);

		switch (*name) {
		case 'i':
			if ((name[1] != 'd' && name[1] != 'x')
			    || (name[2] != '\0')) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_INTEGER) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			val = json_getInteger(j);
			if (name[1] == 'd' && val != id) {
				err = unprocessable(
					http, "field id (%d) does not match "
					"query value (%d)", val, id);
				goto err_exit;
			}
			else if (name[1] == 'x' &&
				 (val < 0 || val > UINT8_MAX)) {
				err = out_of_range(http, "ix");
				goto err_exit;
			}
			if (name[1] == 'd')
				break;
			sg.intensity = val;
			break;
		case 'n':
			if (strcmp(name + 1, "ame") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_TEXT) {
				err = illegal_type(http, "name");
				goto err_exit;
			}
			s = json_getValue(j);
			if (strlen(s) >= MAX_SEGMENT_NAME) {
				err = unprocessable(
					http, "field name too long");
				goto err_exit;
			}
			strcpy(sg_name, s);
			break;
		case 'c':
			if (strcmp(name + 1, "olor") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_OBJ) {
				err = illegal_type(http, "color");
				goto err_exit;
			}
			if (!parse_color(http, j, &sg, &err))
				goto err_exit;
			break;
		case 's':
			/* XXX start spc sel sx */
			switch(name[1]) {
			case 't':
				if (strcmp(name + 2, "art") != 0) {
					err = unknown_fld(http, name);
					goto err_exit;
				}
				val = parse_int(http, j, "start", 0, UINT16_MAX,
						&err);
				if (val == INT_INVALID)
					goto err_exit;
				sg.start = (uint16_t)val;
				break;
			case 'p':
				if (name[2] != 'c' || name[3] != '\0') {
					err = unknown_fld(http, name);
					goto err_exit;
				}
				val = parse_int(http, j, "spc", 0, UINT8_MAX,
						&err);
				if (val == INT_INVALID)
					goto err_exit;
				sg.space = (uint8_t)val;
				break;
			case 'e':
				if (name[2] != 'l' || name[3] != '\0') {
					err = unknown_fld(http, name);
					goto err_exit;
				}
				if (json_getType(j) != JSON_BOOLEAN) {
					err = illegal_type(http, "sel");
					goto err_exit;
				}
				sg.selected = json_getBoolean(j);
				break;
			case 'x':
				if (name[2] != '\0') {
					err = unknown_fld(http, name);
					goto err_exit;
				}
				val = parse_int(http, j, "sx", 0, UINT8_MAX,
						&err);
				if (val == INT_INVALID)
					goto err_exit;
				sg.speed_scale = (uint8_t)val;
				break;
			default:
				err = unknown_fld(http, name);
				goto err_exit;
			}
			break;
		case 'l':
			if (strcmp(name + 1, "en") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			val = parse_int(http, j, "len", 0, UINT16_MAX, &err);
			if (val == INT_INVALID)
				goto err_exit;
			sg.len = (uint16_t)val;
			break;
		case 'p':
			/* XXX max palette number */
			if (strcmp(name + 1, "al") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			val = parse_int(http, j, "pal", 0, UINT16_MAX, &err);
			if (val == INT_INVALID)
				goto err_exit;
			sg.palette = (uint16_t)val;
			break;
		case 'o':
			if (strcmp(name + 1, "ff") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			val = parse_int(http, j, "off", INT16_MIN, INT16_MAX,
					&err);
			if (val == INT_INVALID)
				goto err_exit;
			sg.offset = (int16_t)val;
			break;
		case 'f':
			/* XXX max FX number */
			if (name[1] != 'x' || name[2] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			val = parse_int(http, j, "fx", 0, UINT8_MAX, &err);
			if (val == INT_INVALID)
				goto err_exit;
			sg.fx = (uint8_t)val;
			break;
		case 'g':
			if (strcmp(name + 1, "rp") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			val = parse_int(http, j, "grp", 0, UINT8_MAX, &err);
			if (val == INT_INVALID)
				goto err_exit;
			sg.group = (uint8_t)val;
			break;
		case 'b':
			if (strcmp(name + 1, "ri") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			val = parse_int(http, j, "bri", 0, UINT8_MAX, &err);
			if (val == INT_INVALID)
				goto err_exit;
			sg.bright_scale = (uint8_t)val;
			break;
		case 'r':
			if (strcmp(name + 1, "ev") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_BOOLEAN) {
				err = illegal_type(http, "rev");
				goto err_exit;
			}
			sg.reverse = json_getBoolean(j);
			break;
		case 'm':
			if (name[1] != 'i' || name[2] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_BOOLEAN) {
				err = illegal_type(http, "mi");
				goto err_exit;
			}
			sg.mirror = json_getBoolean(j);
			break;
		default:
			err = unknown_fld(http, name);
			goto err_exit;
		}
	}

	/*
	 * To avoid unnecessarily writing to flash, we change segs[id] and
	 * seg_names[id] only if their values changed. This is done by
	 * store_segment(); pass in the pointers for data that needs to
	 * change, or NULL if the data is unchanged.
	 */

	/* A POST request changes the segment map. */
	if (http_req_method(req) == HTTP_METHOD_POST) {
		sgmap = *segmap | (1U << id);
		sgmap_p = &sgmap;
	}
	sg_cmp = memcmp(&sg, segs + id, sizeof(sg));
	if (sg_cmp != 0)
		sgp = &sg;
	/*
	 * Segment 0 represents the full strip, so the len field also sets
	 * the global length field. Also in this case, pass a valid
	 * pointer to store_segment() if the global config changed, NULL
	 * otherwise.
	 *
	 * Also reset the global preset field if the segment changed.
	 */
	if (sg_cmp != 0 || id == 0) {
		CHECK_OBJ_NOTNULL(global_cfg, GLOBAL_MAGIC);
		memcpy(&cfg, global_cfg, sizeof(cfg));
		CHECK_OBJ(&cfg, GLOBAL_MAGIC);
		cfg.nleds = sg.start + sg.len;
		if (sg_cmp != 0)
			cfg.preset = NO_PRESET;
		if (memcmp(&cfg, global_cfg, sizeof(cfg)) != 0)
			cfgp = &cfg;
	}

	/*
	 * The segs array, segment map, and global config are all stored
	 * in the same flash sector. store_segment() ensures that the
	 * sector is only written once, if necessary.
	 */
	store_segment(id, sgp, sgmap_p, cfgp);
	if (strcmp(sg_name, seg_names[id]) != 0)
		store(seg_names + id, sg_name, strlen(sg_name) + 1);
	notify_cfg_change();

	err = hndlr_no_content_uncacheable(http);
 err_exit:
	LWIP_MEMPOOL_FREE(large_pool, buf);
	return err;
}

err_t
segments_delete_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	unsigned id;
	size_t qlen;
	unsigned const char *query;
	uint16_t sgmap;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_DELETE);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if ((query = http_req_query(req, &qlen)) == NULL)
		/* XXX bad request? */
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((err = parse_id(query, qlen, &id, MAX_SEGMENTS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	if (id == 0)
		return http_resp_err(http, HTTP_STATUS_FORBIDDEN);

	if ((*segmap & (1U << id)) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	sgmap = *segmap & ~(1U << id);
	store_segment(id, NULL, &sgmap, NULL);
	notify_cfg_change();

	return hndlr_no_content_uncacheable(http);
}
