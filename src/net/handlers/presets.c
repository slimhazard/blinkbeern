/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "json-maker/json-maker.h"
#include "tiny-json.h"

#include "handlers.h"
#include "types.h"
#include "net.h"
#include "storage.h"

static unsigned char *
ps2json(int id, unsigned char *p, size_t *len)
{
	char ql[3] = { '\0' };

	CHECK_OBJ_NOTNULL(ps + id, PRESET_MAGIC);
	AN(ps_names[id]);
	AN(p);

	p = json_objOpen(p, NULL, len);
	AN(*len);
	p = json_int(p, "id", id, len);
	AN(*len);
	p = json_str(p, "name", ps_names[id], len);
	AN(*len);
	ql[0] = ps[id].ql[0];
	ql[1] = ps[id].ql[1];
	p = json_str(p, "ql", ql, len);
	AN(*len);
	p = json_bool(p, "bri", ps[id].ib, len);
	AN(*len);
	p = json_bool(p, "sb", ps[id].sb, len);
	AN(*len);
	p = json_objClose(p, len);
	return p;
}

static err_t
get_preset(struct http *http, struct resp *resp, unsigned const char *query,
	   size_t qlen)
{
	err_t err;
	unsigned id;
	unsigned char *p, *buf;
	size_t len = SMALL_BUF_SZ;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AN(query);
	AN(psmap);

	if ((err = parse_id(query, qlen, &id, MAX_PRESETS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((*psmap & (1ULL << id)) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	buf = LWIP_MEMPOOL_ALLOC(small_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Small pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = ps2json(id, p, &len);
	AN(len);
	p = json_end(p, &len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(small_pool, buf);
	return err;
}

err_t
presets_get_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	size_t len = LARGE_BUF_SZ, qlen;
	unsigned char *p, *buf;
	unsigned const char *query;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_GET ||
			  http_req_method(req) == HTTP_METHOD_HEAD);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if (!http_req_hdr_contains_ltrl(req, "Accept", "application/json"))
		return http_resp_err(http, HTTP_STATUS_NOT_ACCEPTABLE);

	if ((query = http_req_query(req, &qlen)) != NULL)
		return get_preset(http, resp, query, qlen);

	buf = LWIP_MEMPOOL_ALLOC(large_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Large pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = json_arrOpen(p, NULL, &len);
	AN(len);
	for (int i = 0; i < MAX_PRESETS; i++) {
		if ((*psmap & (1ULL << i)) == 0)
			continue;
		p = ps2json(i, p, &len);
		AN(len);
	}
	p = json_arrClose(p, &len);
	AN(len);
	p = json_end(p, &len);
	AN(len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(large_pool, buf);
	return err;
}

#define PRESET_MAX_FLDS (5)

err_t
presets_post_put_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	unsigned id;
	size_t qlen;
	unsigned const char *query;
	unsigned char *buf;
	char ps_name[MAX_PRESET_NAME] = { 0 }, ql[QL_LEN] = { 0 };
	json_t flds[PRESET_MAX_FLDS];
	json_t const *j;
	struct preset preset, *ps_p = NULL;
	uint64_t newmap, *map_p = NULL;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_POST ||
			  http_req_method(req) == HTTP_METHOD_PUT);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if (!http_req_hdr_eq_ltrl(req, "Content-Type", "application/json"))
		return http_resp_err(http, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE);

	if ((query = http_req_query(req, &qlen)) == NULL)
		/* XXX bad request? */
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((err = parse_id(query, qlen, &id, MAX_PRESETS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	if (id >= MAX_PRESETS) {
		HTTP_LOG_ERROR("Preset id out of range: %d (max %d)", id,
			       MAX_PRESETS - 1);
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	}

	if ((*psmap & (1ULL << id)) == 0) {
		if (http_req_method(req) == HTTP_METHOD_PUT)
			return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	}
	else if (http_req_method(req) == HTTP_METHOD_POST)
		return http_resp_err(http, HTTP_STATUS_CONFLICT);

	buf = LWIP_MEMPOOL_ALLOC(small_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Small pool allocation failed");
		return ERR_MEM;
	}

	if (!copy_req_body(http, buf, SMALL_BUF_SZ, &err)) {
		HTTP_LOG_ERROR("Failed to copy request body: %d", err);
		goto err_exit;
	}

	CHECK_OBJ_NOTNULL(ps + id, PRESET_MAGIC);
	AN(ps_names[id]);
	if (http_req_method(req) == HTTP_METHOD_POST) {
		INIT_OBJ(&preset, PRESET_MAGIC);
		*ps_name = '\0';
	}
	else {
		CHECK_OBJ_NOTNULL(ps + id, PRESET_MAGIC);
		memcpy(&preset, ps + id, sizeof(preset));
		strcpy(ps_name, ps_names[id]);
	}

	j = json_create(buf, flds, PRESET_MAX_FLDS);
	if (j == NULL) {
		err = parse_fail(http);
		goto err_exit;
	}

	for (j = json_getChild(j); j != NULL; j = json_getSibling(j)) {
		char const *name = json_getName(j), *s;
		AN(name);

		switch (*name) {
		case 'b':
			if (strcmp(name +1, "ri")) { 
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_BOOLEAN) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			preset.ib = json_getBoolean(j);
			break;
		case 'n':
			if (strcmp(name + 1, "ame") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_TEXT) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			s = json_getValue(j);
			if (strlen(s) >= MAX_PRESET_NAME) {
				err = unprocessable(
					http, "field name too long");
				goto err_exit;
			}
			strcpy(ps_name, s);
			break;
		case 'q':
			if (name[1] != 'l' || name[2] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_TEXT) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			s = json_getValue(j);
			if (strlen(s) > QL_LEN) {
				err = unprocessable(http, "field ql too long");
				goto err_exit;
			}
			preset.ql[0] = s[0];
			preset.ql[1] = s[1];
			break;
		case 's':
			if (name[1] != 'b' || name[2] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_BOOLEAN) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			preset.sb = json_getBoolean(j);
			break;
		default:
			err = unknown_fld(http, name);
			goto err_exit;
		}
	}
	preset.segmap = *segmap;

	/* A POST request changes the preset map. */
	if (http_req_method(req) == HTTP_METHOD_POST) {
		newmap = *psmap | (1ULL << id);
		map_p = &newmap;
	}
	if (memcmp(&preset, ps + id, sizeof(preset)) != 0)
		ps_p = &preset;
	store_preset(id, ps_p, map_p);
	if (memcmp(segs, ps_segs + id, sizeof(struct segment) * MAX_SEGMENTS)
	    != 0)
		store(ps_segs + id, segs,
		      sizeof(struct segment) * MAX_SEGMENTS);
	if (memcmp(seg_names, ps_seg_names[id],
		   MAX_SEGMENTS * MAX_SEGMENT_NAME) != 0)
		store(ps_seg_names + id, seg_names,
		      MAX_SEGMENTS * MAX_SEGMENT_NAME);
	if (strcmp(ps_name, ps_names[id]) != 0)
		store(ps_names + id, ps_name, strlen(ps_name) + 1);

	/*
	 * No need to notify the other core of the config change. This
	 * handler only stores the preset; the preset becomes active
	 * via the global handler.
	 */

	err = hndlr_no_content_uncacheable(http);
 err_exit:
	LWIP_MEMPOOL_FREE(small_pool, buf);
	return err;
}

err_t
presets_delete_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	unsigned id;
	size_t qlen;
	unsigned const char *query;
	uint64_t newmap;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_DELETE);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if ((query = http_req_query(req, &qlen)) == NULL)
		/* XXX bad request? */
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((err = parse_id(query, qlen, &id, MAX_PRESETS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	if ((*psmap & (1ULL << id)) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	newmap = *psmap & ~(1ULL << id);
	store(psmap, &newmap, sizeof(newmap));

	return hndlr_no_content_uncacheable(http);
}
