/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "handlers.h"

#ifndef SMALL_POOL_SZ
#define SMALL_POOL_SZ (MAX_CONCURRENT_CX_HINT)
#endif

#ifndef LARGE_POOL_SZ
#define LARGE_POOL_SZ (2)
#endif

#ifndef PL_BODY_POOL_SZ
#define PL_BODY_POOL_SZ (2)
#endif

#ifndef PL_JSON_POOL_SZ
#define PL_JSON_POOL_SZ (2)
#endif

#define ERR_PAYLOAD_MAX_LEN	(64)
#define ERR_BODY_MAX_LEN	\
	(ERR_PAYLOAD_MAX_LEN + STRLEN_LTRL("{\"error\":\"\"}"))

LWIP_MEMPOOL_DECLARE(small_pool, SMALL_POOL_SZ, SMALL_BUF_SZ,
		     "small buffer pool");
LWIP_MEMPOOL_DECLARE(large_pool, LARGE_POOL_SZ, LARGE_BUF_SZ,
		     "large buffer pool");
LWIP_MEMPOOL_DECLARE(pl_body_pool, PL_BODY_POOL_SZ, PL_BODY_SZ,
		     "playlist body pool");
LWIP_MEMPOOL_DECLARE(pl_json_pool, PL_JSON_POOL_SZ, PL_JSON_SZ,
		     "playlist json pool");

err_t
get_hndlr_success(struct http *http, uint8_t *buf, size_t len)
{
	struct resp *resp;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AN(buf);

	if ((err = http_resp_set_type_ltrl(resp, "application/json"))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Set Content-Type: %d", err);
		return err;
	}
	if ((err = http_resp_set_len(resp, len)) != ERR_OK) {
		HTTP_LOG_ERROR("Set Content-Length: %d", err);
		return err;
	}
	if ((err = http_resp_set_hdr_ltrl(resp, "Cache-Control", "no-store"))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Set Cache-Control: %d", err);
		return err;
	}
	return http_resp_send_buf(http, buf, len, false);
}

err_t
hndlr_no_content_uncacheable(struct http *http)
{
	struct resp *resp;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);

	if ((err = http_resp_set_status(resp, HTTP_STATUS_NO_CONTENT))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Set response status: %d", err);
		return err;
	}
	if ((err = http_resp_set_hdr_ltrl(resp, "Cache-Control", "no-store"))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Set Cache-Control: %d", err);
		return err;
	}
	return http_resp_send_hdr(http);
}

err_t
unprocessable(struct http *http, const char *fmt, ...)
{
	struct resp *resp;
	va_list ap;
	char payload[ERR_PAYLOAD_MAX_LEN];
	char body[ERR_BODY_MAX_LEN];
	size_t len;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AN(fmt);

	va_start(ap, fmt);
	vsnprintf(payload, ERR_PAYLOAD_MAX_LEN, fmt, ap);
	va_end(ap);
	len = snprintf(body, ERR_BODY_MAX_LEN, "{\"error\":\"%s\"}", payload);

	if ((err =
	     http_resp_set_status(resp, HTTP_STATUS_UNPROCESSABLE_CONTENT))
	    != ERR_OK) {
		HTTP_LOG_ERROR("set status: %d", err);
		return err;
	}
	/* XXX not really success */
	return get_hndlr_success(http, body, len);
}

bool
copy_req_body(struct http *http, uint8_t *buf, size_t maxlen, err_t *err)
{
	struct req *req;
	size_t body_len;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	AN(buf);
	AN(err);

	body_len = http_req_body_len(req);
	if (body_len > maxlen) {
		HTTP_LOG_ERROR("request body len %zd > max (%d)", body_len,
			       maxlen);
		*err = http_resp_err(http, HTTP_STATUS_CONTENT_TOO_LARGE);
		return false;
	}

	for (size_t total = 0, len = body_len; total < body_len;
	     total += len, len = body_len - total)
		if ((*err = http_req_body(http, buf + total, &len, total))
		    != ERR_OK) {
			HTTP_LOG_ERROR("cannot copy req body: %d", *err);
			return false;
		}

	return true;
}

int32_t
parse_int(struct http *http, json_t const *j, const char *fld, int64_t min,
	  int64_t max, err_t *err)
{
	int64_t val;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(j);
	AN(fld);
	AN(err);

	if (json_getType(j) != JSON_INTEGER) {
		*err = illegal_type(http, fld);
		return INT_INVALID;
	}
	val = json_getInteger(j);
	if (val < min || val > max) {
		*err = out_of_range(http, fld);
		return INT_INVALID;
	}
	return (int32_t)val;
}

err_t
parse_id(unsigned const char *query, size_t qlen, int *id, size_t max)
{
	AN(query);

	if (qlen < 4)
		return ERR_ARG;
	if (query[0] != 'i' || query[1] != 'd' || query[2] != '=')
		return ERR_ARG;
	*id = 0;
	for (int i = 3; i < qlen; i++) {
		if (query[i] < '0' || query[i] > '9')
			return ERR_VAL;
		*id = *id * 10 + (query[i] - '0');
	}
	if (*id >= max)
		return ERR_ARG;
	return ERR_OK;
}
