/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdarg.h>

#include "picow_http/http.h"
#include "lwip/memp.h"
#include "tiny-json.h"

#define INT_INVALID (INT32_MAX)

#ifndef SMALL_BUF_SZ
#define SMALL_BUF_SZ (256)
#endif

#ifndef LARGE_BUF_SZ
#define LARGE_BUF_SZ (6 * 1024)
#endif

#ifndef PL_BODY_SZ
#define PL_BODY_SZ (1024)
#endif

#ifndef PL_JSON_SZ
#define PL_JSON_SZ (3 * 1024)
#endif

LWIP_MEMPOOL_PROTOTYPE(small_pool);
LWIP_MEMPOOL_PROTOTYPE(large_pool);
LWIP_MEMPOOL_PROTOTYPE(pl_body_pool);
LWIP_MEMPOOL_PROTOTYPE(pl_json_pool);

err_t get_hndlr_success(struct http *http, uint8_t *buf, size_t len);
err_t hndlr_no_content_uncacheable(struct http *http);
err_t unprocessable(struct http *http, const char *fmt, ...);
bool copy_req_body(struct http *http, uint8_t *buf, size_t maxlen, err_t *err);
int32_t parse_int(struct http *http, json_t const *j, const char *fld,
		  int64_t min, int64_t max, err_t *err);
err_t parse_id(unsigned const char *query, size_t qlen, int *id, size_t max);

static inline err_t
unknown_fld(struct http *http, const char *fld)
{
	return unprocessable(http, "unknown field: %s", fld);
}

static inline err_t
illegal_type(struct http *http, const char *fld)
{
	return unprocessable(http, "field %s illegal type", fld);
}

static inline err_t
out_of_range(struct http *http, const char *fld)
{
	return unprocessable(http, "field %s out of range", fld);
}

static inline err_t
parse_fail(struct http *http)
{
	return unprocessable(http, "cannot parse request body");
}
