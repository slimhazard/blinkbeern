/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "json-maker/json-maker.h"
#include "tiny-json.h"

#include "main.h"
#include "handlers.h"
#include "types.h"
#include "net.h"
#include "storage.h"

static unsigned char *
pl2json(int id, unsigned char *p, size_t *len)
{
	char ql[3] = { '\0' };

	CHECK_OBJ_NOTNULL(pl + id, PLAYLIST_MAGIC);
	AN(pl_names[id]);
	AN(p);
	AN(len);

	p = json_objOpen(p, NULL, len);
	AN(*len);
	p = json_int(p, "id", id, len);
	AN(*len);
	p = json_str(p, "name", pl_names[id], len);
	AN(*len);
	ql[0] = pl[id].ql[0];
	ql[1] = pl[id].ql[1];
	p = json_str(p, "ql", ql, len);
	AN(*len);
	p = json_arrOpen(p, "ps", len);
	AN(*len);
	for (int i = 0; i < pl[id].nps; i++) {
		p = json_int(p, NULL, pl[id].ps[i], len);
		AN(*len);
	}
	p = json_arrClose(p, len);
	AN(*len);
	p = json_arrOpen(p, "dur", len);
	AN(*len);
	for (int i = 0; i < pl[id].nps; i++) {
		p = json_int(p, NULL, pl[id].dur[i], len);
		AN(len);
	}
	p = json_arrClose(p, len);
	AN(*len);
	p = json_int(p, "repeat", pl[id].repeat, len);
	AN(len);
	p = json_int(p, "end", pl[id].end, len);
	AN(len);
	p = json_bool(p, "shuffle", pl[id].shuffle, len);
	AN(*len);
	p = json_objClose(p, len);
	return p;
}

static err_t
get_playlist(struct http *http, struct resp *resp, unsigned const char *query,
	     size_t qlen)
{
	err_t err;
	unsigned id;
	unsigned char *p, *buf;
	size_t len = LARGE_BUF_SZ;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AN(query);
	AN(plmap);

	if ((err = parse_id(query, qlen, &id, MAX_PLAYLISTS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((*plmap & (1ULL << id)) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	buf = LWIP_MEMPOOL_ALLOC(pl_body_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Playlist body pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = pl2json(id, p, &len);
	AN(len);
	p = json_end(p, &len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(pl_body_pool, buf);
	return err;
}

err_t
playlists_get_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	size_t len = LARGE_BUF_SZ, qlen;
	unsigned char *p, *buf;
	unsigned const char *query;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_GET ||
			  http_req_method(req) == HTTP_METHOD_HEAD);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if (!http_req_hdr_contains_ltrl(req, "Accept", "application/json"))
		return http_resp_err(http, HTTP_STATUS_NOT_ACCEPTABLE);

	if ((query = http_req_query(req, &qlen)) != NULL)
		return get_playlist(http, resp, query, qlen);

	buf = LWIP_MEMPOOL_ALLOC(large_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Large pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = json_arrOpen(p, NULL, &len);
	AN(len);
	for (int i = 0; i < MAX_PLAYLISTS; i++) {
		if ((*plmap & (1ULL << i)) == 0)
			continue;
		p = pl2json(i, p, &len);
		AN(len);
	}
	p = json_arrClose(p, &len);
	AN(len);
	p = json_end(p, &len);
	AN(len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(large_pool, buf);
	return err;
}

#define PLAYLIST_MAX_FLDS (140)

err_t
playlists_post_put_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	unsigned id;
	size_t qlen;
	unsigned const char *query;
	unsigned char *buf;
	char pl_name[MAX_PLAYLIST_NAME] = { 0 }, ql[QL_LEN] = { 0 };
	json_t *flds = NULL;
	json_t const *j;
	struct playlist playlist;
	uint64_t newmap;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_POST ||
			  http_req_method(req) == HTTP_METHOD_PUT);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);
	AN(plmap);
	AN(psmap);

	if (!http_req_hdr_eq_ltrl(req, "Content-Type", "application/json"))
		return http_resp_err(http, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE);

	if ((query = http_req_query(req, &qlen)) == NULL)
		/* XXX bad request? */
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((err = parse_id(query, qlen, &id, MAX_PLAYLISTS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	if (id >= MAX_PLAYLISTS) {
		HTTP_LOG_ERROR("Playlist id out of range: %d (max %d)", id,
			       MAX_PLAYLISTS - 1);
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	}

	if ((*plmap & (1ULL << id)) == 0) {
		if (http_req_method(req) == HTTP_METHOD_PUT)
			return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	}
	else if (http_req_method(req) == HTTP_METHOD_POST)
		return http_resp_err(http, HTTP_STATUS_CONFLICT);

	buf = LWIP_MEMPOOL_ALLOC(pl_body_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Playlist body pool allocation failed");
		return ERR_MEM;
	}

	flds = LWIP_MEMPOOL_ALLOC(pl_json_pool);
	if (flds == NULL) {
		HTTP_LOG_ERROR("Playlist json pool allocation failed");
		err = ERR_MEM;
		goto err_exit;
	}

	if (!copy_req_body(http, buf, LARGE_BUF_SZ, &err)) {
		HTTP_LOG_ERROR("Failed to copy request body: %d", err);
		goto err_exit;
	}

	CHECK_OBJ_NOTNULL(pl + id, PLAYLIST_MAGIC);
	AN(pl_names[id]);
	if (http_req_method(req) == HTTP_METHOD_POST) {
		INIT_OBJ(&playlist, PLAYLIST_MAGIC);
		*pl_name = '\0';
	}
	else {
		CHECK_OBJ_NOTNULL(pl + id, PLAYLIST_MAGIC);
		memcpy(&playlist, pl + id, sizeof(playlist));
		strcpy(pl_name, pl_names[id]);
	}

	j = json_create(buf, flds, PLAYLIST_MAX_FLDS);
	if (j == NULL) {
		err = parse_fail(http);
		goto err_exit;
	}

	for (j = json_getChild(j); j != NULL; j = json_getSibling(j)) {
		int32_t val;
		int idx;
		json_t const *k;
		char const *name = json_getName(j), *s;
		AN(name);

		switch (*name) {
		case 'n':
			if (strcmp(name + 1, "ame") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_TEXT) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			s = json_getValue(j);
			if (strlen(s) >= MAX_PLAYLIST_NAME) {
				err = unprocessable(
					http, "field name too long");
				goto err_exit;
			}
			strcpy(pl_name, s);
			break;
		case 'q':
			if (name[1] != 'l' || name[2] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_TEXT) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			s = json_getValue(j);
			if (strlen(s) > QL_LEN) {
				err = unprocessable(http, "field ql too long");
				goto err_exit;
			}
			playlist.ql[0] = s[0];
			playlist.ql[1] = s[1];
			break;
		case 'p':
			if (name[1] != 's' || name[2] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_ARRAY) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			for (k = json_getChild(j), idx = 0; k != NULL;
			     k = json_getSibling(k), idx++) {
				if (idx >= PS_PER_PL) {
					err = unprocessable(
						http, "too many presets");
					goto err_exit;
				}
				if (json_getType(k) != JSON_INTEGER) {
					err = unprocessable(
						http, "ps[%d] illegal type",
						idx);
					goto err_exit;
				}
				val = json_getInteger(k);
				if (val < 0 || val >= MAX_PRESETS) {
					err = unprocessable(
						http, "ps[%d]=%d out of range",
						idx, val);
					goto err_exit;
				}
				if ((*psmap & (1ULL << val)) == 0) {
					err = unprocessable(
						http,
						"ps[%d]: preset %d not defined",
						idx, val);
					goto err_exit;
				}
				playlist.ps[idx] = val;
			}
			playlist.nps = idx;
			break;
		case 'd':
			if (name[1] != 'u' || name[2] != 'r'
			    || name[3] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_ARRAY) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			for (k = json_getChild(j), idx = 0; k != NULL;
			     k = json_getSibling(k), idx++) {
				if (idx >= PS_PER_PL) {
					err = unprocessable(
						http, "too many durations");
					goto err_exit;
				}
				if (json_getType(k) != JSON_INTEGER) {
					err = unprocessable(
						http, "dur[%d] illegal type",
						idx);
					goto err_exit;
				}
				val = json_getInteger(k);
				if (val < 0 || val > UINT16_MAX) {
					err = unprocessable(
						http, "dur[%d]=%d out of range",
						idx, val);
					goto err_exit;
				}
				playlist.dur[idx] = (uint16_t)val;
			}
			break;
		case 'r':
			if (strcmp(name + 1, "epeat") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_INTEGER) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			val = json_getInteger(j);
			if (val < 0 || val > UINT8_MAX) {
				err = out_of_range(http, name);
				goto err_exit;
			}
			playlist.repeat = (uint8_t)val;
			break;
		case 'e':
			if (name[1] != 'n' || name[2] != 'd'
			    || name[3] != '\0') {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_INTEGER) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			val = json_getInteger(j);
			if (val < 0 || val > UINT8_MAX) {
				err = out_of_range(http, name);
				goto err_exit;
			}
			playlist.end = (uint8_t)val;
			break;
		case 's':
			if (strcmp(name + 1, "huffle") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_BOOLEAN) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			playlist.shuffle = json_getBoolean(j);
			break;
		default:
			err = unknown_fld(http, name);
			goto err_exit;
		}
	}

	/* A POST request changes the playlist map. */
	if (http_req_method(req) == HTTP_METHOD_POST) {
		newmap = *plmap | (1ULL << id);
		store(plmap, &newmap, sizeof(newmap));
	}
	if (memcmp(&playlist, pl + id, sizeof(playlist)) != 0)
		store(pl + id, &playlist, sizeof(playlist));
	if (strcmp(pl_name, pl_names[id]) != 0)
		store(pl_names + id, pl_name, strlen(pl_name) + 1);

	/*
	 * No need to notify the other core of the config change. This
	 * handler only stores the playlist; the playlist becomes active
	 * via the global handler.
	 */
	err = hndlr_no_content_uncacheable(http);
 err_exit:
	LWIP_MEMPOOL_FREE(pl_body_pool, buf);
	if (flds != NULL)
		LWIP_MEMPOOL_FREE(pl_json_pool, flds);
	return err;
}

err_t
playlists_delete_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	unsigned id;
	size_t qlen;
	unsigned const char *query;
	uint64_t newmap;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_DELETE);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);
	AN(plmap);
	CHECK_OBJ_NOTNULL(global_cfg, GLOBAL_MAGIC);

	if ((query = http_req_query(req, &qlen)) == NULL)
		/* XXX bad request? */
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if ((err = parse_id(query, qlen, &id, MAX_PLAYLISTS)) != ERR_OK)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	if ((*plmap & (1ULL << id)) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);

	newmap = *plmap & ~(1ULL << id);
	store(plmap, &newmap, sizeof(newmap));
	if (global_cfg->playlist == id) {
		global_t cfg;

		pl_runner_stop();
		memcpy(&cfg, global_cfg, sizeof(cfg));
		CHECK_OBJ(&cfg, GLOBAL_MAGIC);
		cfg.playlist = NO_PLAYLIST;
		store_global(&cfg);
		notify_cfg_change();
	}

	return hndlr_no_content_uncacheable(http);
}
