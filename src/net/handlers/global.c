/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/cyw43_arch.h"
#include "json-maker/json-maker.h"
#include "tiny-json.h"

#include "main.h"
#include "handlers.h"
#include "types.h"
#include "net.h"
#include "storage.h"

err_t
global_get_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	size_t len = SMALL_BUF_SZ;
	unsigned char *buf, *p;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_GET ||
			  http_req_method(req) == HTTP_METHOD_HEAD);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);
	CHECK_OBJ_NOTNULL(global_cfg, GLOBAL_MAGIC);

	if (!http_req_hdr_contains_ltrl(req, "Accept", "application/json"))
		return http_resp_err(http, HTTP_STATUS_NOT_ACCEPTABLE);

	buf = LWIP_MEMPOOL_ALLOC(small_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Small pool allocation failed");
		return ERR_MEM;
	}
	p = buf;

	p = json_objOpen(p, NULL, &len);
	AN(len);
	p = json_int(p, "nleds", global_cfg->nleds, &len);
	AN(len);
	p = json_int(p, "fps", global_cfg->fps_scale, &len);
	AN(len);
	p = json_int(p, "pl", global_cfg->playlist, &len);
	AN(len);
	p = json_int(p, "bri", global_cfg->bright_scale, &len);
	AN(len);
	p = json_int(p, "ps", global_cfg->preset, &len);
	AN(len);
	p = json_bool(p, "on", global_cfg->on, &len);
	AN(len);
	p = json_objClose(p, &len);
	AN(len);
	p = json_end(p, &len);
	AN(len);

	err = get_hndlr_success(http, buf, p - buf);
	LWIP_MEMPOOL_FREE(small_pool, buf);
	return err;
}

#define GLOBAL_MAX_FLDS (8)

err_t
global_put_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	err_t err;
	json_t flds[GLOBAL_MAX_FLDS];
	json_t const *j;
	global_t cfg;
	unsigned char *buf;
	uint8_t prev_pl;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	PICOW_HTTP_ASSERT(http_req_method(req) == HTTP_METHOD_PUT);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AZ(priv);

	if (!http_req_hdr_eq_ltrl(req, "Content-Type", "application/json"))
		return http_resp_err(http, HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE);

	buf = LWIP_MEMPOOL_ALLOC(small_pool);
	if (buf == NULL) {
		HTTP_LOG_ERROR("Small pool allocation failed");
		return ERR_MEM;
	}

	if (!copy_req_body(http, buf, SMALL_BUF_SZ, &err)) {
		HTTP_LOG_ERROR("Failed to copy request body: %d", err);
		goto err_exit;
	}

	j = json_create(buf, flds, GLOBAL_MAX_FLDS);
	if (j == NULL) {
		err = parse_fail(http);
		goto err_exit;
	}

	memcpy(&cfg, global_cfg, sizeof(cfg));
	prev_pl = global_cfg->playlist;
	for (j = json_getChild(j); j != NULL; j = json_getSibling(j)) {
		int32_t val;
		char const *name = json_getName(j);
		AN(name);

		switch (*name) {
		case 'b':
			if (strcmp(name + 1, "ri") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if ((val = parse_int(http, j, name, 0, UINT8_MAX, &err))
			    == INT_INVALID)
				goto err_exit;
			cfg.bright_scale = (uint8_t)val;
			break;
		case 'f':
			if (strcmp(name + 1, "ps") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if ((val = parse_int(http, j, name, 0, UINT8_MAX, &err))
			    == INT_INVALID)
				goto err_exit;
			cfg.fps_scale = (uint8_t)val;
			break;
		case 'n':
			/* XXX MAX_NLEDS */
			if (strcmp(name + 1, "leds") != 0) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if ((val = parse_int(http, j, name, 0, UINT16_MAX,
					     &err)) == INT_INVALID)
				goto err_exit;
			cfg.nleds = (uint16_t)val;
			break;
		case 'o':
			if ((name[1] != 'n') || (name[2] != '\0')) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_BOOLEAN) {
				err = illegal_type(http, "on");
				goto err_exit;
			}
			cfg.on = json_getBoolean(j);
			break;
		case 'p':
			if ((name[1] != 's' && name[1] != 'l')
			    || (name[2] != '\0')) {
				err = unknown_fld(http, name);
				goto err_exit;
			}
			if (json_getType(j) != JSON_INTEGER) {
				err = illegal_type(http, name);
				goto err_exit;
			}
			val = json_getInteger(j);
			if (val < 0
			    || (name[1] == 's' && val > MAX_PRESETS - 1 &&
				val != NO_PRESET)
			    || (name[1] == 'l' && val > MAX_PLAYLISTS - 1 &&
				val != NO_PLAYLIST)) {
				err = out_of_range(http, name);
				goto err_exit;
			}
			if (name[1] == 's') {
				AN(psmap);

				if (val != NO_PRESET &&
				    (*psmap & (1ULL << val)) == 0) {
					err = unprocessable(
						http, "Preset %d not defined",
						val);
					goto err_exit;
				}
				cfg.preset = (uint8_t)val;
			}
			else {
				AN(plmap);

				if (val != NO_PLAYLIST &&
				    (*plmap & (1ULL << val)) == 0) {
					err = unprocessable(
						http, "Playlist %d not defined",
						val);
					goto err_exit;
				}
				cfg.playlist = (uint8_t)val;
			}
			break;
		default:
			err = unknown_fld(http, name);
			goto err_exit;
		}
	}

	/*
	 * If there was a change, store the new values of global_cfg, and
	 * signal core0 to reconfigure.
	 */
	if (memcmp(&cfg, global_cfg, sizeof(cfg)) != 0) {
		store_global(&cfg);
		notify_cfg_change();
	}
	/* Start or stop the playlist as needed. */
	if (cfg.playlist != prev_pl) {
		pl_runner_stop();
		if (cfg.playlist != NO_PLAYLIST)
			pl_runner_start();
	}

	err = hndlr_no_content_uncacheable(http);
 err_exit:
	LWIP_MEMPOOL_FREE(small_pool, buf);
	return err;
}
