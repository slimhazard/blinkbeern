/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <string.h>

#include <stdio.h>

#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "pico/sem.h"
#include "pico/bootrom.h"
#include "pico/binary_info.h"
#include "pico/rand.h"
#include "hardware/dma.h"
#include "hardware/pio.h"
#include "hardware/irq.h"
#include "hardware/exception.h"

#include "sk6812.pio.h"

#include "main.h"
#include "fx_tbl.h"
#include "types.h"

#include "storage.h"
#include "net/net.h"

#define MAX_PIXELS (4096)

static struct semaphore storage_init_flag;
static struct semaphore cfg_reset;
static struct semaphore frame_update;
static struct semaphore refresh_ready;

static const int PIN_TX = 16;

static const uint64_t refresh_us = 80;

static unsigned reset_alarm_num, frame_alarm_num;
static unsigned px_chan;

static uint32_t pxbuf[2][MAX_PIXELS], segbuf[MAX_PIXELS * 2];

static uint8_t seg_speed_accum[MAX_SEGMENTS];

static void
frame_alarm(unsigned alarm_num)
{
	(void)alarm_num;

	sem_release(&frame_update);
}

static void
reset_alarm(unsigned alarm_num)
{
	(void)alarm_num;

	sem_release(&refresh_ready);
}

static void
dma_isr(void)
{
	dma_channel_acknowledge_irq0(px_chan);
	if (!hardware_alarm_set_target(reset_alarm_num,
				       make_timeout_time_us(refresh_us)))
		return;
	sem_release(&refresh_ready);
}

void
notify_cfg_change(void)
{
	(void)sem_release(&cfg_reset);
	(void)sem_release(&frame_update);
}

void
notify_storage_inited(void)
{
	sem_release(&storage_init_flag);
}

void
on_hardfault(void)
{
	HTTP_LOG_ERROR("HARDFAULT");
#ifndef NDEBUG
	__breakpoint();
#endif
}

static inline uint64_t
get_frame_intvl_us(void)
{
	uint16_t n = global_cfg->nleds;
	uint8_t s = global_cfg->fps_scale;

	if (s == 0)
		return 0;
	if (s == UINT8_MAX)
		return (192 * n)/5 + refresh_us;
	return ((12 * n + 25) << 12) / (5 * s);
}

static inline bool
pace(int i)
{
	uint8_t prev = seg_speed_accum[i];

	if (segs[i].fx == FX_STATIC)
		return (false);
	seg_speed_accum[i] += segs[i].speed_scale;
	if (seg_speed_accum[i] < prev)
		return (true);
	return (false);
}

static inline bool
seg_active(unsigned i)
{
	return (((*segmap & (1U << i)) != 0) && segs[i].selected);
}

static struct fx_state state[MAX_SEGMENTS];

extern char __StackLimit, __end__;
#define HEAPSZ (&__StackLimit - &__end__)

int
main(void)
{
	unsigned pace_chan;
	uint16_t nleds;
	PIO pio = pio0;
	int sm;

	/* Values from vcocalc.py to set clk_sys to 133 MHz */
	set_sys_clock_pll(1596 * MHZ, 6, 2);

	exception_set_exclusive_handler(HARDFAULT_EXCEPTION, on_hardfault);

	stdout_uart_init();

	/* Storage is initially flagged as not initialized */
	sem_init(&storage_init_flag, 0, 1);

	/* Config is initially ready to reset. */
	sem_init(&cfg_reset, 1, 1);

	multicore_lockout_victim_init();
	sleep_ms(5);
	multicore_reset_core1();
	sleep_ms(5);
	multicore_launch_core1(core1_main);
	sleep_ms(5);

	sm = pio_claim_unused_sm(pio, true);
	sk6812_rgbw_program_init(pio, sm, PIN_TX);

	px_chan = dma_claim_unused_channel(true);
	dma_channel_config dma_cfg = dma_channel_get_default_config(px_chan);
	channel_config_set_read_increment(&dma_cfg, true);
	channel_config_set_write_increment(&dma_cfg, false);
	channel_config_set_transfer_data_size(&dma_cfg, DMA_SIZE_32);
	channel_config_set_dreq(&dma_cfg, pio_get_dreq(pio, sm, true));
	dma_channel_configure(px_chan, &dma_cfg, &pio->txf[sm], NULL, 1, false);

	col2buf_chan = dma_claim_unused_channel(true);
	dma_cfg	= dma_channel_get_default_config(col2buf_chan);
	channel_config_set_read_increment(&dma_cfg, false);
	channel_config_set_write_increment(&dma_cfg, true);
	channel_config_set_transfer_data_size(&dma_cfg, DMA_SIZE_32);
	dma_channel_configure(col2buf_chan, &dma_cfg, pxbuf, NULL, 1, false);

	buf2buf_chan = dma_claim_unused_channel(true);
	dma_cfg	= dma_channel_get_default_config(buf2buf_chan);
	channel_config_set_read_increment(&dma_cfg, true);
	channel_config_set_write_increment(&dma_cfg, true);
	channel_config_set_transfer_data_size(&dma_cfg, DMA_SIZE_32);
	dma_channel_configure(buf2buf_chan, &dma_cfg, NULL, NULL, 1, false);

	pace_chan = dma_claim_unused_channel(true);
	dma_cfg	= dma_channel_get_default_config(pace_chan);
	channel_config_set_read_increment(&dma_cfg, true);
	channel_config_set_write_increment(&dma_cfg, true);
	channel_config_set_transfer_data_size(&dma_cfg, DMA_SIZE_32);
	dma_channel_configure(pace_chan, &dma_cfg, NULL, NULL, 1, false);

	irq_set_exclusive_handler(DMA_IRQ_0, dma_isr);
	irq_set_enabled(DMA_IRQ_0, true);
	dma_channel_set_irq0_enabled(px_chan, true);

	reset_alarm_num = hardware_alarm_claim_unused(true);
	frame_alarm_num = hardware_alarm_claim_unused(true);

	/* Initially ready */
	sem_init(&refresh_ready, 1, 1);
	/* Initially run the default boot preset */
	sem_init(&frame_update, 1, 1);

	/* seeds pico_rand */
	(void)get_rand_32();

	hardware_alarm_set_callback(reset_alarm_num, reset_alarm);
	hardware_alarm_set_callback(frame_alarm_num, frame_alarm);

	int cur = 0;
	uint64_t frame_intvl_us = 0;
	memset(segs + 1, 0, (MAX_SEGMENTS - 1) * sizeof(struct segment));

	/* Wait until the other core signals that storage is initialized. */
	sem_acquire_blocking(&storage_init_flag);

	nleds = global_cfg->nleds;

	dma_channel_set_trans_count(px_chan, global_cfg->nleds, false);
	dma_channel_set_trans_count(col2buf_chan, global_cfg->nleds, false);

	HTTP_LOG_DEBUG("heap size=%ld", HEAPSZ);

	for (;;) {
		absolute_time_t timeout;

		sem_acquire_blocking(&frame_update);
		hardware_alarm_cancel(frame_alarm_num);
		if (sem_try_acquire(&cfg_reset)) {
			if (global_cfg->nleds != nleds) {
				dma_channel_set_trans_count(
					px_chan, global_cfg->nleds, false);
				nleds = global_cfg->nleds;
			}
			for (int i = 0; i < MAX_SEGMENTS; i++) {
				if (!seg_active(i))
					continue;
				state[i].segbuf[0] = segbuf + segs[i].start * 2;
				state[i].segbuf[1] = segbuf + segs[i].len +
					segs[i].start * 2;
				seg_speed_accum[i] = 0;
				if (fx_tbl[segs[i].fx].init != NULL)
					fx_tbl[segs[i].fx].init(
						global_cfg, segs + i,
						state + i);
				fx_tbl[segs[i].fx].fx(
					pxbuf[cur] + segs[i].start,
					segs + i, state + i);
			}
			frame_intvl_us = get_frame_intvl_us();
			HTTP_LOG_DEBUG("frame_intvl_us=%llu", frame_intvl_us);
#ifndef NDEBUG
			for (int i = 0; i < MAX_SEGMENTS; i++) {
				if (!seg_active(i))
					continue;
				HTTP_LOG_DEBUG("\tsegs[%d] fx=%d", i,
					       segs[i].fx);
			}
#endif
		}
		sem_acquire_blocking(&refresh_ready);
		if (!global_cfg->on) {
			dma_channel_set_write_addr(col2buf_chan, pxbuf[cur],
						   false);
			dma_channel_transfer_from_buffer_now(
				col2buf_chan, &DARK, global_cfg->nleds);
			frame_intvl_us = 0;
			dma_channel_wait_for_finish_blocking(col2buf_chan);
			/*
			 * XXX this hack ensures that the next DMA
			 * transfer reliably sets the LED strip
			 * dark. Without it, or if the sleep is too
			 * short, the strip stops updating, but
			 * sometimes stays in the current
			 * frame. (fixme)
			 */
			sleep_us(10 * refresh_us);
		}
		dma_channel_set_read_addr(px_chan, pxbuf[cur], true);
		if (frame_intvl_us == 0)
			timeout = at_the_end_of_time;
		else
			timeout = make_timeout_time_us(frame_intvl_us);
		if (hardware_alarm_set_target(frame_alarm_num, timeout)) {
			HTTP_LOG_ERROR("frame alarm not set");
			sem_release(&frame_update);
		}
		cur ^= 1;
		for (int i = 0; i < MAX_SEGMENTS; i++) {
			if (!seg_active(i))
				continue;
			if (!pace(i)) {
				dma_channel_wait_for_finish_blocking(pace_chan);
				dma_channel_set_write_addr(
					pace_chan, pxbuf[cur] + segs[i].start,
					false);
				dma_channel_transfer_from_buffer_now(
					pace_chan, pxbuf[1-cur] + segs[i].start,
					segs[i].len);
				continue;
			}
			fx_tbl[segs[i].fx].fx(
				pxbuf[cur] + segs[i].start, segs + i,
				state + i);
		}
		dma_channel_wait_for_finish_blocking(pace_chan);
	}
}
