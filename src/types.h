/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef __CMD_H
#define __CMD_H

#include <stdint.h>

#include "hardware/flash.h"

#define MAX_SEGMENTS		(16)
#define MAX_SEGMENT_NAME	(32)
#define MAX_PRESET_NAME		(32)
#define MAX_PLAYLIST_NAME	(32)
#define CMDQ_LEN		(4)
#define N_SEG_COLORS		(3)
#define WIFI_PW_MIN_LEN		(8)
#define WIFI_PW_MAX_LEN		(63)
#define SSID_MIN_LEN		(2)
#define SSID_MAX_LEN		(32)
#define MAX_PRESETS		(64)
#define MAX_PLAYLISTS		(64)
#define QL_LEN			(2)
#define PS_PER_PL		(64)

#ifndef AP_DEFAULT_SSID
#define AP_DEFAULT_SSID		"blinkbeern"
#endif

#ifndef AP_DEFAULT_PASS
#define AP_DEFAULT_PASS		"blinkbeern"
#endif

#define AP_DEFAULT_SSID_LEN	(sizeof(AP_DEFAULT_SSID) - 1)
#define AP_DEFAULT_PASS_LEN	(sizeof(AP_DEFAULT_PASS) - 1)

#define GLOBAL_DEFAULT_NLEDS	(1)
#define GLOBAL_DEFAULT_BRI	(127)
#define GLOBAL_DEFAULT_FPS	(127)
#define GLOBAL_DEFAULT_ON	(false)

#define SEG0_DEFAULT_LEN	(1)
#define SEG0_DEFAULT_COLOR	(0x7f)
#define SEG0_DEFAULT_BRI	(127)
#define SEG0_DEFAULT_SX		(127)
#define SEG0_DEFAULT_IX		(127)
#define SEG0_DEFAULT_SEL	(true)

#define SEGMAP_DEFAULT		(0x01)

#define NO_PRESET		(UINT8_MAX)
#define NO_PLAYLIST		(UINT8_MAX)

typedef struct {
	char		pass[WIFI_PW_MAX_LEN];
	char		ssid[SSID_MAX_LEN];
	uint32_t	auth;
	uint32_t	country;
	uint8_t		pass_len;
	uint8_t		ssid_len;
} wifi_creds_t;

typedef struct {
	char		pass[WIFI_PW_MAX_LEN];
	char		ssid[SSID_MAX_LEN];
	uint8_t		pass_len;
	uint8_t		ssid_len;
} ap_creds_t;

typedef struct {
	unsigned	magic;
#define NETWORK_CFG_MAGIC (0xae9cb972)
	wifi_creds_t	wifi;
	ap_creds_t	ap;
} network_cfg_t;

typedef struct global {
	unsigned	magic;
#define GLOBAL_MAGIC	(0x0b892ab5)
	uint16_t	nleds;
	uint8_t		fps_scale;
	uint8_t		playlist;
	uint8_t		bright_scale;
	uint8_t		preset;
	bool		on;
} global_t;

struct segment {
	unsigned	magic;
#define SEG_MAGIC	(0xca41bb26)
	uint32_t	color[N_SEG_COLORS];
	uint16_t	start;
	uint16_t	len;
	uint16_t	palette;
	int16_t		offset;
	uint8_t		fx;
	uint8_t		group;
	uint8_t		space;
	uint8_t		speed_scale;
	uint8_t		intensity;
	uint8_t		bright_scale;
	bool		selected:1;
	bool		reverse:1;
	bool		mirror:1;
};

struct preset {
	unsigned	magic;
#define PRESET_MAGIC	(0x9cd467a5)
	uint16_t	segmap;
	char		ql[QL_LEN];
	bool		ib:1;
	bool		sb:1;
};

struct playlist {
	unsigned	magic;
#define PLAYLIST_MAGIC	(0x0c3be4fe)
	uint16_t	dur[PS_PER_PL];
	uint16_t	transition[PS_PER_PL];
	uint8_t		ps[PS_PER_PL];
	char		ql[QL_LEN];
	uint8_t		nps;
	uint8_t		repeat;
	uint8_t		end;
	bool		shuffle:1;
} __attribute__((aligned(FLASH_SECTOR_SIZE/8)));

union color_pun {
        uint32_t color;
        uint8_t b[4];  /* Little-endian: W=0 B=1 R=2 G=3 */
        struct {
                uint8_t w;
                uint8_t b;
                uint8_t r;
                uint8_t g;
        } rgbw;
};

#endif // __CMD_H
