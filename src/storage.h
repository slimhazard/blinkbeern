/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef __STORAGE_H
#define __STORAGE_H

#include <stdint.h>

#include "pico/platform.h"
#include "pico/sem.h"
#include "hardware/flash.h"

#include "picow_http/assertion.h"

#include "types.h"

typedef struct {
	uint64_t	magic;
#define STORAGE_CFG_MAGIC (0x523563e81a366e75)
	unsigned	version;
} storage_cfg_t;

/* Flash sector size is a power of 2. */
static_assert((FLASH_SECTOR_SIZE & (FLASH_SECTOR_SIZE - 1)) == 0, "");

/*
 * NOTE: The storage version number MUST be bumped on any change that
 * requires re-intialization of existing storage (and preferably ONLY
 * then). Such changes include changes in the storage layout or in the
 * types of the stored objects.
 */
#define STORAGE_VERSION	(5)

#define OFF2ADDR(off)	(XIP_BASE + (off))

extern storage_cfg_t * const storage_cfg;
extern network_cfg_t * const network_cfg;
extern global_t * const global_cfg;
extern uint64_t * const psmap;
extern uint64_t * const plmap;
extern uint16_t * const segmap;

/*
 * As if declared as:
 * struct segment segs[MAX_SEGMENTS];
 */
extern struct segment * const segs;

/*
 * As if declared as:
 * char seg_names[MAX_SEGMENTS][MAX_SEGMENT_NAME];
 */
typedef char seg_name_t[MAX_SEGMENT_NAME];
extern seg_name_t * const seg_names;

/*
 * As if declared as:
 * struct preset ps[MAX_PRESETS];
 */
extern struct preset * const ps;

/*
 * As if declared as:
 * struct segment ps_segs[MAX_PRESETS][MAX_SEGMENTS];
 */
typedef struct segment seg_list_t[MAX_SEGMENTS];
extern seg_list_t * const ps_segs;

/*
 * As if declared as:
 * char ps_seg_names[MAX_PRESETS][MAX_SEGMENTS][MAX_SEGMENT_NAME];
 */
typedef seg_name_t seg_name_list_t[MAX_SEGMENTS];
extern seg_name_list_t * const ps_seg_names;

/*
 * As if declared as:
 * struct playlist pl[MAX_PLAYLISTS];
 */
extern struct playlist * const pl;

/*
 * As if declared as:
 * char ps_names[MAX_PRESETS][MAX_PRESET_NAME];
 */
typedef char ps_name_t[MAX_PRESET_NAME];
extern ps_name_t * const ps_names;

/*
 * As if declared as:
 * char pl_names[MAX_PLAYLISTS][MAX_PLAYLIST_NAME];
 */
extern ps_name_t * const pl_names;

void init_storage(void);
void check_storage(void);
void notify_storage_inited(void);
void store(void *dst, void *src, size_t len);
void store_global(global_t *cfg);
void store_segment(int id, struct segment *sg, uint16_t *sg_map, global_t *cfg);
void store_preset(int id, struct preset *preset, uint64_t *ps_map);
bool wifi_inited(void);
void reset_network_cfg(void);

static inline bool
storage_inited(void)
{
	return VALID_OBJ(storage_cfg, STORAGE_CFG_MAGIC) &&
		storage_cfg->version == STORAGE_VERSION;
}

#endif // __STORAGE_H
