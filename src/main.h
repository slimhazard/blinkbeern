/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef __MAIN_H
#define __MAIN_H

void core1_main(void);
void on_hardfault(void);
void notify_cfg_change(void);

#endif // __MAIN_H
