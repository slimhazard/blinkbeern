/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>
#include <string.h>

#include "pico/multicore.h"
#include "pico/cyw43_arch.h"
#include "pico/time.h"
#include "hardware/flash.h"
#include "hardware/sync.h"

#include "storage.h"

#include "picow_http/log.h"
#include "picow_http/assertion.h"

/* From Varnish vdef.h */
#define PALGN		(sizeof(void *) - 1)		/* size of alignment */
#define PAOK(p)		(((uintptr_t)(p) & PALGN) == 0)		/* is aligned */
#define PRNDDN(p)	((uintptr_t)(p) & ~PALGN)		/* Round down */
#define PRNDUP(p)	(((uintptr_t)(p) + PALGN) & ~PALGN)	/* Round up */

#define PAGE_ALGN	(FLASH_PAGE_SIZE - 1)
#define PAGE_ALGNED(p)	(((uintptr_t)(p) & PAGE_ALGN) == 0)
#define PAGE_RNDDN(p)	((uintptr_t)(p) & ~PAGE_ALGN)
#define PAGE_RNDUP(p)	(((uintptr_t)(p) + PAGE_ALGN) & ~PAGE_ALGN)

#define SECT_ALGN	(FLASH_SECTOR_SIZE - 1)
#define SECT_ALGNED(p)	(((uintptr_t)(p) & SECT_ALGN) == 0)
#define SECT_RNDDN(p)	((uintptr_t)(p) & ~SECT_ALGN)
#define SECT_RNDUP(p)	(((uintptr_t)(p) + SECT_ALGN) & ~SECT_ALGN)

/* Only core1 locks and writes to flash. */
#define LOCKING_CORE	((lock_owner_id_t)1)

#define STORAGE_CFG_OFF	(PICO_FLASH_SIZE_BYTES - FLASH_SECTOR_SIZE)
#define STORAGE_CFG_ADDR (OFF2ADDR(STORAGE_CFG_OFF))
static_assert(PAOK(STORAGE_CFG_ADDR), "");
static_assert(SECT_ALGNED(STORAGE_CFG_ADDR), "");
storage_cfg_t * const storage_cfg = ((storage_cfg_t *)STORAGE_CFG_ADDR);

#define NET_CFG_OFF	(PRNDUP(STORAGE_CFG_OFF + sizeof(*storage_cfg)))
#define NET_CFG_ADDR	(OFF2ADDR(NET_CFG_OFF))
static_assert(PAOK(NET_CFG_ADDR), "");
network_cfg_t * const network_cfg = ((network_cfg_t *)NET_CFG_ADDR);

#define GLOBAL_OFF	(PRNDUP(NET_CFG_OFF + sizeof(*network_cfg)))
#define GLOBAL_CFG_ADDR	(OFF2ADDR(GLOBAL_OFF))
static_assert(PAOK(GLOBAL_CFG_ADDR), "");
global_t * const global_cfg = ((global_t *)GLOBAL_CFG_ADDR);

#define PSMAP_OFF	(PRNDUP(GLOBAL_OFF + sizeof(*global_cfg)))
#define PSMAP_ADDR	(OFF2ADDR(PSMAP_OFF))
static_assert(PAOK(PSMAP_ADDR), "");
uint64_t * const psmap = ((uint64_t *)PSMAP_ADDR);

#define PLMAP_OFF	(PRNDUP(PSMAP_OFF + sizeof(*psmap)))
#define PLMAP_ADDR	(OFF2ADDR(PLMAP_OFF))
static_assert(PAOK(PLMAP_ADDR), "");
uint64_t * const plmap = ((uint64_t *)PLMAP_ADDR);

#define SEGMAP_OFF	(PRNDUP(PLMAP_OFF + sizeof(*plmap)))
#define SEGMAP_ADDR	(OFF2ADDR(SEGMAP_OFF))
static_assert(PAOK(SEGMAP_ADDR), "");
uint16_t * const segmap = ((uint16_t *)SEGMAP_ADDR);

#define SEGS_OFF	(PRNDUP(SEGMAP_OFF + sizeof(*segmap)))
#define SEGS_ADDR	(OFF2ADDR(SEGS_OFF))
static_assert(PAOK(SEGS_ADDR), "");
struct segment * const segs = ((struct segment *)SEGS_ADDR);

#define SEG_NAMES_OFF	(PRNDUP(SEGS_OFF + MAX_SEGMENTS * sizeof(*segs)))
#define SEG_NAMES_ADDR	(OFF2ADDR(SEG_NAMES_OFF))
static_assert(PAOK(SEG_NAMES_ADDR), "");
seg_name_t * const seg_names = ((seg_name_t *)SEG_NAMES_ADDR);

#define PS_OFF	       (PRNDUP(SEG_NAMES_OFF + MAX_SEGMENTS * MAX_SEGMENT_NAME))
#define PS_ADDR	       (OFF2ADDR(PS_OFF))
static_assert(PAOK(PS_ADDR), "");
struct preset * const ps = ((struct preset *)PS_ADDR);

#define TOP_SECTOR_OFF	(STORAGE_CFG_OFF)
#define TOP_SECTOR_ADDR	(OFF2ADDR(TOP_SECTOR_OFF))
#define TOP_SECTOR_PAGE_SZ \
	(PAGE_RNDUP(PS_OFF + MAX_PRESETS * sizeof(*ps)) - STORAGE_CFG_OFF)

static_assert(PS_OFF + MAX_PRESETS * sizeof(*ps) <
	      OFF2ADDR(PICO_FLASH_SIZE_BYTES), "");

#define PS_SEGS_OFF	(SECT_RNDDN(STORAGE_CFG_OFF -			\
				    MAX_PRESETS * MAX_SEGMENTS *	\
				    sizeof(struct segment)))
#define PS_SEGS_ADDR	(OFF2ADDR(PS_SEGS_OFF))
static_assert(PAOK(PS_SEGS_ADDR), "");
static_assert(SECT_ALGNED(PS_SEGS_ADDR), "");
seg_list_t * const ps_segs = ((seg_list_t *)PS_SEGS_ADDR);

#define PS_SEG_NAMES_OFF	\
       (SECT_RNDDN(PS_SEGS_OFF - MAX_PRESETS * MAX_SEGMENTS * MAX_SEGMENT_NAME))
#define PS_SEG_NAMES_ADDR	(OFF2ADDR(PS_SEG_NAMES_OFF))
static_assert(PAOK(PS_SEG_NAMES_ADDR), "");
static_assert(SECT_ALGNED(PS_SEG_NAMES_ADDR), "");
seg_name_list_t * const ps_seg_names = ((seg_name_list_t *)PS_SEG_NAMES_ADDR);

#define PL_OFF		\
	(SECT_RNDDN(PS_SEG_NAMES_OFF - MAX_PLAYLISTS * sizeof(struct playlist)))
#define PL_ADDR		(OFF2ADDR(PL_OFF))
static_assert(PAOK(PL_ADDR), "");
static_assert(SECT_ALGNED(PL_ADDR), "");
struct playlist * const pl = ((struct playlist *)PL_ADDR);

#define PS_NAMES_OFF	(PL_OFF - MAX_PRESETS * MAX_PRESET_NAME)
#define PS_NAMES_ADDR	(OFF2ADDR(PS_NAMES_OFF))
static_assert(PAOK(PS_NAMES_ADDR), "");
ps_name_t * const ps_names = ((ps_name_t *)PS_NAMES_ADDR);

#define PL_NAMES_OFF	\
	(SECT_RNDDN(PS_NAMES_OFF - MAX_PLAYLISTS * MAX_PLAYLIST_NAME))
#define PL_NAMES_ADDR	(OFF2ADDR(PL_NAMES_OFF))
static_assert(PAOK(PL_NAMES_ADDR), "");
static_assert(SECT_ALGNED(PL_NAMES_ADDR), "");
ps_name_t * const pl_names = ((ps_name_t *)PL_NAMES_ADDR);

static char storage_buf[FLASH_SECTOR_SIZE];

static inline uint32_t
addr2off(void *p)
{
	PICOW_HTTP_ASSERT((uintptr_t)p > XIP_BASE);
	return ((uint32_t)((uintptr_t)p - XIP_BASE));
}

static inline void *
buf_off(char *base, size_t off)
{
	return (void *)(storage_buf + off - base);
}

static void
write_storage(void *dst, size_t len)
{
	uint32_t status;

	AN(dst);
	PICOW_HTTP_ASSERT(SECT_ALGNED(dst));
	PICOW_HTTP_ASSERT(PAGE_ALGNED(len));
	PICOW_HTTP_ASSERT(lock_get_caller_owner_id() == LOCKING_CORE);

	if (len == 0)
		return;

	multicore_lockout_start_blocking();
	status = save_and_disable_interrupts();

	flash_range_erase(addr2off(dst), len);
	flash_range_program(addr2off(dst), storage_buf, len);

	restore_interrupts(status);
	multicore_lockout_end_blocking();
}

void
store(void *dst, void *src, size_t len)
{
	char *sector_base;
	ptrdiff_t sector_off;

	AN(dst);
	AN(src);

	if (len == 0)
		return;

	sector_base = (char *)((uintptr_t)SECT_RNDDN(dst));
	sector_off = (char *)dst - sector_base;
	memcpy(storage_buf, sector_base, FLASH_SECTOR_SIZE);
	memcpy(storage_buf + sector_off, src, len);

	write_storage(sector_base, FLASH_SECTOR_SIZE);
}

void
store_global(global_t *cfg)
{
	ptrdiff_t sector_off;
	uint8_t *top_addr = (uint8_t *)TOP_SECTOR_ADDR;

	CHECK_OBJ_NOTNULL(cfg, GLOBAL_MAGIC);

	memcpy(storage_buf, top_addr, TOP_SECTOR_PAGE_SZ);
	sector_off = (uint8_t *)global_cfg - top_addr;
	memcpy(storage_buf + sector_off, cfg, sizeof(*cfg));

	if (cfg->preset != NO_PRESET && cfg->preset != global_cfg->preset) {
		struct segment *sg;
		uint16_t start[MAX_SEGMENTS], len[MAX_SEGMENTS];
		uint8_t bri[MAX_SEGMENTS], ps_id = cfg->preset;

		AN(*psmap & (1ULL << ps_id));
		CHECK_OBJ_NOTNULL(ps + ps_id, PRESET_MAGIC);
		AN(ps_segs + ps_id);
		AN(ps_seg_names + ps_id);

		/* Copy segments.
		 * If preset.sb is false, save the current start and len
		 * config, to be retained after the change.
		 * If preset.ib is false, save the current brightness config.
		 */
		if (!ps[ps_id].sb || !ps[ps_id].ib)
			for (int i = 0; i < MAX_SEGMENTS; i++) {
				if ((*segmap & (1U << i)) == 0)
					continue;
				CHECK_OBJ(segs + i, SEG_MAGIC);
				if (!ps[ps_id].sb) {
					start[i] = segs[i].start;
					len[i] = segs[i].len;
				}
				if (!ps[ps_id].ib)
					bri[i] = segs[i].bright_scale;
			}

		sector_off = (uint8_t *)segs - top_addr;
		memcpy(storage_buf + sector_off, ps_segs[ps_id],
		       MAX_SEGMENTS * sizeof(struct segment));

		/* Now copy back segment bounds and brightness as needed. */
		sg = (struct segment *)(storage_buf + sector_off);
		if (!ps[ps_id].sb || !ps[ps_id].ib)
			for (int i = 0; i < MAX_SEGMENTS; i++) {
				if ((*segmap & (1U << i)) == 0)
					continue;
				CHECK_OBJ(sg + i, SEG_MAGIC);
				if (!ps[ps_id].sb) {
					sg[i].start = start[i];
					sg[i].len = len[i];
				}
				if (!ps[ps_id].ib)
					sg[i].bright_scale = bri[i];
			}

		/* Copy the segment map */
		sector_off = (uint8_t *)segmap - top_addr;
		*((uint16_t *)(storage_buf + sector_off)) = ps[ps_id].segmap;

		/* Copy the segment names */
		sector_off = (uint8_t *)seg_names - top_addr;
		memcpy(storage_buf + sector_off, ps_seg_names[ps_id],
		       MAX_SEGMENTS * MAX_SEGMENT_NAME);
	}

	write_storage(top_addr, TOP_SECTOR_PAGE_SZ);
}

void
store_segment(int id, struct segment *sg, uint16_t *sg_map, global_t *cfg)
{
	ptrdiff_t sector_off;
	char *top_addr = (char *)TOP_SECTOR_ADDR;

	if (sg == NULL && sg_map == NULL && cfg == NULL)
		return;

	memcpy(storage_buf, top_addr, TOP_SECTOR_PAGE_SZ);
	if (sg != NULL) {
		CHECK_OBJ(sg, SEG_MAGIC);
		sector_off = (char *)(segs + id) - top_addr;
		memcpy(storage_buf + sector_off, sg, sizeof(*sg));
	}
	if (sg_map != NULL) {
		sector_off = (char *)segmap - top_addr;
		memcpy(storage_buf + sector_off, sg_map, sizeof(*sg_map));
	}
	if (cfg != NULL) {
		CHECK_OBJ(cfg, GLOBAL_MAGIC);
		sector_off = (char *)global_cfg - top_addr;
		memcpy(storage_buf + sector_off, cfg, sizeof(*cfg));
	}

	write_storage(top_addr, TOP_SECTOR_PAGE_SZ);
}

void
store_preset(int id, struct preset *preset, uint64_t *ps_map)
{
	ptrdiff_t sector_off;
	char *top_addr = (char *)TOP_SECTOR_ADDR;

	if (preset == NULL && ps_map == NULL)
		return;

	memcpy(storage_buf, top_addr, TOP_SECTOR_PAGE_SZ);
	if (preset != NULL) {
		CHECK_OBJ(preset, PRESET_MAGIC);
		sector_off = (char *)(ps + id) - top_addr;
		memcpy(storage_buf + sector_off, preset, sizeof(*preset));
	}
	if (ps_map != NULL) {
		sector_off = (char *)psmap - top_addr;
		memcpy(storage_buf + sector_off, ps_map, sizeof(*ps_map));
	}

	write_storage(top_addr, TOP_SECTOR_PAGE_SZ);
}

void
init_storage(void)
{
	storage_cfg_t *st_cfg;
	network_cfg_t *net_cfg;
	global_t *glbl;
	uint16_t *sgmap;
	struct segment *sg;
	struct preset *pps;
	struct playlist *ppl;
	uint32_t t = to_ms_since_boot(get_absolute_time());

	HTTP_LOG_INFO("Initializing storage ...");

	memset(storage_buf, 0, TOP_SECTOR_PAGE_SZ);
	st_cfg = (storage_cfg_t *)storage_buf;
	st_cfg->magic = STORAGE_CFG_MAGIC;
	st_cfg->version = STORAGE_VERSION;

	net_cfg = (network_cfg_t *)(buf_off((char *)STORAGE_CFG_OFF,
					    NET_CFG_OFF));
	INIT_OBJ(net_cfg, NETWORK_CFG_MAGIC);
	net_cfg->wifi.country = CYW43_COUNTRY_WORLDWIDE;
	net_cfg->wifi.auth = CYW43_AUTH_WPA2_AES_PSK;
	memcpy(net_cfg->ap.ssid, AP_DEFAULT_SSID, AP_DEFAULT_SSID_LEN);
	memcpy(net_cfg->ap.pass, AP_DEFAULT_PASS, AP_DEFAULT_PASS_LEN);
	net_cfg->ap.ssid_len = AP_DEFAULT_SSID_LEN;
	net_cfg->ap.pass_len = AP_DEFAULT_PASS_LEN;

	glbl = (global_t *)(buf_off((char *)STORAGE_CFG_OFF, GLOBAL_OFF));
	INIT_OBJ(glbl, GLOBAL_MAGIC);
	glbl->nleds = GLOBAL_DEFAULT_NLEDS;
	glbl->on = GLOBAL_DEFAULT_ON;
	glbl->bright_scale = GLOBAL_DEFAULT_BRI;
	glbl->fps_scale = GLOBAL_DEFAULT_FPS;
	glbl->playlist = NO_PLAYLIST;
	glbl->preset = NO_PRESET;

	sgmap = (uint16_t *)(buf_off((char *)STORAGE_CFG_OFF, SEGMAP_OFF));
	*sgmap = SEGMAP_DEFAULT;

	*psmap = 0;
	*plmap = 0;

	sg = (struct segment *)(buf_off((char *)STORAGE_CFG_OFF, SEGS_OFF));
	for (int i = 0; i < MAX_SEGMENTS; i++)
		INIT_OBJ(sg + i, SEG_MAGIC);
	sg[0].len = SEG0_DEFAULT_LEN;
	sg[0].color[0] = SEG0_DEFAULT_COLOR;
	sg[0].selected = SEG0_DEFAULT_SEL;
	sg[0].bright_scale = SEG0_DEFAULT_BRI;
	sg[0].speed_scale = SEG0_DEFAULT_SX;
	sg[0].intensity = SEG0_DEFAULT_IX;

	pps = (struct preset *)(buf_off((char *)STORAGE_CFG_OFF, PS_OFF));
	for (int i = 0; i < MAX_PRESETS; i++)
		INIT_OBJ(pps + i, PRESET_MAGIC);
	write_storage(storage_cfg, TOP_SECTOR_PAGE_SZ);

	for (int i = 0; i < MAX_PRESETS; i++) {
		unsigned off =
			(uintptr_t)(ps_segs + i) & (FLASH_SECTOR_SIZE - 1);
		if (SECT_ALGNED(ps_segs + i))
			memset(storage_buf, 0, FLASH_SECTOR_SIZE);
		for (int j = 0; j < MAX_SEGMENTS; j++) {
			sg = (struct segment *)
				((storage_buf + off)
				 + j * sizeof(struct segment));
			INIT_OBJ(sg, SEG_MAGIC);
		}
		if (SECT_ALGNED(ps_segs + i + 1))
			/* store at SECT_RNDDN(ps_segs + i) */
			write_storage((char *)(SECT_RNDDN(ps_segs + i)),
				      FLASH_SECTOR_SIZE);
	}

	for (seg_name_list_t *p = ps_seg_names;
	     p < ps_seg_names + MAX_PRESETS;
	     p += FLASH_SECTOR_SIZE / sizeof(*p)) {
		memset(storage_buf, 0, FLASH_SECTOR_SIZE);
		write_storage(p, FLASH_SECTOR_SIZE);
		/* store at p */
	}

	for (int i = 0; i < MAX_PLAYLISTS; i++) {
		unsigned off =
			(uintptr_t)(pl + i) & (FLASH_SECTOR_SIZE - 1);
		if (SECT_ALGNED(pl + i))
			memset(storage_buf, 0, FLASH_SECTOR_SIZE);
		ppl = (struct playlist *)(storage_buf + off);
		INIT_OBJ(ppl, PLAYLIST_MAGIC);
		if (SECT_ALGNED(pl + i + 1))
			/* store at SECT_RNDDN(pl + i) */
			write_storage((char *)(SECT_RNDDN(pl + i)),
				      FLASH_SECTOR_SIZE);
	}

	memset(storage_buf, 0, FLASH_SECTOR_SIZE);
	write_storage(pl_names, FLASH_SECTOR_SIZE);

	HTTP_LOG_INFO("... storage initialized in %d ms",
		      to_ms_since_boot(get_absolute_time()) - t);
}

void
check_storage(void)
{
	CHECK_OBJ(storage_cfg, STORAGE_CFG_MAGIC);
	CHECK_OBJ(network_cfg, NETWORK_CFG_MAGIC);
	CHECK_OBJ(global_cfg, GLOBAL_MAGIC);
	for (int i = 0; i < MAX_SEGMENTS; i++)
		CHECK_OBJ(&segs[i], SEG_MAGIC);
	for (int i = 0; i < MAX_PRESETS; i++) {
		CHECK_OBJ(&ps[i], PRESET_MAGIC);
		for (int j = 0; j < MAX_SEGMENTS; j++)
			CHECK_OBJ(&ps_segs[i][j], SEG_MAGIC);
	}
	for (int i = 0; i < MAX_PLAYLISTS; i++)
		CHECK_OBJ(&pl[i], PLAYLIST_MAGIC);
}

bool
wifi_inited(void)
{
	if (!VALID_OBJ(network_cfg, NETWORK_CFG_MAGIC))
		return false;
	if (network_cfg->wifi.ssid_len < SSID_MIN_LEN)
		return false;
	if (network_cfg->wifi.auth == CYW43_AUTH_OPEN)
		return true;
	if (network_cfg->wifi.pass_len < WIFI_PW_MIN_LEN)
		return false;
	return true;
}

void
reset_network_cfg(void)
{
	network_cfg_t cfg;

	INIT_OBJ(&cfg, NETWORK_CFG_MAGIC);
	cfg.wifi.country = CYW43_COUNTRY_WORLDWIDE;
	cfg.wifi.auth = CYW43_AUTH_WPA2_AES_PSK;
	memcpy(cfg.ap.ssid, AP_DEFAULT_SSID, AP_DEFAULT_SSID_LEN);
	memcpy(cfg.ap.pass, AP_DEFAULT_PASS, AP_DEFAULT_PASS_LEN);
	cfg.ap.ssid_len = AP_DEFAULT_SSID_LEN;
	cfg.ap.pass_len = AP_DEFAULT_PASS_LEN;
	store(network_cfg, &cfg, sizeof(cfg));
}
