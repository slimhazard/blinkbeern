/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "hardware/dma.h"
#include "hardware/interp.h"
#include "hardware/divider.h"
#include "pico/divider.h"

#include "picow_http/log.h"
#include "picow_http/assertion.h"

#include "types.h"
#include "rnd.h"
#include "fx_tbl.h"
#include "sine.h"
#include "palettes.h"
#include "gamma.h"

/*
 * The largest number of divisors for values <= MAX_PIXELS
 * (2520 has 48 divisors).
 */
#define MAX_DIVISORS (48)

const uint32_t DARK = 0;
unsigned col2buf_chan, buf2buf_chan;

struct wave_params {
	uint32_t	alpha_diff;
	uint16_t	waveln;
};

static inline uint32_t
scale(uint32_t color, uint8_t scale)
{
	uint32_t c1 = color & 0x00ff00ff;
	uint32_t c2 = (color >> 8) & 0x00ff00ff;
	c1 *= scale;
	c2 *= scale;
	return (((c1 >> 8) & 0x00ff00ff) | (c2 & 0xff00ff00));
}

static inline union color_pun
grb2grbw(union color_pun color)
{
	uint8_t min = 0xff;

	for (int i = 1; i < 4; i++)
		if (color.b[i] < min)
			min = color.b[i];
	for (int i = 1; i < 4; i++)
		color.b[i] -= min;
	color.b[0] = min;
	return (color);
}

static inline union color_pun
rnd_color(void)
{
	union color_pun c;

	c.color = rnd32_range(0x808080, 0xffffff) << 8;
	return (grb2grbw(gamma_correct(c)));
}

static inline void
mirror(uint32_t *buf, const struct segment *seg)
{
	for (int i = 0, j = seg->len - 1; i < j; i++, j--) {
		uint32_t temp = buf[i];
		buf[i] = buf[j];
		buf[j] = temp;
	}
}

static void
rotate_segbuf(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	dma_channel_set_write_addr(buf2buf_chan, buf, false);
	dma_channel_transfer_from_buffer_now(buf2buf_chan,
					     state->segbuf[0] + state->counter,
					     seg->len);
	if (state->counter >= seg->len - 1 && state->step > 0)
		state->counter = 0;
	else if (state->counter <= 0 && state->step < 0)
		state->counter = seg->len - 1;
	else
		state->counter += state->step;
	dma_channel_wait_for_finish_blocking(buf2buf_chan);
}

void
init_generic(const struct global *global, const struct segment *seg,
	     struct fx_state *state)
{
	state->counter = 0;
	state->palette = palettes[seg->palette];
	if (seg->reverse)
		state->step = -1;
	else
		state->step = 1;
	if (global->bright_scale != UINT8_MAX || seg->bright_scale != UINT8_MAX)
		state->bri = (global->bright_scale * seg->bright_scale) >> 8;
	else
		state->bri = UINT8_MAX;
}

void
init_static(const struct global *global, const struct segment *seg,
	    struct fx_state *state)
{
	init_generic(global, seg, state);
	if (state->bri != UINT8_MAX)
		state->color[0] = scale(seg->color[0], state->bri);
	else
		state->color[0] = seg->color[0];
}

void
fx_static(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	dma_channel_set_write_addr(col2buf_chan, buf, false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, state->color,
					     seg->len);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
}

void
init_snakes(const struct global *global, const struct segment *seg,
	    struct fx_state *state)
{
	int len = seg->group, buflen = seg->len * 2;
	uint32_t color[3];

	init_generic(global, seg, state);
	if (len == 0)
		len = 1;
	if (seg->mirror || seg->reverse) {
		state->counter = seg->len - 1;
		state->step = -1;
	}
	else {
		state->counter = 0;
		state->step = 1;
	}
	if (state->bri != UINT8_MAX)
		for (int i = 0; i < 3; i++)
			color[i] = scale(seg->color[i], state->bri);
	else
		memcpy(color, seg->color, 3 * sizeof(uint32_t));
	for (int i = 0, c = 0; i < buflen; i += len) {
		int l = len;

		if (i + l > buflen)
			l = buflen - i;
		dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] + i,
					   false);
		dma_channel_transfer_from_buffer_now(col2buf_chan, color + c,
						     l);
		c++;
		if (c == 3)
			c = 0;
		dma_channel_wait_for_finish_blocking(col2buf_chan);
	}
}

void
fx_snakes(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}

void
fx_random(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	for (int i = 0; i < seg->len; ++i) {
		int r = get_rand_32();
		uint8_t color = (uint8_t)(r >> 24);
		if (state->bri != UINT8_MAX)
			color = (state->bri * color) >> 8;
		int shift = (r & 0x030000) >> 16;
		buf[i] = color << (shift * 8);
	}
}

void
fx_random_rgbw(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	union color_pun c;

	for (int i = 0; i < seg->len; ++i) {
		c = rnd_color();
		if (state->bri != UINT8_MAX)
			c.color = scale(c.color, state->bri);
		buf[i] = c.color;
	}
}

/* intensity scales the probability of a sparkle (color[1]) from 0 to 3/16. */
void
init_sparkle(const struct global *global, const struct segment *seg,
	     struct fx_state *state)
{
	init_generic(global, seg, state);
	if (seg->intensity == UINT8_MAX)
		state->scale = 0x30000000;
	else
		state->scale = __fast_mul(seg->intensity, 0x300000);
	if (state->bri == UINT8_MAX) {
		state->color[0] = seg->color[0];
		state->color[1] = seg->color[1];
	}
	else {
		state->color[0] = scale(seg->color[0], state->bri);
		state->color[1] = scale(seg->color[1], state->bri);
	}
}

void
fx_sparkle(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	for (int i = 0; i < seg->len; ++i) {
		if (get_rand_32() > state->scale)
			buf[i] = state->color[0];
		else
			buf[i] = state->color[1];
	}
}

static interp_hw_save_t interp_greys;

/*
 * Generate a ramp of byte values from 0 to the brightness scale (or
 * the other way around if reversed). The RGBW values are all set to
 * those byte values.
 * ix determines the length of the ramp, from 2 to 255. Short lengths
 * for high intensities, long lengths for low intensities.
 * So we use the interpolator to set values along the ramp.
 */
void
init_greys(const struct global *global, const struct segment *seg,
	   struct fx_state *state)
{
	interp_config cfg;
	uint8_t start = 0, len = 257 - seg->intensity;

	if (seg->intensity <= 2)
		len = 255;
	init_generic(global, seg, state);
	hw_divider_divmod_u32_start(0xff000000, len);

	cfg = interp_default_config();
	interp_config_set_blend(&cfg, true);
	interp_set_config(interp0, 0, &cfg);
	cfg = interp_default_config();
	interp_config_set_shift(&cfg, 24);
	interp_set_config(interp0, 1, &cfg);
	/*
	 * scale is the increment for blend mode's alpha value, as an
	 * 8.24 fixed point value.
	 */
	state->scale = hw_divider_u32_quotient_wait() + 1;
	if (seg->reverse || seg->mirror) {
		interp0->accum[1] = 0xff000000;
		start = state->bri;
		state->step = -1;
	}
	else {
		interp0->accum[1] = 0;
		state->step = 1;
	}

	interp0->base[0] = start;
	interp0->base[1] = state->bri - start;
	interp_save(interp0, &interp_greys);
}

void
fx_greys(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	int32_t prev;

	interp_restore(interp0, &interp_greys);
	prev = interp0->accum[1];
	for (int i = 0; i < seg->len; i++) {
		buf[i] = __fast_mul(interp0->peek[1], 0x01010101);
		interp0->add_raw[1] = state->scale;
	}
	interp0->accum[1] = prev;
	interp0->add_raw[1] = state->scale * state->step;
	interp_save(interp0, &interp_greys);
}

void
init_fade(const struct global *global, const struct segment *seg,
	  struct fx_state *state)
{
	init_generic(global, seg, state);
	state->step = 1;
	state->scale = (int)rnd32_nxt_bounded(3);
	state->shift = state->scale << 3;
}

void
fx_fade(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	uint32_t color = state->counter << state->shift;

	if (state->bri != UINT8_MAX)
		color = scale(color, state->bri);
	dma_channel_set_write_addr(col2buf_chan, buf, false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &color, seg->len);
	state->counter += state->step;
	state->counter &= 0xff;
	if (state->counter == 0) {
		state->step = 1;
		state->scale++;
		state->scale &= 3;
		state->shift = state->scale << 3;
	}
	else if (state->counter == 0xff)
		state->step = -1;
	dma_channel_wait_for_finish_blocking(col2buf_chan);
}

void
init_wipe(const struct global *global, const struct segment *seg,
	  struct fx_state *state)
{
	init_generic(global, seg, state);
	if (seg->reverse || seg->mirror) {
		state->counter = seg->len;
		state->step = -1;
		state->color[0] = seg->color[1];
		state->color[1] = seg->color[0];
	}
	else {
		state->counter = 0;
		state->step = 1;
		state->color[0] = seg->color[0];
		state->color[1] = seg->color[1];
	}
	if (state->bri != UINT8_MAX) {
		state->color[0] = scale(state->color[0], state->bri);
		state->color[1] = scale(state->color[1], state->bri);
	}
}

void
fx_wipe(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	if (state->counter > 0) {
		dma_channel_set_write_addr(col2buf_chan, buf, false);
		dma_channel_transfer_from_buffer_now(col2buf_chan,
						     state->color,
						     state->counter);
	}
	if (state->counter < seg->len) {
		dma_channel_wait_for_finish_blocking(col2buf_chan);
		dma_channel_set_write_addr(col2buf_chan, buf + state->counter,
					   false);
		dma_channel_transfer_from_buffer_now(col2buf_chan,
						     state->color + 1,
						     seg->len - state->counter);
	}

	state->counter += state->step;
	if (state->counter == 0 || state->counter == seg->len)
		state->step = -state->step;
	dma_channel_wait_for_finish_blocking(col2buf_chan);
}

void
init_chase(const struct global *global, const struct segment *seg,
	   struct fx_state *state)
{
	uint8_t grp = seg->group, spc = seg->space;
	int buflen = seg->len * 2;
	uint32_t color[2];

	init_generic(global, seg, state);
	if (grp == 0)
		grp = 1;
	if (spc == 0)
		spc = 1;
	if (seg->mirror || seg->reverse) {
		state->counter = seg->len - 1;
		state->step = -1;
	}
	else {
		state->counter = 0;
		state->step = 1;
	}
	if (state->bri != UINT8_MAX) {
		color[0] = scale(seg->color[0], state->bri);
		color[1] = scale(seg->color[1], state->bri);
	}
	else
		memcpy(color, seg->color, 2 * sizeof(uint32_t));
	for (int i = 0; i < buflen; i += grp + spc) {
		if (i + grp > buflen)
			grp = buflen - i;
		dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] + i,
					   false);
		dma_channel_transfer_from_buffer_now(col2buf_chan, color, grp);
		if (i + grp + spc > buflen)
			spc = buflen + grp - i;
		dma_channel_wait_for_finish_blocking(col2buf_chan);
		dma_channel_set_write_addr(col2buf_chan,
					   state->segbuf[0] + i + grp, false);
		dma_channel_transfer_from_buffer_now(col2buf_chan, color + 1,
						     spc);
		dma_channel_wait_for_finish_blocking(col2buf_chan);
	}
}

void
fx_chase(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}

void
fx_scale(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	static uint8_t counter = 0;

	uint8_t val = counter;
	for (int i = 0; i < seg->len; i++) {
		buf[i] = scale(seg->color[0], val);
		val += state->step;
	}
	counter += state->step;
}

static void
setup_interp_blend(void)
{
	static bool saved = false;
	static interp_hw_save_t saver0, saver1;
	interp_config cfg;

	if (saved) {
		interp_restore(interp0, &saver0);
		interp_restore(interp1, &saver1);
		return;
	}
	cfg = interp_default_config();
	interp_config_set_blend(&cfg, true);
	interp_set_config(interp0, 0, &cfg);

	cfg = interp_default_config();
	interp_config_set_shift(&cfg, 24);
	interp_config_set_mask(&cfg, 0, 7);
	interp_set_config(interp0, 1, &cfg);
	interp_save(interp0, &saver0);

	cfg = interp_default_config();
	interp_set_config(interp1, 0, &cfg);
	interp_set_config(interp1, 1, &cfg);
	interp_save(interp1, &saver1);
	saved = true;
}

static inline void
setup_blend_iteration(uint8_t color0, uint8_t color1, uint32_t alpha_diff)
{
	interp0->base[0] = color0;
	interp0->base[1] = color1;
	interp0->accum[1] = 0;
	interp1->accum[0] = 0;
	interp1->base[0] = alpha_diff;
}

static inline void
blend_iterate(struct fx_state *state, int shift, int idx)
{
	uint32_t c = interp0->peek[1] << shift;
	state->segbuf[0][idx] |= c;
	state->segbuf[1][idx] |= c;
	interp0->accum[1] = interp1->pop[0];
}

static inline unsigned
lg(unsigned x)
{
        AN(x);
        return (31 - __builtin_clz(x));
}

unsigned
isqrt(unsigned n)
{
        if (n <= 1)
                return (n);

        unsigned x0 = 1U << ((lg(n) / 2) + 1);
        for (unsigned x1 = (x0 + n/x0) / 2; x1 < x0;) {
                x0 = x1;
                x1 = (x0 + n/x0) / 2;
        }
        return (x0);
}

static int
cmp16(const void *a, const void *b)
{
	return (int)(*(int16_t *)a - *(int16_t *)b);
}

static void
init_waveform(const struct global *global, const struct segment *seg,
	      struct fx_state *state, uint16_t min, struct wave_params *params)
{
	uint8_t ix = UINT8_MAX - seg->intensity;

	init_generic(global, seg, state);

	if (ix == UINT8_MAX)
		params->waveln = seg->len;
	else {
		uint16_t divisors[MAX_DIVISORS];
		divmod_result_t div;
		size_t ndivs = 1;
		uint32_t s = (uint32_t)isqrt(seg->len);

		divisors[0] = seg->len;
		for (uint32_t i = 2; i < min; i++) {
			divmod_result_t div = divmod_u32u32((uint32_t)seg->len,
							    i);
			if (to_remainder_u32(div) == 0)
				divisors[ndivs++] =
					(uint16_t)to_quotient_u32(div);
		}
		for (uint32_t i = min; i <= s; i++) {
			divmod_result_t div = divmod_u32u32((uint32_t)seg->len,
							    i);
			if (to_remainder_u32(div) == 0) {
				divisors[ndivs] = (uint16_t)i;
				divisors[ndivs + 1] =
					(uint16_t)to_quotient_u32(div);
				ndivs += 2;
			}
		}
		qsort(divisors, ndivs, sizeof(uint16_t), cmp16);
		params->waveln = divisors[(ix * ndivs) >> 8];
	}

	hw_divider_divmod_u32_start(UINT32_MAX, (uint32_t)params->waveln - 1);
	setup_interp_blend();
	if (seg->mirror || seg->reverse) {
		state->counter = seg->len - 1;
		state->step = -1;
	}
	else {
		state->counter = 0;
		state->step = 1;
	}
	if (state->bri == UINT8_MAX)
		state->color[0] = seg->color[0];
	else
		state->color[0] = scale(seg->color[0], state->bri);
	params->alpha_diff = hw_divider_u32_quotient_wait();
}

void
init_running(const struct global *global, const struct segment *seg,
	     struct fx_state *state)
{
	struct wave_params params;

	init_waveform(global, seg, state, 4, &params);
	for (int i = 0; i < seg->len * 2; i += params.waveln) {
		setup_blend_iteration(0, UINT8_MAX, params.alpha_diff);
		for (int w = 0; w < params.waveln; w++) {
			union color_pun c, g;

			c.color = scale(state->color[0],
					sin_u8(interp0->peek[1]));
			g = gamma_correct(c);
			state->segbuf[0][i + w] = g.color;
			interp0->accum[1] = interp1->pop[0];
		}
	}
}

void
fx_running(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}

void
init_sawtooth(const struct global *global, const struct segment *seg,
	      struct fx_state *state)
{
	struct wave_params params;

	init_waveform(global, seg, state, 2, &params);
	for (int i = 0; i < seg->len * 2; i += params.waveln) {
		setup_blend_iteration(0, UINT8_MAX, params.alpha_diff);
		for (int w = 0; w < params.waveln; w++) {
			union color_pun c, g;

			c.color = scale(state->color[0], interp0->peek[1]);
			g = gamma_correct(c);
			state->segbuf[0][i + w] = g.color;
			interp0->accum[1] = interp1->pop[0];
		}
	}
}

void
fx_sawtooth(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}

void
init_square(const struct global *global, const struct segment *seg,
	    struct fx_state *state)
{
	struct wave_params params;
	bool dark = false;

	init_waveform(global, seg, state, 1, &params);
	for (int i = 0; i < seg->len * 2; i += params.waveln) {
		const uint32_t *color = dark ? &DARK : state->color;
		dma_channel_wait_for_finish_blocking(col2buf_chan);
		dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] + i,
					   false);
		dma_channel_transfer_from_buffer_now(col2buf_chan, color,
						     params.waveln);
		dark = !dark;
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
}

void
fx_square(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}

void
init_triangle(const struct global *global, const struct segment *seg,
	      struct fx_state *state)
{
	struct wave_params params;

	init_waveform(global, seg, state, 3, &params);
	for (int i = 0; i < seg->len * 2; i += params.waveln) {
		setup_blend_iteration(0, UINT8_MAX, params.alpha_diff << 1);
		for (int w = 0, l = params.waveln - 1; w < params.waveln >> 1;
		     w++, l -= 2) {
			union color_pun c, g;

			c.color = scale(state->color[0], interp0->peek[1]);
			g = gamma_correct(c);
			state->segbuf[0][i + w] = g.color;
			state->segbuf[0][i + w + l] = g.color;
			interp0->accum[1] = interp1->pop[0];
		}
	}
}

void
fx_triangle(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}

static void
init_pal_col12(const struct segment *seg, struct fx_state *state)
{
	union color_pun c0, c1;
	uint32_t alpha_diff;

	hw_divider_divmod_u32_start(UINT32_MAX, (uint32_t)(seg->len >> 1) - 1);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, 2 * seg->len);
	setup_interp_blend();

	if (state->bri == UINT8_MAX) {
		c0.color = seg->color[0];
		c1.color = seg->color[1];
	}
	else {
		c0.color = scale(seg->color[0], state->bri);
		c1.color = scale(seg->color[1], state->bri);
	}
	if (seg->mirror) {
		union color_pun tmp = c0;
		c0 = c1;
		c1 = tmp;
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	alpha_diff = hw_divider_u32_quotient_wait();

	for (int i = 0; i < 4; i++) {
		int shift = i << 3;

		setup_blend_iteration(c0.b[i], c1.b[i], alpha_diff);
		for (unsigned j = 0; j < seg->len >> 1; j++) {
			uint32_t c = interp0->peek[1] << shift;
			state->segbuf[0][j] |= c;
			state->segbuf[1][j] |= c;
			state->segbuf[0][seg->len - j - 1] |= c;
			state->segbuf[1][seg->len - j - 1] |= c;
			interp0->accum[1] = interp1->pop[0];
		}
	}
}

static void
init_pal_gradient(const struct segment *seg, struct fx_state *state)
{
	union color_pun c0, c1, c2;
	uint32_t alpha_diff, seg_third;

	hw_divider_divmod_u32_start(seg->len, 3);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, 2 * seg->len);
	setup_interp_blend();
	seg_third = hw_divider_u32_quotient_wait();
	hw_divider_divmod_u32_start(UINT32_MAX, seg_third);

	if (state->bri == UINT8_MAX) {
		c0.color = seg->color[0];
		c1.color = seg->color[1];
		c2.color = seg->color[2];
	}
	else {
		c0.color = scale(seg->color[0], state->bri);
		c1.color = scale(seg->color[1], state->bri);
		c2.color = scale(seg->color[2], state->bri);
	}
	if (seg->mirror) {
		union color_pun tmp = c0;
		c0 = c2;
		c2 = tmp;
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	alpha_diff = hw_divider_u32_quotient_wait();

	for (int i = 0; i < 4; i++) {
		int shift = i << 3;

		setup_blend_iteration(c0.b[i], c1.b[i], alpha_diff);
		for (unsigned j = 0; j < seg_third; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c1.b[i], c2.b[i], alpha_diff);
		for (unsigned j = seg_third; j < seg_third * 2; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c2.b[i], c0.b[i], alpha_diff);
		for (unsigned j = seg_third * 2; j < seg->len; j++)
			blend_iterate(state, shift, j);
	}
}

static void
init_pal_only2(const struct segment *seg, struct fx_state *state)
{
	union color_pun c0, c1;
	/* len_short = 1/16 * seg->len, len_long = 7/16 * seg->len */
	int len_long = (7 * seg->len) >> 4,
		len_short = (seg->len - (len_long << 1)) >> 1;
	uint32_t alpha_diff;

	hw_divider_divmod_u32_start(UINT32_MAX, len_short);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] + len_long,
				   false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, len_short);

	if (state->bri == UINT8_MAX) {
		c0.color = seg->color[0];
		c1.color = seg->color[1];
	}
	else {
		c0.color = scale(seg->color[0], state->bri);
		c1.color = scale(seg->color[1], state->bri);
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[1] + len_long,
				   true);
	if (seg->mirror) {
		union color_pun tmp = c0;
		c0 = c1;
		c1 = tmp;
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	alpha_diff = hw_divider_u32_quotient_wait();

	/* 7/16 color0 */
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &c0.color, len_long);
	setup_interp_blend();
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[1], true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/16 color0->color1, set to DARK above */

	/* 7/16 color1 */
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[0] + len_long + len_short,
				   false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &c1.color, len_long);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[1] + len_long + len_short,
				   true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/16 color1->color0 */
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[0] + 2 * len_long + len_short,
				   false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, len_short);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[1] + 2 * len_long + len_short,
				   true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	for (int i = 0; i < 4; i++) {
		int shift = i << 3;

		setup_blend_iteration(c0.b[i], c1.b[i], alpha_diff);
		for (int j = 0; j < len_short; j++) {
			uint32_t c = interp0->peek[1] << shift;
			state->segbuf[0][j + len_long] |= c;
			state->segbuf[0][seg->len - 1 - j] |= c;
			state->segbuf[1][j + len_long] |= c;
			state->segbuf[1][seg->len - 1 - j] |= c;
			interp0->accum[1] = interp1->pop[0];
		}
	}
}

static void
init_pal_only3(const struct segment *seg, struct fx_state *state)
{
	union color_pun c0, c1, c2;
	int len_short = seg->len >> 4, len_long = seg->len >> 2;
	uint32_t alpha_diff;

	hw_divider_divmod_u32_start(UINT32_MAX, len_short);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] + len_long,
				   false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, len_short);

	if (state->bri == UINT8_MAX) {
		c0.color = seg->color[0];
		c1.color = seg->color[1];
		c2.color = seg->color[2];
	}
	else {
		c0.color = scale(seg->color[0], state->bri);
		c1.color = scale(seg->color[1], state->bri);
		c2.color = scale(seg->color[2], state->bri);
	}
	if (seg->mirror) {
		union color_pun tmp = c0;
		c0 = c2;
		c2 = tmp;
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[1] + len_long,
				   true);
	alpha_diff = hw_divider_u32_quotient_wait();
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/4 color0 */
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &c0.color, len_long);
	setup_interp_blend();
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[1], true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/16 color0->color1, set to DARK above */

	/* 1/4 color1 */
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[0] + len_long + len_short,
				   false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &c1.color, len_long);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[1] + len_long + len_short,
				   true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/16 color1->color2 */
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[0] + 2 * len_long + len_short,
				   false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, len_short);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan,
				   state->segbuf[1] + 2 * len_long + len_short,
				   true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/4 color2 */
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] +
				   2 * len_long + 2 * len_short, false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &c2.color, len_long);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] +
				   2 * len_long + 2 * len_short, true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/16 color2->color0 */
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] +
				   3 * len_long + 2 * len_short, false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, len_short);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[1] +
				   3 * len_long + 2 * len_short, true);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	/* 1/16 color0 */
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0] +
				   3 * len_long + 3 * len_short, false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &c0.color,
					     len_short);
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[1] +
				   3 * len_long + 3 * len_short, false);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	for (int i = 0; i < 4; i++) {
		int shift = i << 3;

		setup_blend_iteration(c0.b[i], c1.b[i], alpha_diff);
		for (int j = len_long; j < len_long + len_short; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c1.b[i], c2.b[i], alpha_diff);
		for (int j = 2 * len_long + len_short;
		     j <  2 * len_long + 2 * len_short; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c2.b[i], c0.b[i], alpha_diff);
		for (int j = 3 * len_long + 2 * len_short;
		     j < 3 * len_long + 3 * len_short; j++)
			blend_iterate(state, shift, j);
	}
}

static void
init_pal_random(const struct segment *seg, struct fx_state *state)
{
	union color_pun c0, c1, c2, c3;
	int len = seg->len >> 2;
	uint32_t alpha_diff;

	hw_divider_divmod_u32_start(UINT32_MAX, len);
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, seg->len * 2);
	setup_interp_blend();
	c0 = rnd_color();
	c1 = rnd_color();
	c2 = rnd_color();
	c3 = rnd_color();
	if (state->bri != UINT8_MAX) {
		c0.color = scale(c0.color, state->bri);
		c1.color = scale(c1.color, state->bri);
		c2.color = scale(c2.color, state->bri);
		c3.color = scale(c3.color, state->bri);
	}
	dma_channel_wait_for_finish_blocking(col2buf_chan);
	alpha_diff = hw_divider_u32_quotient_wait();

	for (int i = 0; i < 4; i++) {
		int shift = i << 3;

		setup_blend_iteration(c0.b[i], c1.b[i], alpha_diff);
		for (int j = 0; j < len; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c1.b[i], c2.b[i], alpha_diff);
		for (int j = len; j < 2 * len; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c2.b[i], c3.b[i], alpha_diff);
		for (int j = 2 * len; j < 3 * len; j++)
			blend_iterate(state, shift, j);

		setup_blend_iteration(c3.b[i], c0.b[i], alpha_diff);
		for (int j = 3 * len; j < seg->len; j++)
			blend_iterate(state, shift, j);
	}
	state->deadline = make_timeout_time_ms(
		1000 + __fast_mul(255 - seg->intensity, 100));
}

void
init_palette(const struct global *global, const struct segment *seg,
	     struct fx_state *state)
{
	init_generic(global, seg, state);
	if (state->palette != NULL) {
		const struct pal_anchors * const anx = state->palette->anx;
		const struct pal_colors * const colors = state->palette->colors;

		dma_channel_set_write_addr(col2buf_chan, state->segbuf[0],
					   false);
		dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK,
						     2 * seg->len);
		setup_interp_blend();
		dma_channel_wait_for_finish_blocking(col2buf_chan);

		for (int i = 0; i < anx->nanx - 1; i++) {
			union color_pun c0, c1;
			uint16_t s0, s1;
			uint32_t alpha_diff;
			unsigned l;

			s0 = (anx->anchor[i]   * seg->len + 128) >> 8;
			s1 = (anx->anchor[i+1] * seg->len + 128) >> 8;
			if (s1 > seg->len)
				s1 = seg->len;
			if (s0 == s1)
				continue;
			l = s1 - s0 - 1;
			hw_divider_divmod_u32_start(UINT32_MAX, l);
			if (i == 0)
				c0.color = colors->first;
			else
				c0.color = colors->color[i - 1];
			c1.color = colors->color[i];
			if (state->bri != UINT8_MAX) {
				c0.color = scale(c0.color, state->bri);
				c1.color = scale(c1.color, state->bri);
			}
			alpha_diff = hw_divider_u32_quotient_wait();

			for (int j = 0; j < 4; j++) {
				int shift = j << 3;

				setup_blend_iteration(c0.b[j], c1.b[j],
						       alpha_diff);
				for (unsigned s = s0; s < s1; s++)
					blend_iterate(state, shift, s);
			}
		}
		if (seg->mirror)
			mirror(state->segbuf[0], seg);
		return;
	}

	switch (seg->palette) {
	case PAL_DYN_COLOR1:
		init_static(global, seg, state);
		return;
	case PAL_DYN_COLOR12:
		init_pal_col12(seg, state);
		return;
	case PAL_DYN_COL_GRAD:
		init_pal_gradient(seg, state);
		return;
	case PAL_DYN_COL_ONLY:
		if (seg->color[2] == 0)
			init_pal_only2(seg, state);
		else
			init_pal_only3(seg, state);
		return;
	case PAL_DYN_RANDOM:
		init_pal_random(seg, state);
		return;
	default:
		panic("Invalid palette number: %u", seg->palette);
	}
}

void
fx_palette(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	if (state->palette != NULL) {
		rotate_segbuf(buf, seg, state);
		return;
	}
	if (seg->palette == PAL_DYN_COLOR1) {
		fx_static(buf, seg, state);
		return;
	}
	rotate_segbuf(buf, seg, state);
	if (seg->palette != PAL_DYN_RANDOM ||
	    absolute_time_diff_us(get_absolute_time(), state->deadline) >= 0)
		return;
	init_pal_random(seg, state);
}

#if 0
/*
 * Compute 6t^5 - 15t^4 + 10t^3 using Q16.16.
 * Returns the result in [0.0, 1.0] scaled to integer [0, 255].
 */
static inline uint8_t
quintic_scale(uint32_t t)
{
	uint32_t q = t * 6 - 0xf0000;
	q = (q * t) >> 16;
	q += 0xa0000;
	q = (q * t) >> 16;
	q = (q * t) >> 16;
	q = (q * t) >> 16;
	return ((q + 0x80) >> 8);
}

void
init_noise(const struct global *global, const struct segment *seg,
	   struct fx_state *state)
{
	uint16_t len8 = seg->len >> 3;
	uint32_t d = len8 + ((seg->intensity * ((4 - len8))) >> 8);
	uint32_t d2, dinv;
	uint8_t *q, start;
	unsigned i;

	hw_divider_divmod_u32_start(0x10000, d);
	q = malloc(d);
	if (q == NULL)
		panic("Out of memory for quintic parameters");
	d2 = d >> 1;
	setup_interp_blend();
	start = rnd32_nxt_bounded(255);
	interp0->base[0] = start;
	dinv = hw_divider_u32_quotient_wait();

	for (i = 0; i < d2; i++)
		q[i] = quintic_scale(i * dinv);
	if (d & 1)
		q[d2] = quintic_scale(d2 * dinv);
	for (i = d2; i < d; i++)
		q[i] = 255 - q[d - i - 1];

	init_palette(global, seg, state);
	for (i = 0; i + d < 2 * seg->len; i++) {
		interp0->base[1] = rnd32_nxt_bounded(255);
		for (unsigned j = 0; j < d; i++, j++) {
			interp0->accum[1] = q[j];
			if (seg->palette == PAL_DYN_COLOR1) {
				state->segbuf[0][i] = scale(seg->color[0],
							    interp0->peek[1]);
				continue;
			}
			state->segbuf[0][i] = state->dyn_pal[interp0->peek[1]];
		}
		interp0->base[0] = interp0->base[1];
	}
	d = 2 * seg->len - i;
	hw_divider_divmod_u32_start(0x10000, d);
	free(q);
	interp0->base[1] = start;
	dinv = hw_divider_u32_quotient_wait();
	for (int j = 0; i < 2 * seg->len; i++, j++) {
		interp0->accum[j] = quintic_scale(j * dinv);
		if (seg->palette == PAL_DYN_COLOR1)
			state->segbuf[0][i] = scale(seg->color[0],
						    interp0->peek[1]);
		else
			state->segbuf[0][i] = state->dyn_pal[interp0->peek[1]];
		interp0->add_raw[1] = d;
	}
}

void
fx_noise(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	rotate_segbuf(buf, seg, state);
}
#endif

static uint16_t
seg_rand_point(const struct segment *seg)
{
	uint8_t rp = (uint8_t)(get_rand_32() >> 24);
	uint32_t p = (rp * seg->len + 128) >> 8;
	if (p >= seg->len)
		p = seg->len - 1;
	return (uint16_t)p;
}

static uint32_t
color_blend2(union color_pun c0, union color_pun c1, uint32_t alpha_diff)
{
	uint32_t color = 0;

	setup_interp_blend();
	for (int i = 0; i < 4; i++) {
		int shift = i << 3;

		interp0->base[0] = c0.b[i];
		interp0->base[1] = c1.b[i];
		interp0->accum[1] = alpha_diff;
		color |= interp0->peek[1] << shift;
	}
	return color;
}

static uint32_t
pal_rand_color(struct fx_state *state)
{
	uint32_t color = 0, alpha_diff;
	uint8_t rc = (uint8_t)(get_rand_32() >> 24), diff, len;
	union color_pun c0, c1;

	AN(state->palette);
	const struct pal_anchors * const anx = state->palette->anx;
	const struct pal_colors * const colors = state->palette->colors;
	uint8_t lo = 0, hi = anx->nanx - 1;

	while (lo != hi) {
		uint8_t mid = (lo + hi) / 2, midVal = anx->anchor[mid];

		if (midVal == rc) {
			lo = hi = mid;
			break;
		}
		else if (midVal < rc) {
			if (anx->anchor[mid + 1] <= rc) {
				lo = mid + 1;
				continue;
			}
			lo = mid;
			hi = mid + 1;
			break;
		}
		else if (anx->anchor[mid - 1] >= rc) {
			hi = mid - 1;
			continue;
		}
		lo = mid - 1;
		hi = mid;
		break;
	}

	if (lo == hi) {
		if (lo == 0)
			color = colors->first;
		else
			color = colors->color[lo - 1];
		return color;
	}

	len = anx->anchor[hi] - anx->anchor[lo];
	hw_divider_divmod_u32_start(UINT32_MAX, len);
	if (lo == 0)
		c0.color = colors->first;
	else
		c0.color = colors->color[lo - 1];
	c1.color = colors->color[hi - 1];
	diff = rc - anx->anchor[lo];
	alpha_diff = hw_divider_u32_quotient_wait() * diff;

	return color_blend2(c0, c1, alpha_diff);
}

static uint32_t
pal_rand_col12(const struct segment *seg)
{
	uint32_t alpha_diff = get_rand_32();
	union color_pun c0, c1;

	c0.color = seg->color[0];
	c1.color = seg->color[1];
	return color_blend2(c0, c1, alpha_diff);
}

static uint32_t
pal_rand_gradient(const struct segment *seg)
{
	uint32_t r = get_rand_32(), alpha_diff = get_rand_32();
	union color_pun c0, c1;

	if (r < 0x55555555) {
		c0.color = seg->color[0];
		c1.color = seg->color[1];
	}
	else if (r < 0xaaaaaaaa) {
		c0.color = seg->color[1];
		c1.color = seg->color[2];
	}
	else {
		c0.color = seg->color[2];
		c1.color = seg->color[0];
	}

	return color_blend2(c0, c1, alpha_diff);
}

static uint32_t
pal_rand_only(const struct segment *seg)
{
	uint32_t r = get_rand_32();
	union color_pun c0, c1;

	if (seg->color[2] == 0) {
		if (r < 0x70000000)
			return seg->color[0];
		if (r < 0xe0000000)
			return seg->color[1];
		c0.color = seg->color[0];
		c1.color = seg->color[1];
	}
	else {
		if (r < 0x50000000)
			return seg->color[0];
		if (r < 0x90000000)
			return seg->color[1];
		if (r < 0xd0000000)
			return seg->color[2];
		if (r < 0xe0000000) {
			c0.color = seg->color[0];
			c1.color = seg->color[1];
		}
		else if (r < 0xf0000000) {
			c0.color = seg->color[1];
			c1.color = seg->color[2];
		}
		else {
			c0.color = seg->color[2];
			c1.color = seg->color[0];
		}
	}

	return color_blend2(c0, c1, get_rand_32());
}

static uint32_t
pal_pick_color(const struct segment *seg, struct fx_state *state)
{
	union color_pun c;

	if (state->palette != NULL)
		return pal_rand_color(state);
	else {
		switch (seg->palette) {
		case PAL_DYN_COLOR1:
			return seg->color[0];
		case PAL_DYN_COLOR12:
			return pal_rand_col12(seg);
		case PAL_DYN_COL_GRAD:
			return pal_rand_gradient(seg);
		case PAL_DYN_COL_ONLY:
			return pal_rand_only(seg);
		case PAL_DYN_RANDOM:
			c = rnd_color();
			return c.color;
		default:
			panic("Invalid palette number: %u", seg->palette);
		}
	}
}

void
init_pal_splash(const struct global *global, const struct segment *seg,
		struct fx_state *state)
{
	uint16_t p;
	uint32_t color;

	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, 2 * seg->len);

	init_generic(global, seg, state);

	p = seg_rand_point(seg);
	color = pal_pick_color(seg, state);
	if (state->bri != UINT8_MAX)
		color = scale(color, state->bri);

	dma_channel_wait_for_finish_blocking(col2buf_chan);
	state->segbuf[0][p] = color;
	state->segbuf[1][p] = color;
}

void
fx_pal_splash(uint32_t *buf, const struct segment *seg, struct fx_state *state)
{
	int b = state->counter & 1;
	uint16_t p;
	uint32_t color;

	dma_channel_set_write_addr(buf2buf_chan, buf, false);
	dma_channel_transfer_from_buffer_now(buf2buf_chan, state->segbuf[b],
					     seg->len);
	p = seg_rand_point(seg);
	state->palette = palettes[seg->palette];
	color = pal_pick_color(seg, state);

	if (seg->intensity != UINT8_MAX)
		for (int i = 0; i < seg->len; i++)
			state->segbuf[1-b][i] = scale(
				state->segbuf[1-b][i], seg->intensity);
	if (state->bri != UINT8_MAX)
		color = scale(color, state->bri);
	state->counter++;
	dma_channel_wait_for_finish_blocking(buf2buf_chan);

	state->segbuf[0][p] = color;
	state->segbuf[1][p] = color;
}

static void
supercomputer(const struct segment *seg, struct fx_state *state, int buf)
{
	for (uint32_t *b = state->segbuf[buf], n = 0;
	     b < state->segbuf[buf] + seg->len; b++, n++) {
		uint32_t rnd_color;

		if (get_rand_32() >= state->scale)
			continue;

		if (*b != 0) {
			*b = 0;
			continue;
		}

		rnd_color = get_rand_32();
		if (rnd_color < 0x55555555)
			*b = state->color[0];
		else if (rnd_color < 0xaaaaaaaa)
			*b = state->color[1];
		else
			*b = state->color[2];
	}
}

void
init_supercomputer(const struct global *global, const struct segment *seg,
		   struct fx_state *state)
{
	dma_channel_set_write_addr(col2buf_chan, state->segbuf[0], false);
	dma_channel_transfer_from_buffer_now(col2buf_chan, &DARK, 2 * seg->len);

	init_generic(global, seg, state);
	memcpy(state->color, seg->color, 3 * (sizeof(uint32_t)));
	if (state->bri != UINT8_MAX)
		for (int i = 0; i < 3; i++)
			state->color[i] = scale(state->color[i], state->bri);
	if (seg->intensity == UINT8_MAX)
		state->scale = 0x80000000;
	else
		state->scale = __fast_mul(seg->intensity, 0x800000);
	dma_channel_wait_for_finish_blocking(col2buf_chan);

	supercomputer(seg, state, 0);
	dma_channel_set_write_addr(buf2buf_chan, state->segbuf[1], false);
	dma_channel_transfer_from_buffer_now(buf2buf_chan, state->segbuf[0],
					     seg->len);
	dma_channel_wait_for_finish_blocking(buf2buf_chan);
}

void
fx_supercomputer(uint32_t *buf, const struct segment *seg,
		 struct fx_state *state)
{
	int curr = state->counter & 1, next = 1 - curr;

	dma_channel_set_write_addr(buf2buf_chan, buf, false);
	dma_channel_transfer_from_buffer_now(buf2buf_chan, state->segbuf[curr],
					     seg->len);
	supercomputer(seg, state, next);
	dma_channel_wait_for_finish_blocking(buf2buf_chan);

	dma_channel_set_write_addr(buf2buf_chan, state->segbuf[curr], false);
	dma_channel_transfer_from_buffer_now(buf2buf_chan, state->segbuf[next],
					     seg->len);
	state->counter++;
	dma_channel_wait_for_finish_blocking(buf2buf_chan);
}
