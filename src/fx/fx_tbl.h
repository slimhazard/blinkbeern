/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "fx.h"

extern unsigned col2buf_chan, buf2buf_chan;

void init_generic(const struct global *global, const struct segment *seg,
		  struct fx_state *state);

void fx_static(uint32_t *buf, const struct segment *seg,
	       struct fx_state *state);
void init_static(const struct global *global, const struct segment *seg,
		 struct fx_state *state);

void fx_snakes(uint32_t *buf, const struct segment *seg,
	       struct fx_state *state);
void init_snakes(const struct global *global, const struct segment *seg,
		 struct fx_state *state);

void fx_random(uint32_t *buf, const struct segment *seg,
	       struct fx_state *state);

void fx_random_rgbw(uint32_t *buf, const struct segment *seg,
		    struct fx_state *state);

void fx_sparkle(uint32_t *buf, const struct segment *seg,
		struct fx_state *state);
void init_sparkle(const struct global *global, const struct segment *seg,
		  struct fx_state *state);

void fx_greys(uint32_t *buf, const struct segment *seg, struct fx_state *state);
void init_greys(const struct global *global, const struct segment *seg,
		struct fx_state *state);

void fx_fade(uint32_t *buf, const struct segment *seg, struct fx_state *state);
void init_fade(const struct global *global, const struct segment *seg,
	       struct fx_state *state);

void fx_wipe(uint32_t *buf, const struct segment *seg, struct fx_state *state);
void init_wipe(const struct global *global, const struct segment *seg,
	       struct fx_state *state);

void fx_chase(uint32_t *buf, const struct segment *seg, struct fx_state *state);
void init_chase(const struct global *global, const struct segment *seg,
		struct fx_state *state);

void fx_scale(uint32_t *buf, const struct segment *seg, struct fx_state *state);

void fx_running(uint32_t *buf, const struct segment *seg,
		struct fx_state *state);
void init_running(const struct global *global, const struct segment *seg,
		  struct fx_state *state);

void fx_palette(uint32_t *buf, const struct segment *seg,
		struct fx_state *state);
void init_palette(const struct global *global, const struct segment *seg,
		  struct fx_state *state);

void fx_pal_splash(uint32_t *buf, const struct segment *seg,
		   struct fx_state *state);
void init_pal_splash(const struct global *global, const struct segment *seg,
		     struct fx_state *state);

void fx_supercomputer(uint32_t *buf, const struct segment *seg,
		      struct fx_state *state);
void init_supercomputer(const struct global *global, const struct segment *seg,
			struct fx_state *state);

void fx_sawtooth(uint32_t *buf, const struct segment *seg,
		 struct fx_state *state);
void init_sawtooth(const struct global *global, const struct segment *seg,
		   struct fx_state *state);

void fx_square(uint32_t *buf, const struct segment *seg,
	       struct fx_state *state);
void init_square(const struct global *global, const struct segment *seg,
		 struct fx_state *state);

void fx_triangle(uint32_t *buf, const struct segment *seg,
		 struct fx_state *state);
void init_triangle(const struct global *global, const struct segment *seg,
		   struct fx_state *state);

static const struct {
	fx_f		fx;
	fx_init_f	init;
} fx_tbl[] = {
	[FX_STATIC]		= { fx_static,		init_static	},
	[FX_SNAKES]		= { fx_snakes,		init_snakes	},
	[FX_RANDOM]		= { fx_random,		init_generic	},
	[FX_RANDOM_RGBW]	= { fx_random_rgbw,	init_generic	},
	[FX_SPARKLE]		= { fx_sparkle,		init_sparkle	},
	[FX_GREYS]		= { fx_greys,		init_greys	},
	[FX_FADE]		= { fx_fade,		init_fade	},
	[FX_WIPE]		= { fx_wipe,		init_wipe	},
	[FX_CHASE]		= { fx_chase,		init_chase	},
	[FX_SCALE]		= { fx_scale,		init_generic	},
	[FX_RUNNING]		= { fx_running,		init_running	},
	[FX_PALETTE]		= { fx_palette,		init_palette	},
	[FX_PAL_SPLASH]		= { fx_pal_splash,	init_pal_splash	},
	[FX_SUPERCOMPUTER]	= { fx_supercomputer,	init_supercomputer },
	[FX_SAWTOOTH]		= { fx_sawtooth,	init_sawtooth	},
	[FX_SQUARE]		= { fx_square,		init_square	},
	[FX_TRIANGLE]		= { fx_triangle,	init_triangle	},
};
