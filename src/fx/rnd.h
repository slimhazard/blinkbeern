/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/rand.h"

/*
 * Daniel Lemire's method for unbiased bounded random ranges using
 * 64-bit multiplication, with tweaks by Melissa O'Neill.
 * https://www.pcg-random.org/posts/bounded-rands.html
 */
static inline uint32_t
rnd32_nxt_bounded(uint32_t bound)
{
	uint64_t m = (uint64_t)get_rand_32() * (uint64_t)++bound;
	uint32_t l = (uint32_t)m;
	if (l < bound) {
		uint32_t t = -bound;
		if (t >= bound) {
			t -= bound;
			if (t >= bound)
				t %= bound;
		}
		while (l < t) {
			m = (uint64_t)get_rand_32() * (uint64_t)bound;
			l = (uint32_t)m;
		}
	}
	return (m >> 32);
}

static inline uint32_t
rnd32_range(uint32_t min, uint32_t max)
{
	return (min + rnd32_nxt_bounded(max - min));
}
