/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/time.h"

#include "types.h"

extern const uint32_t DARK;

enum fx {
	FX_STATIC = 0,
	FX_SNAKES,
	FX_RANDOM,
	FX_RANDOM_RGBW,
	FX_SPARKLE,
	FX_GREYS,
	FX_FADE,
	FX_WIPE,
	FX_CHASE,
	FX_SCALE,
	FX_RUNNING,
	FX_PALETTE,
	FX_PAL_SPLASH,
	FX_SUPERCOMPUTER,
	FX_SAWTOOTH,
	FX_SQUARE,
	FX_TRIANGLE,
	__MAX_FX,
};

struct fx_state {
	uint32_t	*segbuf[2];
	uint32_t	color[3];
	absolute_time_t	deadline;
	const struct palette	*palette;
	int		counter, shift, step, scale, mask;
	uint8_t		bri;
};

typedef void (*fx_f)(uint32_t *buf, const struct segment *seg,
		     struct fx_state *state);
typedef void (*fx_init_f)(const struct global *global,
			  const struct segment *seg, struct fx_state *state);
