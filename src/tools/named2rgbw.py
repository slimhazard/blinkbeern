#!/usr/bin/python3

import sys

gamma = [2.6, 2.2, 2.5]

for line in sys.stdin:
    f = line.rstrip().split()
    rgb = [float(f[1]), float(f[2]), float(f[3])]
    m = 255
    for i in range(3):
        rgb[i] = int(pow(rgb[i]/255.0, gamma[i]) * 255.0 + 0.5)
        if rgb[i] < m:
            m = rgb[i]
    grbw = [rgb[1] - m, rgb[0] - m, rgb[2] - m, m]
    hx = ''.join('{:02x}'.format(c) for c in grbw)
    print(f'{f[0]} 0x{hx}')
