#!/usr/bin/python3

import sys
import re

def rgb2grbw(rgb):
    min = 255
    for i in range(3):
        if rgb[i] < min:
            min = rgb[i]
    return [rgb[1] - min, rgb[0] - min, rgb[2] - min, min]

decl = re.compile(r"\s*const byte ([^[]+)")
vals = re.compile(r"(\d+),\s*(\d+),\s*(\d+),\s*(\d+)(\D)")

for l in sys.stdin:
    line = l.rstrip().lstrip()
    if line.startswith("// Size: "):
        print("// Converted to GRBW and fully interpolated for rp2040-sk6812")
        continue
    if line.startswith("//"):
        print(line)
        continue
    if len(line) == 0:
        print()
        continue
    m = decl.match(line)
    if m != None:
        print('static const uint32_t ' + m.group(1) + '[] = {')
        gr = []
        continue
    m = vals.search(line)
    if m == None:
        continue
    pos = int(m.group(1))
    rgb = [int(m.group(2)), int(m.group(3)), int(m.group(4))]
    term = m.group(5)
    gr.append([pos, rgb])
    if term == ",":
        continue
    n = 0
    print("\t", end="")
    for i in range(len(gr) - 1):
        gr0 = gr[i]
        gr1 = gr[i + 1]
        r0 = float(gr0[1][0])
        g0 = float(gr0[1][1])
        b0 = float(gr0[1][2])
        r1 = float(gr1[1][0])
        g1 = float(gr1[1][1])
        b1 = float(gr1[1][2])
        for j in range(gr1[0] - gr0[0]):
            t = float(j) / float(gr1[0] - gr0[0])
            rgb = [int((1 - t) * r0 + t * r1 + 0.5),
                   int((1 - t) * g0 + t * g1 + 0.5),
                   int((1 - t) * b0 + t * b1 + 0.5)]
            grbw = rgb2grbw(rgb)
            hx = ''.join('{:02x}'.format(c) for c in grbw)
            print(f'0x{hx}', end=", ")
            n = n + 1
            if n & 3 == 0:
                print()
                print("\t", end="")
        if i == len(gr) - 2:
            rgb = [int(r1), int(g1), int(b1)]
            grbw = rgb2grbw(rgb)
            hx = ''.join('{:02x}'.format(c) for c in grbw)
            print(f'0x{hx},')
    print('};')
