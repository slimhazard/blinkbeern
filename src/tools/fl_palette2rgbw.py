#!/usr/bin/python3

import sys
import re

gamma = [2.6, 2.2, 2.5]

crgb = {
    'CRGB::Aqua': [0, 255, 255],
    'CRGB::Aquamarine': [127, 255, 212],
    'CRGB::Black': [0, 0, 0],
    'CRGB::Blue': [0, 0, 255],
    'CRGB::CadetBlue': [95, 158, 160],
    'CRGB::CornflowerBlue': [100, 149, 237],
    'CRGB::DarkBlue': [0, 0, 139],
    'CRGB::DarkCyan': [0, 139, 139],
    'CRGB::DarkGreen': [0, 100, 0],
    'CRGB::DarkOliveGreen': [85, 107, 47],
    'CRGB::DarkRed': [139, 0, 0],
    'CRGB::ForestGreen': [34, 139, 34],
    'CRGB::Green': [0, 128, 0],
    'CRGB::LawnGreen': [124, 252, 0],
    'CRGB::LightBlue': [173, 216, 230],
    'CRGB::LightGreen': [144, 238, 144],
    'CRGB::LightSkyBlue': [135, 206, 250],
    'CRGB::LimeGreen': [50, 205, 50],
    'CRGB::Maroon': [128, 0, 0],
    'CRGB::MediumAquamarine': [102, 205, 170],
    'CRGB::MediumBlue': [0, 0, 205],
    'CRGB::MidnightBlue': [25, 25, 112],
    'CRGB::Navy': [0, 0, 128],
    'CRGB::OliveDrab': [107, 142, 35],
    'CRGB::Orange': [255, 165, 0],
    'CRGB::Red': [255, 0, 0],
    'CRGB::SeaGreen': [46, 139, 87],
    'CRGB::SkyBlue': [135, 206, 235],
    'CRGB::Teal': [0, 128, 128],
    'CRGB::White': [255, 255, 255],
    'CRGB::YellowGreen': [154, 205, 50],
}

def rgb2grbw(rgb):
    min = 255
    for i in range(3):
        rgb[i] = int(pow(rgb[i]/255.0, gamma[i]) * 255.0 + 0.5)
        if rgb[i] < min:
            min = rgb[i]
    return [rgb[1] - min, rgb[0] - min, rgb[2] - min, min]

pal_16_decl = re.compile(r"^extern const TProgmemRGBPalette16 (\w+)")
col = re.compile(r"([\w:]+)(?:,|$)")

in_palette = False
for l in sys.stdin:
    line = l.rstrip().lstrip()
    if line.startswith("//"):
        continue
    if len(line) == 0:
        continue
    m = pal_16_decl.match(line)
    if m != None:
        print('static const uint32_t ' + m.group(1) + '[] = {')
        gr = []
        continue
    if line == "{":
        in_palette = True
        continue
    if line == "};":
        n = 0
        print("\t", end="")
        steps = int(256 / len(gr))
        for i in range(len(gr)):
            gr0 = gr[i]
            if i == len(gr) - 1:
                gr1 = gr[0]
            else:
                gr1 = gr[i + 1]
            r0 = float(gr0[0])
            g0 = float(gr0[1])
            b0 = float(gr0[2])
            r1 = float(gr1[0])
            g1 = float(gr1[1])
            b1 = float(gr1[2])
            for j in range(steps):
                t = float(j)/float(steps)
                # print (f't={t}')
                rgb = [int((1 - t) * r0 + t * r1 + 0.5),
                       int((1 - t) * g0 + t * g1 + 0.5),
                       int((1 - t) * b0 + t * b1 + 0.5)]
                # print (f'rgb={rgb}')
                grbw = rgb2grbw(rgb)
                hx = ''.join('{:02x}'.format(c) for c in grbw)
                print(f'0x{hx}', end=", ")
                n = n + 1
                if n & 3 == 0:
                    print()
                    print("\t", end="")
        print('};')
        in_palette = False
        continue
    if in_palette == False:
        continue
    vals = col.findall(line)
    for val in vals:
        if val in crgb:
            gr.append(crgb[val])
            continue
        v = int(val, 16)
        rgb = [(v & 0x0ff0000) >> 16, (v & 0x0ff00) >> 8, (v & 0x0ff)]
        gr.append(rgb)
