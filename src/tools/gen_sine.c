#include <stdio.h>
#include <math.h>
#include <stdint.h>

int
main(void)
{
        for (int i = 0; i < 64; i++) {
                printf("0x%02x, ",
                    (int8_t)(sin(M_PI/2 * i/64.d) * 127.d + 0.5));
                if ((i & 7) == 7)
                        puts("");
        }
}
