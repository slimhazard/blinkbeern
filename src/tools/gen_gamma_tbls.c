#include <stdio.h>
#include <stdint.h>
#include <math.h>

/* R, G, B */
static const double G[] = { 2.6, 2.2, 2.5 };

int
main(void)
{
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j <= UINT8_MAX; j++) {
			uint8_t g = (uint8_t)(pow(j/255., G[i]) * 255. + 0.5);
			printf("0x%02x,", g);
			if ((j & 7) == 7)
				puts("");
			else
				putchar(' ');
		}
		puts("\n");
	}
}
