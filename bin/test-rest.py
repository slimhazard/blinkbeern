#!/usr/bin/env python3

# Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
#
# SPDX-License-Identifier: BSD-2-Clause
# See LICENSE

import unittest
import urllib3
from urllib3.response import HTTPResponse
from urllib3.util import Url
from urllib.parse import urlencode
from http import HTTPStatus
import json
import random
import string

CX = 6
CX_POOLS = urllib3.PoolManager(maxsize=CX, block=True)

SCHEME = 'http'
HOST = 'PicoW'

MIME_JSON = 'application/json'

HDR_TYPE = 'Content-Type'
HDR_LEN = 'Content-Length'
HDR_CCTRL = 'Cache-Control'

NO_STORE = 'no-store'

MAX_NLEDS = 65535
MAX_PRESETS = 63
MAX_PRESET_NAME = 31
MAX_PLAYLISTS = 63
MAX_PLAYLIST_NAME = 31
MAX_PS_PER_PL = 63
MAX_SEGMENTS = 15
MAX_SEGMENT_NAME = 31
MAX_QL_LEN = 2
MAX_PALETTES = 65535

NO_PLAYLIST = 255
NO_PRESET = 255

UINT8_MAX = 255
UINT16_MAX = 65535
INT16_MIN = -32768
INT16_MAX = 32767

def check_get_hdr_uncacheable(testCase, path=None):
    http = testCase._cx
    if path is None:
        path = testCase._path
    for method in [ 'GET', 'HEAD' ]:
        url = Url(scheme=SCHEME, host=HOST, path=path).url
        resp = http.request(method, url, headers = {
            'Accept': MIME_JSON
        })
        testCase.assertEqual(resp.status, HTTPStatus.OK)
        testCase.assertIn(HDR_TYPE, resp.headers)
        testCase.assertEqual(resp.headers[HDR_TYPE], MIME_JSON)
        testCase.assertIn(HDR_LEN, resp.headers)
        testCase.assertIn(HDR_CCTRL, resp.headers)
        testCase.assertEqual(resp.headers[HDR_CCTRL], NO_STORE)

def check_not_allowed(testCase, disallowed):
    http = testCase._cx
    for method in disallowed:
        url = Url(scheme=SCHEME, host=HOST, path=testCase._path).url
        resp = http.request(method, url, headers = {
            'Accept': MIME_JSON
        })
        testCase.assertEqual(resp.status, HTTPStatus.METHOD_NOT_ALLOWED)

def check_not_acceptable(testCase):
    http = testCase._cx
    for method in [ 'GET', 'HEAD' ]:
        url = Url(scheme=SCHEME, host=HOST, path=testCase._path).url
        resp = http.request(method, url)
        testCase.assertEqual(resp.status, HTTPStatus.NOT_ACCEPTABLE)
        resp = http.request(method, url, headers = { 'Accept': 'foo/bar' })
        testCase.assertEqual(resp.status, HTTPStatus.NOT_ACCEPTABLE)

def check_unsupported_media(testCase, methods):
    for method in methods:
        http = testCase._cx
        url = Url(scheme=SCHEME, host=HOST, path=testCase._path).url
        resp = http.request(method, url, headers = {
            HDR_TYPE: 'foo/bar'
        })
        testCase.assertEqual(resp.status, HTTPStatus.UNSUPPORTED_MEDIA_TYPE)
        resp = http.request(method, url)
        testCase.assertEqual(resp.status, HTTPStatus.UNSUPPORTED_MEDIA_TYPE)

def check_number(testCase, data, fld, min, max):
    testCase.assertIn(fld, data)
    testCase.assertGreaterEqual(data[fld], min)
    testCase.assertLessEqual(data[fld], max)

def check_number_with_sentinel(testCase, data, fld, min, max, sentinel):
    testCase.assertIn(fld, data)
    if data[fld] != sentinel:
        testCase.assertGreaterEqual(data[fld], min)
        testCase.assertLessEqual(data[fld], max)

def check_bool(testCase, data, fld):
    testCase.assertIn(fld, data)
    testCase.assertIsInstance(data[fld], bool)

def check_unprocessable(testCase, resp, msg):
    testCase.assertEqual(resp.status, HTTPStatus.UNPROCESSABLE_ENTITY)
    testCase.assertIn(HDR_TYPE, resp.headers)
    testCase.assertEqual(resp.headers[HDR_TYPE], MIME_JSON)
    testCase.assertIn(HDR_LEN, resp.headers)
    testCase.assertEqual(int(resp.headers[HDR_LEN]), len(resp.data))
    testCase.assertIn(HDR_CCTRL, resp.headers)
    testCase.assertEqual(resp.headers[HDR_CCTRL], NO_STORE)
    decoded = json.loads(resp.data.decode('utf-8'))
    testCase.assertIn('error', decoded)
    testCase.assertIn(msg, decoded['error'])

def get_resp_for_req_data(http, method, url, data):
    encoded = json.dumps(data).encode('utf-8')
    resp = http.request(method, url, body=encoded, headers = {
        HDR_TYPE: MIME_JSON
    })
    return resp

def tearDownModule():
    CX_POOLS.clear()

class TestNetwork(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._path = '/v1/network'

    def test_get_header(self):
        check_get_hdr_uncacheable(self)

    def test_get_body(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        self.assertIn(HDR_LEN, resp.headers)
        self.assertEqual(int(resp.headers[HDR_LEN]), len(resp.data))

        data = json.loads(resp.data.decode('utf-8'))
        self.assertIn('wifi', data)
        wifi = data['wifi']
        self.assertIn('ssid', wifi)
        self.assertIn('country', wifi)
        self.assertEqual(len(wifi['country']), 2)
        self.assertIn('auth', wifi)
        self.assertGreaterEqual(wifi['auth'], 0)
        self.assertLessEqual(wifi['auth'], 3)

        self.assertIn('ap', data)
        ap = data['ap']
        self.assertIn('ssid', ap)

    def test_not_allowed(self):
        disallowed = [ 'POST', 'DELETE', 'CONNECT', 'TRACE', 'OPTIONS' ]
        check_not_allowed(self, disallowed)

    def test_not_acceptable(self):
        check_not_acceptable(self)

    # Test PUT /network only for failure. We don't want to change the
    # configuration, because we don't want the side effects.
    def test_unsupported_media(self):
        check_unsupported_media(self, [ 'PUT' ])

    def test_unprocessable(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('PUT', url, body = 'foo', headers = {
            HDR_TYPE: MIME_JSON
        })
        check_unprocessable(self, resp, 'cannot parse')

        data = {'foo':'bar'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'unknown field')

        data = {'wifi':4711}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'ap':True}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'wifi':{'foo':'bar'}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'unknown field')

        data = {'wifi':{'ssid':4711}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'wifi':{'pass':False}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'wifi':{'country':1147}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'wifi':{'country':'foobar'}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'length')

        data = {'wifi':{'auth':'bazquux'}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'wifi':{'auth':4711}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal value')

        data = {'ap':{'ssid':4711}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'ap':{'pass':False}}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

class TestGlobal(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._path = '/v1/global'

    def test_get_header(self):
        check_get_hdr_uncacheable(self)

    def test_get_body(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        self.assertIn(HDR_LEN, resp.headers)
        self.assertEqual(int(resp.headers[HDR_LEN]), len(resp.data))

        data = json.loads(resp.data.decode('utf-8'))
        check_number(self, data, 'nleds', 0, MAX_NLEDS)
        check_number(self, data, 'fps', 0, UINT8_MAX)
        check_number_with_sentinel(self, data, 'pl', 0, MAX_PLAYLISTS,
                                   NO_PLAYLIST)
        check_number(self, data, 'bri', 0, UINT8_MAX)
        check_number_with_sentinel(self, data, 'ps', 0, MAX_PRESETS, NO_PRESET)
        check_bool(self, data, 'on')

    def test_put(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        test_data = {
            'nleds': 1,
            'fps': 128,
            'bri': 128,
            'pl': 255,
            'ps': 255,
            'on': False,
        }
        encoded = json.dumps(test_data).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)

        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(data, test_data)

    def test_not_allowed(self):
        disallowed = [ 'POST', 'DELETE', 'CONNECT', 'TRACE', 'OPTIONS' ]
        check_not_allowed(self, disallowed)

    def test_not_acceptable(self):
        check_not_acceptable(self)

    def test_unsupported_media(self):
        check_unsupported_media(self, [ 'PUT' ])

    def test_unprocessable(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('PUT', url, body = 'foo', headers = {
            HDR_TYPE: MIME_JSON
        })
        check_unprocessable(self, resp, 'cannot parse')

        data = {'foo':'bar'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'unknown field')

        data = {'nleds':'4711'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'nleds':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'nleds': MAX_NLEDS + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'bri':'117'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'bri':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'bri': UINT8_MAX + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'fps':'117'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'fps':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'fps': UINT8_MAX + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'on':4711}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'ps':'117'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'ps':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'ps': MAX_PRESETS + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        # Delete a preset (OK if not found), then attempt to set it.
        url_ps = Url(scheme=SCHEME, host=HOST, path='/v1/presets').url
        resp = http.request('DELETE', url_ps, fields={ 'id': MAX_PRESETS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)
        data = {'ps': MAX_PRESETS}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, f'Preset {MAX_PRESETS} not defined')

        # Delete a playlist (OK if not found), then attempt to set it.
        url_pl = Url(scheme=SCHEME, host=HOST, path='/v1/playlists').url
        resp = http.request('DELETE', url_pl, fields={ 'id': MAX_PLAYLISTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)
        data = {'pl': MAX_PLAYLISTS}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, f'Playlist {MAX_PLAYLISTS} not defined')

class TestSegments(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._path = '/v1/segments'
        cls._path0 = cls._path + '?id=0'
        cls._test_data = {
            'name': 'Test Run',
            'color': {
                'pri': {
                    'r': 47,
                    'g': 11,
                    'b': 8,
                    'w': 15,
                },
                "sec": {
                    'r': 111,
                    'g': 222,
                    'b': 33,
                    'w': 44,
                },
                "tert": {
                    'r': 114,
                    'g': 103,
                    'b': 98,
                    'w': 119,
                },
            },
            'start': 0,
            'len': 1,
            'pal': 12,
            'off': 0,
            'fx': 0,
            'grp': 0,
            'spc': 0,
            'sx': 128,
            'ix': 128,
            'bri': 128,
            'sel': True,
            'rev': False,
            'mi': False,
        }

    def test_get_header(self):
        check_get_hdr_uncacheable(self)
        check_get_hdr_uncacheable(self, self._path0)

    def check_segment(self, seg):
        self.assertIn('name', seg)
        self.assertIsInstance(seg['name'], str)
        self.assertLessEqual(len(seg['name']), MAX_SEGMENT_NAME)

        self.assertIn('color', seg)
        color = seg['color']
        for ord in [ 'pri', 'sec', 'tert' ]:
            self.assertIn(ord, color)
            rgbw = color[ord]
            for c in [ 'r', 'g', 'b', 'w' ]:
                check_number(self, rgbw, c, 0, UINT8_MAX)

        check_number(self, seg, 'id', 0, MAX_SEGMENTS)
        check_number(self, seg, 'start', 0, MAX_NLEDS)
        check_number(self, seg, 'len', 0, MAX_NLEDS)
        check_number(self, seg, 'pal', 0, UINT8_MAX)
        check_number(self, seg, 'off', -128, 127)
        check_number(self, seg, 'fx', 0, UINT8_MAX)
        check_number(self, seg, 'grp', 0, UINT8_MAX)
        check_number(self, seg, 'spc', 0, UINT8_MAX)
        check_number(self, seg, 'sx', 0, UINT8_MAX)
        check_number(self, seg, 'ix', 0, UINT8_MAX)
        check_number(self, seg, 'bri', 0, UINT8_MAX)
        check_bool(self, seg, 'sel')
        check_bool(self, seg, 'rev')
        check_bool(self, seg, 'mi')

    def test_get_body(self):
        http = self._cx
        for path in [ self._path, self._path0 ]:
            url = Url(scheme=SCHEME, host=HOST, path=path).url
            resp = http.request('GET', url, headers = {
                'Accept': MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.OK)
            self.assertIn(HDR_LEN, resp.headers)
            self.assertEqual(int(resp.headers[HDR_LEN]), len(resp.data))

            data = json.loads(resp.data.decode('utf-8'))
            if path == self._path:
                self.assertIsInstance(data, list)
                for seg in data:
                    self.check_segment(seg)
            else:
                self.check_segment(data)

    def test_not_allowed(self):
        disallowed = [ 'CONNECT', 'TRACE', 'OPTIONS' ]
        check_not_allowed(self, disallowed)

    def test_not_acceptable(self):
        check_not_acceptable(self)

    def test_unsupported_media(self):
        check_unsupported_media(self, [ 'PUT', 'POST' ])

    def test_not_found(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        for method in [ 'POST', 'PUT', 'DELETE' ]:
            # No query string
            resp = http.request(method, url, headers={
                HDR_TYPE: MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string not id=N
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields={ 'foo': 'bar' })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string is id=X with non-numeric X
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields={ 'id': 'foo' })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string is id=N with N out of range
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields={ 'id': MAX_SEGMENTS + 1 })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

        # For testing, delete segment id=15 (OK if it doesn't exist).
        resp = http.request('DELETE', url, fields={ 'id': MAX_SEGMENTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

        # These methods require that the segment id exists.
        for method in [ 'PUT', 'DELETE' ]:
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields = { 'id': MAX_SEGMENTS })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

    def test_conflict(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path0).url
        resp = http.request('POST', url, headers={ HDR_TYPE: MIME_JSON })
        self.assertEqual(resp.status, HTTPStatus.CONFLICT)

    def test_forbidden(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path0).url
        resp = http.request('DELETE', url)
        self.assertEqual(resp.status, HTTPStatus.FORBIDDEN)

    def verify_seg(self, url, id):
        http = self._cx
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))
        self.assertIn('id', data)
        self.assertEqual(data['id'], id)
        del data['id']
        self.assertEqual(data, self._test_data)

    def test_put(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path0).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_seg(url, 0)

    def test_post_and_delete(self):
        http = self._cx
        id15 = urlencode({ 'id': MAX_SEGMENTS })
        path = self._path + '?' + id15
        url = Url(scheme=SCHEME, host=HOST, path=path).url

        # First delete id=15, with or without success, to prepare
        resp = http.request('DELETE', url)
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

        # Post to id=15
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_seg(url, MAX_SEGMENTS)

        # Delete id=15 for real
        resp = http.request('DELETE', url)
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT)

        # Verify that it's gone
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

    def test_unprocessable(self):
        http = self._cx
        method = 'PUT'
        url = Url(scheme=SCHEME, host=HOST, path=self._path0).url
        resp = http.request(method, url, body = 'foo', headers = {
            HDR_TYPE: MIME_JSON
        })
        check_unprocessable(self, resp, 'cannot parse')

        data = {'foo':'bar'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'unknown field')

        data = {'name':4711}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'name':'12345678901234567890123456789012'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too long')

        data = {'color':'foo'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'color':{'foo':{}}}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'unknown field')

        for ord in [ 'pri', 'sec', 'tert' ]:
            data = {'color':{ord:False}}
            resp = get_resp_for_req_data(http, method, url, data)
            check_unprocessable(self, resp, 'illegal type')

            data = {'color':{ord:{"z":0}}}
            resp = get_resp_for_req_data(http, method, url, data)
            check_unprocessable(self, resp, 'unknown field')

            for c in [ 'r', 'g', 'b', 'w']:
                data = {'color':{ord:{c:'quux'}}}
                resp = get_resp_for_req_data(http, method, url, data)
                check_unprocessable(self, resp, 'illegal type')

                data = {'color':{ord:{c:UINT8_MAX+1}}}
                resp = get_resp_for_req_data(http, method, url, data)
                check_unprocessable(self, resp, 'out of range')

        data = {'start':'0'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'start':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'start': MAX_NLEDS + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'len':False}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'len':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'len': MAX_NLEDS + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'pal':'palname'}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'pal':-1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'pal': MAX_PALETTES + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'off':True}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'off':INT16_MIN - 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'off': INT16_MAX + 1}
        resp = get_resp_for_req_data(http, 'PUT', url, data)
        check_unprocessable(self, resp, 'out of range')

        for fld in [ 'fx', 'grp', 'spc', 'sx', 'ix', 'bri' ]:
            data = {fld:'foo'}
            resp = get_resp_for_req_data(http, 'PUT', url, data)
            check_unprocessable(self, resp, 'illegal type')

            data = {fld:-1}
            resp = get_resp_for_req_data(http, 'PUT', url, data)
            check_unprocessable(self, resp, 'out of range')

            data = {fld:UINT8_MAX+1}
            resp = get_resp_for_req_data(http, 'PUT', url, data)
            check_unprocessable(self, resp, 'out of range')

        for fld in [ 'sel', 'rev', 'mi' ]:
            data = {fld:'foo'}
            resp = get_resp_for_req_data(http, 'PUT', url, data)
            check_unprocessable(self, resp, 'illegal type')

class TestPresets(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._path = '/v1/presets'
        cls._path_id = f'/v1/presets?id={MAX_PRESETS}'
        cls._test_data = {
            'name': 'Test Preset',
            'ql': 'TE',
            'bri': True,
            'sb': True,
        }

    def delete_for_testing(self):
        # For testing, delete preset id=MAX_PRESETS (OK if it doesn't exist).
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('DELETE', url, fields={ 'id': MAX_PRESETS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

    def tearDown(self):
        self.delete_for_testing()

    def test_get_header(self):
        check_get_hdr_uncacheable(self)

    def check_preset(self, ps):
        self.assertIn('name', ps)
        self.assertIsInstance(ps['name'], str)
        self.assertLessEqual(len(ps['name']), MAX_PRESET_NAME)

        self.assertIn('ql', ps)
        self.assertIsInstance(ps['ql'], str)
        self.assertLessEqual(len(ps['ql']), MAX_QL_LEN)

        check_bool(self, ps, 'bri')
        check_bool(self, ps, 'sb')

    def test_get_body(self):
        http = self._cx

        # make sure we have test data
        self.delete_for_testing()
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        for path in [ self._path, self._path_id ]:
            url = Url(scheme=SCHEME, host=HOST, path=path).url
            resp = http.request('GET', url, headers = {
                'Accept': MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.OK)
            self.assertIn(HDR_LEN, resp.headers)
            self.assertEqual(int(resp.headers[HDR_LEN]), len(resp.data))

            data = json.loads(resp.data.decode('utf-8'))
            if path == self._path:
                self.assertIsInstance(data, list)
                for ps in data:
                    self.check_preset(ps)
            else:
                self.check_preset(data)

    def test_not_allowed(self):
        disallowed = [ 'CONNECT', 'TRACE', 'OPTIONS' ]
        check_not_allowed(self, disallowed)

    def test_not_acceptable(self):
        check_not_acceptable(self)

    def test_unsupported_media(self):
        check_unsupported_media(self, [ 'PUT', 'POST' ])

    def test_not_found(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        for method in [ 'POST', 'PUT', 'DELETE' ]:
            # No query string
            resp = http.request(method, url, headers={
                HDR_TYPE: MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string not id=N
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields={ 'foo': 'bar' })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string is id=X with non-numeric X
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields={ 'id': 'foo' })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string is id=N with N out of range
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields={ 'id': MAX_PRESETS + 1 })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

        self.delete_for_testing()
        # These methods require that the segment id exists.
        for method in [ 'PUT', 'DELETE' ]:
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON },
                                fields = { 'id': MAX_PRESETS })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

    def test_conflict(self):
        self.delete_for_testing()
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers={
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        resp = http.request('POST', url, body=encoded, headers={
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.CONFLICT)

    def verify_ps(self, url, id):
        http = self._cx
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))
        self.assertIn('id', data)
        self.assertEqual(data['id'], id)
        del data['id']
        self.assertEqual(data, self._test_data)

    def test_post_and_delete(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url

        # First delete id=MAX, with or without success, to prepare
        self.delete_for_testing()

        # Post to id=MAX
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_ps(url, MAX_PRESETS)

        # Delete id=MAX for real
        resp = http.request('DELETE', url)
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT)

        # Verify that it's gone
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

    def test_put(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url

        self.delete_for_testing()
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_ps(url, MAX_PRESETS)

    def test_unprocessable(self):
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url

        # Delete and post id=max, all further tests with PUT.
        self.delete_for_testing()
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_ps(url, MAX_PRESETS)

        method = 'PUT'
        resp = http.request(method, url, body = 'foo', headers = {
            HDR_TYPE: MIME_JSON
        })
        check_unprocessable(self, resp, 'cannot parse')

        data = {'foo':'bar'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'unknown field')

        data = {'name':4711}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'name':'12345678901234567890123456789012'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too long')

        data = {'ql':815}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'ql':'123'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too long')

        for fld in [ 'bri', 'sb' ]:
            data = {fld:'foo'}
            resp = get_resp_for_req_data(http, 'PUT', url, data)
            check_unprocessable(self, resp, 'illegal type')

    def test_set_ps(self):
        # 1. Set a test segment
        # 2. Save as a preset
        # 3. Change the segment config
        # 4. Set the preset in global config
        # 5. Verify that the segment config is now set as in the preset

        self.delete_for_testing()
        http = self._cx

        # Sets the global preset to none
        global_cfg = { 'ps': NO_PRESET }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        encoded = json.dumps(global_cfg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Delete any segments with id != 0
        for id in range(1, MAX_SEGMENTS + 1):
            url = Url(scheme=SCHEME, host=HOST, path='/v1/segments').url
            resp = http.request('DELETE', url, fields={ 'id': id })
            self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                            resp.status == HTTPStatus.NOT_FOUND)

        # Set the test segment
        test_segment = {
            'name': 'Test Segment',
            'id': 0,
            'color': {
                'pri': {
                    'r': 47,
                    'g': 11,
                    'b': 8,
                    'w': 15,
                },
                "sec": {
                    'r': 111,
                    'g': 222,
                    'b': 33,
                    'w': 44,
                },
                "tert": {
                    'r': 114,
                    'g': 103,
                    'b': 98,
                    'w': 119,
                },
            },
            'start': 0,
            'len': 1,
            'pal': 1,
            'off': 0,
            'fx': 0,
            'grp': 0,
            'spc': 0,
            'sx': 128,
            'ix': 128,
            'bri': 128,
            'sel': True,
            'rev': False,
            'mi': False,
        }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        encoded = json.dumps(test_segment).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)

        # Save the preset
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_ps(url, MAX_PRESETS)

        # Change the segment config
        for i in range(3):
            changed_segment = test_segment.copy()
            changed_segment['id'] = i
            for fld in ['pal', 'fx', 'spc', 'sx', 'ix', 'bri']:
                changed_segment[fld] = changed_segment[fld] + i + 1
                changed_segment['name'] = f'Changed Segment {i}'
            path = f'/v1/segments?id={i}'
            url = Url(scheme=SCHEME, host=HOST, path=path).url
            encoded = json.dumps(changed_segment).encode('utf-8')
            if i == 0:
                method = 'PUT'
            else:
                method = 'POST'
            resp = http.request(method, url, body=encoded, headers = {
                HDR_TYPE: MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)

        # Set the preset
        global_cfg = { 'ps': MAX_PRESETS }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        encoded = json.dumps(global_cfg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Get the segment config
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments').url
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))

        # Verify that the segment is now configured as in the preset
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], test_segment)

        # Change the segment again
        test_segment['fx'] = test_segment['fx'] + 1
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        encoded = json.dumps(test_segment).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)

        # Get the global config, and verify that the preset field is now unset
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(data['ps'], NO_PRESET)

    def test_ps_sb_false(self):
        # Set segment bounds and save ps with sb == false.
        # Change segment bounds
        # Set the preset, and confirm the changed bounds
        self.delete_for_testing()
        http = self._cx
        START = 0
        LEN = 10
        CHANGED_START = START + 1
        CHANGED_LEN = LEN - 1

        # Sets the global preset to none
        global_cfg = { 'ps': NO_PRESET }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        encoded = json.dumps(global_cfg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Set segment 0 bounds
        seg = { 'start': START, 'len': LEN }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        encoded = json.dumps(seg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Save the preset with sb == false
        ps_sb_false = self._test_data.copy()
        ps_sb_false['sb'] = False
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(ps_sb_false).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)

        # Change segment 0 bounds
        seg = { 'start': CHANGED_START, 'len': CHANGED_LEN }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        encoded = json.dumps(seg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Sets the global preset as saved above
        global_cfg = { 'ps': MAX_PRESETS }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        encoded = json.dumps(global_cfg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Get the segment config
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))

        # Verify that the segment bounds were not changed by the preset
        self.assertEqual(data['start'], CHANGED_START)
        self.assertEqual(data['len'], CHANGED_LEN)

    def test_ps_bri_false(self):
        # As above, saving ps with bri/ib == false.
        self.delete_for_testing()
        http = self._cx
        BRI = 64
        CHANGED_BRI = 192

        # Sets the global preset to none
        global_cfg = { 'ps': NO_PRESET }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        encoded = json.dumps(global_cfg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Set segment 0 brightness
        seg = { 'bri': BRI }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        encoded = json.dumps(seg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Save the preset with bri == false
        ps_bri_false = self._test_data.copy()
        ps_bri_false['bri'] = False
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(ps_bri_false).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)

        # Change segment 0 bounds
        seg = { 'bri': CHANGED_BRI }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        encoded = json.dumps(seg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Sets the global preset as saved above
        global_cfg = { 'ps': MAX_PRESETS }
        url = Url(scheme=SCHEME, host=HOST, path='/v1/global').url
        encoded = json.dumps(global_cfg).encode('utf-8')
        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Get the segment config
        url = Url(scheme=SCHEME, host=HOST, path='/v1/segments?id=0').url
        resp = http.request('GET', url, headers = {
            'Accept': MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))

        # Verify that the brightness was not changed by the preset
        self.assertEqual(data['bri'], CHANGED_BRI)

class TestPlaylists(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._path = '/v1/playlists'
        cls._path_id = f'/v1/playlists?id={MAX_PLAYLISTS}'
        cls._test_presets = [
            {
                'name': 'Test PS 1',
                'ql': 'T1',
                'bri': True,
                'sb': True,
            },
            {
                'name': 'Test PS 2',
                'ql': 'T2',
                'bri': True,
                'sb': False,
            },
            {
                'name': 'Test PS 3',
                'ql': 'T3',
                'bri': False,
                'sb': True,
            },
            {
                'name': 'Test PS 4',
                'ql': 'T3',
                'bri': False,
                'sb': False,
            },
        ]
        cls._test_data = {
            'name': 'Test Playlist',
            'ql': 'TP',
            'ps': [0, 1, 2, 3],
            'dur': [100, 200, 300, 400],
            'repeat': 0,
            'end': 0,
            'shuffle': True,
        }

    def delete_for_testing(self):
        # For testing, delete playlist id=MAX_PLAYLISTS (OK if not found).
        http = self._cx
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('DELETE', url, fields={ 'id': MAX_PLAYLISTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)
        for id in range(4):
            url = Url(scheme=SCHEME, host=HOST, path='/v1/presets').url
            resp = http.request('DELETE', url, fields={ 'id': id })
            self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                            resp.status == HTTPStatus.NOT_FOUND)

    def setUp(self):
        # Define the test_presets; first delete if necessary.
        self.delete_for_testing()
        http = self._cx
        for id in range(4):
            url = Url(scheme=SCHEME, host=HOST, path=f'/v1/presets?id={id}').url
            encoded = json.dumps(self._test_presets[id]).encode('utf-8')
            resp = http.request('POST', url, body=encoded, headers={
                HDR_TYPE: MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

    def tearDown(self):
        self.delete_for_testing()

    def test_get_header(self):
        check_get_hdr_uncacheable(self)

    def check_playlist(self, pl):
        self.assertIn('name', pl)
        self.assertIsInstance(pl['name'], str)
        self.assertLessEqual(len(pl['name']), MAX_PRESET_NAME)

        self.assertIn('ql', pl)
        self.assertIsInstance(pl['ql'], str)
        self.assertLessEqual(len(pl['ql']), MAX_QL_LEN)

        self.assertIn('ps', pl)
        self.assertIsInstance(pl['ps'], list)
        self.assertLessEqual(len(pl['ps']), MAX_PS_PER_PL + 1)
        for ps in pl['ps']:
            self.assertIsInstance(ps, int)
            self.assertGreaterEqual(ps, 0)
            self.assertLessEqual(ps, MAX_PRESETS)

        self.assertIn('dur', pl)
        self.assertIsInstance(pl['dur'], list)
        self.assertLessEqual(len(pl['dur']), MAX_PS_PER_PL + 1)
        for dur in pl['dur']:
            self.assertIsInstance(dur, int)
            self.assertGreaterEqual(dur, 0)
            self.assertLessEqual(dur, UINT16_MAX)

        self.assertIn('repeat', pl)
        self.assertIsInstance(pl['repeat'], int)
        self.assertGreaterEqual(pl['repeat'], 0)
        self.assertLessEqual(pl['repeat'], UINT8_MAX)

        self.assertIn('end', pl)
        self.assertIsInstance(pl['end'], int)
        self.assertGreaterEqual(pl['end'], 0)
        self.assertLessEqual(pl['end'], MAX_PRESETS)

        check_bool(self, pl, 'shuffle')

    def test_get_body(self):
        http = self._cx

        # make sure we have test data
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT,
                         resp.data.decode('utf-8'))

        for path in [ self._path, self._path_id ]:
            url = Url(scheme=SCHEME, host=HOST, path=path).url
            resp = http.request('GET', url, headers = {
                'Accept': MIME_JSON
            })
            self.assertEqual(resp.status, HTTPStatus.OK)
            self.assertIn(HDR_LEN, resp.headers)
            self.assertEqual(int(resp.headers[HDR_LEN]), len(resp.data))

            data = json.loads(resp.data.decode('utf-8'))
            if path == self._path:
                self.assertIsInstance(data, list)
                for pl in data:
                    self.check_playlist(pl)
            else:
                self.check_playlist(data)

    def test_not_allowed(self):
        disallowed = [ 'CONNECT', 'TRACE', 'OPTIONS' ]
        check_not_allowed(self, disallowed)

    def test_not_acceptable(self):
        check_not_acceptable(self)

    def test_unsupported_media(self):
        check_unsupported_media(self, [ 'PUT', 'POST' ])

    def test_not_found(self):
        http = self._cx
        for method in [ 'POST', 'PUT', 'DELETE' ]:
            # No query string
            url = Url(scheme=SCHEME, host=HOST, path=self._path).url
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string not id=N
            url = Url(scheme=SCHEME, host=HOST,
                      path=self._path + '?foo=bar').url
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string is id=X with non-numeric X
            url = Url(scheme=SCHEME, host=HOST,
                      path=self._path + '?id=foo').url
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

            # Query string is id=N with N out of range
            url = Url(scheme=SCHEME, host=HOST,
                      path=self._path + f'?id={MAX_PLAYLISTS + 1}').url
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

        self.delete_for_testing()
        # These methods require that the segment id exists.
        for method in [ 'PUT', 'DELETE' ]:
            url = Url(scheme=SCHEME, host=HOST,
                      path=self._path + f'?id={MAX_PLAYLISTS}').url
            resp = http.request(method, url, headers={ HDR_TYPE: MIME_JSON })
            self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

    def test_conflict(self):
        http = self._cx
        # Delete the playlist, but not the presets.
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('DELETE', url, fields={ 'id': MAX_PLAYLISTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers={
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT, msg=resp.data)
        resp = http.request('POST', url, body=encoded, headers={
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.CONFLICT)

    def verify_pl(self, url, id):
        http = self._cx
        resp = http.request('GET', url, headers = { 'Accept': MIME_JSON })
        self.assertEqual(resp.status, HTTPStatus.OK)
        data = json.loads(resp.data.decode('utf-8'))
        self.assertIn('id', data)
        self.assertEqual(data['id'], id)
        del data['id']
        self.assertEqual(data, self._test_data)

    def test_post_and_delete(self):
        http = self._cx

        # Delete the playlist, but not the presets.
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('DELETE', url, fields={ 'id': MAX_PLAYLISTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

        # Post to id=MAX
        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_pl(url, MAX_PLAYLISTS)

        # Delete id=MAX for real
        resp = http.request('DELETE', url)
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        # Verify that it's gone
        resp = http.request('GET', url, headers = { 'Accept': MIME_JSON })
        self.assertEqual(resp.status, HTTPStatus.NOT_FOUND)

    def test_put(self):
        http = self._cx

        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('DELETE', url, fields={ 'id': MAX_PLAYLISTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)

        resp = http.request('PUT', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_pl(url, MAX_PLAYLISTS)

    def test_unprocessable(self):
        http = self._cx

        # Delete and post id=max, all further tests with PUT.
        url = Url(scheme=SCHEME, host=HOST, path=self._path).url
        resp = http.request('DELETE', url, fields={ 'id': MAX_PLAYLISTS })
        self.assertTrue(resp.status == HTTPStatus.NO_CONTENT or
                        resp.status == HTTPStatus.NOT_FOUND)

        url = Url(scheme=SCHEME, host=HOST, path=self._path_id).url
        encoded = json.dumps(self._test_data).encode('utf-8')
        resp = http.request('POST', url, body=encoded, headers = {
            HDR_TYPE: MIME_JSON
        })
        self.assertEqual(resp.status, HTTPStatus.NO_CONTENT)
        self.verify_pl(url, MAX_PRESETS)

        method = 'PUT'
        resp = http.request(method, url, body = 'foo', headers = {
            HDR_TYPE: MIME_JSON
        })
        check_unprocessable(self, resp, 'cannot parse')

        data = {'foo':'bar'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'unknown field')

        data = {'name':4711}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'name':'12345678901234567890123456789012'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too long')

        data = {'ql':815}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'ql':'123'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too long')

        ps = []
        for idx in range(65):
            ps.append(idx % 4)
        data = {'ps':ps}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too many presets')

        ps = ['foo']
        data = {'ps':ps}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'ps[0] illegal type')

        data = {'ps':[MAX_PRESETS + 1]}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'ps[0]=64 out of range')

        data = {'ps':[len(self._test_presets)]}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'ps[0]: preset 4 not defined')

        dur = []
        for idx in range(65):
            dur.append(idx * 100)
        data = {'dur':dur}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'too many durations')

        data = {'dur':['foo']}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'dur[0] illegal type')

        data = {'dur':[UINT16_MAX + 1]}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'dur[0]=65536 out of range')

        data = {'repeat':'foo'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

        data = {'repeat':UINT8_MAX + 1}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'repeat':-1}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'out of range')

        data = {'shuffle':'foo'}
        resp = get_resp_for_req_data(http, method, url, data)
        check_unprocessable(self, resp, 'illegal type')

if __name__ == '__main__':
    unittest.main()
