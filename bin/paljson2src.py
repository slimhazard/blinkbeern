#!/usr/bin/env python3

import sys
import os
import re
import json
import math
from jinja2 import Environment, FileSystemLoader

def mangler(m):
    c = m.group(0)[0]
    return f'__{ord(c):02x}__'

def gamma(rgb):
    exp = [ 2.7, 2.2, 2.5 ]
    g = []
    for i in range(3):
        g.append(math.floor(math.pow(rgb[i]/255.0, exp[i]) * 255.0 + 0.5))
    return g

def rgb2rgbw(rgb):
    w = min(rgb)
    rgbw = []
    for i in range(3):
        rgbw.append(rgb[i] - w)
    rgbw.append(w)
    return rgbw

##
## Main
##

if len(sys.argv) != 2:
    print(f"Usage: {sys.argv[0]} template", file=sys.stderr)
    sys.exit(-1)

pals = json.load(sys.stdin)

for p in pals:
    p['mangle'] = re.sub(r'\W', mangler, p['name'])
    if 'colors' not in p:
        continue
    for c in p['colors']:
        rgbw = rgb2rgbw(gamma([c['r'], c['g'], c['b']]))
        grbw32 = (rgbw[1] << 24) | (rgbw[0] << 16) | (rgbw[2] << 8) | rgbw[3]
        c['grbw32'] = f'0x{grbw32:08x}'

tmpl_path = os.path.dirname(sys.argv[1])
tmpl_base = os.path.basename(sys.argv[1])
env = Environment(loader=FileSystemLoader(tmpl_path))
try:
    tmpl = env.get_template(tmpl_base)
except Exception as e:
    print(f'Cannot load template {sys.argv[1]}: {e}', file=sys.stderr)
    sys.exit(-1)

try:
    data = { 'palettes': pals }
    content = tmpl.render(data)
    sys.stdout.write(content)
except Exception as e:
    print(f'Cannot render template {sys.argv[1]}: {e}', file=sys.stderr)
    sys.exit(-1)
