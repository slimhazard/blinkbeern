/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

const wifiSsidEl = document.getElementById("wifi_ssid");
const wifiPassEl = document.getElementById("wifi_pass");
const countryEl = document.getElementById("country");
const authEl = document.getElementById("auth");
const apSsidEl = document.getElementById("ap_ssid");
const apPassEl = document.getElementById("ap_pass");
const formEl = document.getElementById("form");
const statusEl = document.getElementById("status");

async function submit(evt) {
    evt.preventDefault();
    statusEl.style.display = "inline";

    let cfg = { "wifi": {}, "ap": {} };
    cfg["wifi"]["ssid"] = wifiSsidEl.value;
    cfg["wifi"]["pass"] = wifiPassEl.value;
    cfg["wifi"]["country"] = countryEl.value;
    cfg["wifi"]["auth"] = parseInt(authEl.value);
    cfg["ap"]["ssid"] = apSsidEl.value;
    cfg["ap"]["pass"] = apPassEl.value;

    try {
        let response = await fetch("/v1/network", {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(cfg),
        });
        switch (response.status) {
        case 204:
            statusEl.textContent = "Success!";
            break;
        case 422:
            let errmsg = await response.json();
            statusEl.textContent = errmsg.error;
            break;
        default:
            if (response.status >= 400) {
                statusEl.textContent = "Error: " + response.status;
            }
            else {
                statusEl.textContent = "Unexpected error: "
                    + response.status;
            }
        }
    }
    catch (ex) {
        statusEl.textContent = "Unexpected error: " + ex.message;
    }
}

async function preLoad() {
    try {
        let response = await fetch('/v1/network', {
            headers: {
                "Accept": "application/json",
            },
        });
        if (!response.ok)
            throw new Error("GET /network status: " + response.status);
        let data = await response.json();
        wifiSsidEl.value = data.wifi.ssid;
        countryEl.value = data.wifi.country;
        authEl.value = data.wifi.auth;
        apSsidEl.value = data.ap.ssid;
    }
    catch(ex) {
        console.log(ex.message);
    }
}

async function init() {
    await preLoad();
    formEl.addEventListener("submit", submit);
}

if (document.readyState !== "loading") {
    init();
}
else {
    document.addEventListener("DOMContentLoaded", init);
}
