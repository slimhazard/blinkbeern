/*
 * Copyright (c) 2023 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/*
 * Constants and utilities
 */

const MAX_SEGMENT_ID = 15;
const MAX_PRESET_ID = 63;

const HDRS_ACCEPT_JSON = { "Accept": "application/json" };
const HDRS_TYPE_JSON = { "Content-Type": "application/json" };

function put_post_opts(method, data) {
    return {
        method: method,
        headers: HDRS_TYPE_JSON,
        body: JSON.stringify(data),
    }
}

function put_opts(data) {
    return put_post_opts("PUT", data);
}

function post_opts(data) {
    return put_post_opts("POST", data);
}

function clamp(x, min, max) {
    return Math.min(Math.max(x, min), max);
}

/*
 * State object
 */

var state = {
    on: false,
    nleds: 1,
    fx: [],
    unusedSegId: 0,
    unusedPsId: 0,
};

function maxFps() {
    return Math.round(1e6 / (38.4 * state.nleds + 80));
};

/*
 * Power button
 */

/*
 * XXX style the button for on/off.
 */
const pwrElem = document.getElementById("pwrBtn");

async function onPowerBtnClick() {
    state.on = !state.on
    let data = { "on": state.on };
    let response = await fetch("/v1/global", put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log("power on/off: status=" + response.status + ", body=" +
                    response.body);
}

/*
 * Color pickers
 */

const pickerElems = document.getElementsByClassName("picker");

/* R, G, B */
const gamma_exp = [ 2.7, 2.2, 2.5 ];
const inv_gamma_exp = [ 1/gamma_exp[0], 1/gamma_exp[1], 1/gamma_exp[2] ];

function hex2rgb(hex) {
    const hexPattern = /^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i;
    let hexes = hex.match(hexPattern);
    if (hexes == null)
        throw new Error("invalid hex color format: " + hex);
    let rgb = [];
    for (let i = 0; i < 3; i++) {
        rgb[i] = parseInt(hexes[i+1], 16);
    }
    return rgb;
}

function rgb2hex(rgb) {
    return "#" + rgb.map(x => x.toString(16).padStart(2, '0')).join('');
}

function compute_gamma(rgb, exps) {
    let g_rgb = []
    for (let i = 0; i < 3; i++) {
        g_rgb[i] = Math.round(Math.pow(rgb[i]/255, exps[i]) * 255);
    }
    return g_rgb;
}

function gamma(rgb) {
    return compute_gamma(rgb, gamma_exp);
}

function inv_gamma(rgb) {
    return compute_gamma(rgb, inv_gamma_exp);
}

function rgb2rgbw(rgb) {
    let w = Math.min(...rgb);
    let rgbw = {};
    rgbw.r = rgb[0] - w;
    rgbw.g = rgb[1] - w;
    rgbw.b = rgb[2] - w;
    rgbw.w = w;
    return rgbw;
}

async function onColorSelect() {
    let segElem = segList.querySelector(".hilite");
    if (segElem === null) {
        /* XXX error handling */
        console.log("No segment is highlighted");
        return;
    }

    let data = {};
    let val = {};
    val[this.id] = rgb2rgbw(gamma(hex2rgb(this.value)));
    data['color'] = val;
    /* XXX id of the segment with focus */
    let response = await fetch(`/v1/segments?id=${segElem.dataset.id}`,
                               put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log("set color: status=" + response.status + ", body=" +
                    resopnse.body);

    segElem.dataset["color_" + this.id] = this.value;
}

/*
 * Global brightness & fps sliders
 */

const globalElems = document.getElementsByClassName("glbl");
const briElem = document.getElementById("glbl_bri");
const briOutElem = document.getElementById("glbl_bri_out");
const fpsElem = document.getElementById("glbl_fps");
const fpsOutElem = document.getElementById("glbl_fps_out");
const sxElem = document.querySelector("#speed");
const sxOutElem = document.querySelector("#speed-out");
const ixElem = document.querySelector("#intensity");
const ixOutElem = document.querySelector("#ix-out");

const slider2out = new Map();
slider2out.set(briElem, briOutElem);
slider2out.set(fpsElem, fpsOutElem);
slider2out.set(sxElem, sxOutElem);
slider2out.set(ixElem, ixOutElem);

async function onGlobalSliderChange() {
    let data = {};
    let fld = this.id.replace(/^glbl_/, '');
    data[fld] = parseInt(this.value);
    let response = await fetch("/v1/global", put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(fld + ": status=" + response.status + ", body=" +
                    resopnse.body);
}

function updateOutput(elem) {
    let max = 100;
    if (elem == fpsElem) {
        max = maxFps();
    }
    let val = parseInt(elem.value);
    let out = (val/255) * max;
    let outElem = null;
    if (elem.getAttribute("name") === "bright") {
        /* Segment brightness */
        let p = elem.parentElement;
        outElem = p.querySelector('[name="bri-out"]');
    }
    else {
        outElem = slider2out.get(elem);
    }
    outElem.value = out.toFixed(0);
}

function onSliderInput() {
    updateOutput(this);
}

/*
 * Effects
 */

const fxElem = document.querySelector("#fx");
const fx0BtnElem = document.querySelector("#fx0");

async function onFxClick() {
    let segElem = segList.querySelector(".hilite");
    if (segElem === null) {
        /* XXX error handling */
        console.log("No segment is highlighted");
        return;
    }

    let data = { "fx": parseInt(this.dataset.id) };
    let response = await fetch(`/v1/segments?id=${segElem.dataset.id}`,
                               put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`set fx ${this.dataset.id}: status=${response.status} ` +
                    `body=${response.body}`);

    segElem.dataset.fx = this.dataset.id;

    let fx_hilited = fxElem.querySelector(".hilite");
    if (fx_hilited !== null) {
        fx_hilited.classList.remove("hilite");
    }
    this.classList.add("hilite");
}

async function getFx() {
    let response = await fetch("/v1/fx");
    /* XXX error handling */
    if (!response.ok)
        throw new Error("Fetch fx: status=" + response.status +
                        " body=" + response.body);
    let data = await response.json();
    let fx0 = data.shift();
    let fx = [];
    for (const [idx, name] of data.entries()) {
        fx.push({ "id": idx + 1, "name": name });
    }
    fx.sort(function(a, b) {
        return a.name.localeCompare(b.name);
    });
    fx.unshift({ "id": 0, "name": fx0 });
    state.fx = fx;

    fx0BtnElem.textContent = fx0;
    fx0BtnElem.addEventListener("click", onFxClick);
    for (const [idx, fxData] of state.fx.entries()) {
        if (idx === 0) {
            continue;
        }
        let fxBtn = fx0BtnElem.cloneNode();
        fxBtn.id = "fx" + idx;
        fxBtn.dataset.id = fxData.id;
        fxBtn.textContent = fxData.name;
        fxBtn.addEventListener("click", onFxClick);
        let p = document.createElement("p");
        p.append(fxBtn);
        fxElem.append(p);
    }
}

/*
 * Palettes
 */

const colorElem = document.querySelector("#color");
const pal0BtnElem = document.querySelector("#pal0");

async function onPalClick() {
    let segElem = segList.querySelector(".hilite");
    if (segElem === null) {
        /* XXX error handling */
        console.log("No segment is highlighted");
        return;
    }

    let data = { "pal": parseInt(this.dataset.id) };
    let response = await fetch(`/v1/segments?id=${segElem.dataset.id}`,
                               put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`set pal ${this.dataset.id}: status=${response.status} ` +
                    `body=${response.body}`);

    segElem.dataset.pal = this.dataset.id;

    let hilited = colorElem.querySelector(".hilite");
    if (hilited !== null) {
        hilited.classList.remove("hilite");
    }
    this.classList.add("hilite");
}

async function getPalettes() {
    let response = await fetch("/v1/palettes");
    /* XXX error handling */
    if (!response.ok)
        throw new Error(`Fetch palettes: status=${response.status} ` +
                        `body=${response.body}`);
    let data = await response.json();
    let pal0 = data.shift();
    let pals = [];
    for (const [idx, p] of data.entries()) {
        pals.push({ "id": idx + 1, "name": p.name });
    }
    pals.unshift({ "id": 0, "name": pal0.name });
    pals = pals.slice(0, 6).concat(pals.slice(6).sort(
	function(a, b) {
	    return a.name.localeCompare(b.name);
	}
    ));

    pal0BtnElem.textContent = pal0.name;
    pal0BtnElem.addEventListener("click", onPalClick);
    for (const pal of pals) {
        if (pal.id === 0) {
            continue;
        }
        let palBtn = pal0BtnElem.cloneNode();
        palBtn.id = "pal" + pal.id;
        palBtn.dataset.id = pal.id;
        palBtn.textContent = pal.name;
        palBtn.addEventListener("click", onPalClick);
        let p = document.createElement("p");
        p.append(palBtn);
        colorElem.append(p);
    }
}

/*
 * Effect speed and intensity controls
 */

async function onFxSliderChange() {
    let segElem = segList.querySelector(".hilite");
    if (segElem === null) {
        /* XXX error handling */
        console.log("No segment is highlighted");
        return;
    }
    let fld = "sx";
    if (this.id === "intensity") {
        fld = "ix";
    }
    let val = parseInt(this.value);
    let data = { };
    data[fld] = val;
    let response = await fetch(`/v1/segments?id=${segElem.dataset.id}`,
                               put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`set ${fld} seg=${segElem.dataset.id}: ` +
                    `status=${response.status} body=${response.body}`);
    segElem.dataset[fld] = val;
}

/*
 * Segments
 */

const segList = document.querySelector('#seg-list');

const newSegBtnElem = document.querySelector("#apply-new-seg");
const newSegNameElem = document.querySelector("#new-seg-name");
const newSegStartElem = document.querySelector("#new-seg-start");
const newSegLenElem = document.querySelector("#new-seg-length");

async function onSegCheckboxClick() {
    let data = { [this.dataset.type] : this.checked };
    let response = await fetch(`/v1/segments?id=${this.dataset.id}`,
                               put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`set ${this.dataset.type} ${this.dataset.id}: ` +
                    `status=${response.status} body=${response.body}`);
}

async function onSegNameChange() {
    let id = this.dataset.id;
    let data = { "name": this.value };
    let response = await fetch(`/v1/segments?id=${id}`, put_opts(data));
    /* XXX error handling */
    if (!response.ok) {
        console.log(`set name ${this.dataset.id}: `+
                    `status=${response.status} body=${response.body}`);
        return;
    }
    let segElem = segList.querySelector(`[data-id="${id}"]`);
    let nameElem = segElem.querySelector('[name="name"]');
    nameElem.textContent = this.value;
    this.value = "";
}

async function onSegBriChange() {
    let data = { "bri": parseInt(this.value) };
    let response = await fetch(`/v1/segments?id=${this.dataset.id}`,
                               put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`set bri ${this.dataset.id}: status=${response.status} ` +
                    `body=${response.body}`);
}

async function onSegApplyClick() {
    let id = this.dataset.id;
    let segElem = segList.querySelector(`[data-id="${id}"]`);
    let startElem = segElem.querySelector('[name="start"]');
    let lenElem = segElem.querySelector('[name="length"]');
    let offElem = segElem.querySelector('[name="offset"]');
    let grpElem = segElem.querySelector('[name="group"]');
    let spcElem = segElem.querySelector('[name="space"]');

    let data = {
        "start": parseInt(startElem.value),
        "len": parseInt(lenElem.value),
        "off": parseInt(offElem.value),
        "grp": parseInt(grpElem.value),
        "spc": parseInt(spcElem.value),
    };
    let response = await fetch(`/v1/segments?id=${id}`, put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`apply seg ${this.dataset.id}: status=${response.status} ` +
                    `body=${response.body}`);
}

async function onSegDeleteClick() {
    let id = this.dataset.id;
    let response = await fetch(`/v1/segments?id=${id}`, { method: "DELETE" });
    /* XXX error handling */
    if (!response.ok) {
        console.log(`delete seg ${id}: status=${response.status} ` +
                    `body=${response.body}`);
        return;
    }
    /*
     * Set display:none to hide the element immediately.
     * updateSegments() will remove it from the DOM.
     */
    this.style.display = "none";
    if (id < state.unusedSegId) {
        state.unusedSegId = id;
    }
    await updateSegments();
}

function segHilite(segElem) {
    let seg_hilited = segList.querySelector(".hilite");
    if (seg_hilited !== null) {
        seg_hilited.classList.remove("hilite");
    }
    segElem.classList.add("hilite");

    sxElem.value = segElem.dataset.sx;
    ixElem.value = segElem.dataset.ix;
    updateOutput(sxElem);
    updateOutput(ixElem);

    let hilited = fxElem.querySelector(".hilite");
    let to_hilite = fxElem.querySelector(`[data-id="${segElem.dataset.fx}"]`);
    if (hilited !== null) {
        hilited.classList.remove("hilite");
    }
    if (to_hilite === null) {
        console.log(`fx for segment ${segElem.dataset.id} not found`);
        return;
    }
    to_hilite.classList.add("hilite");

    /* hilite palette */
    hilited = colorElem.querySelector(".hilite");
    to_hilite = colorElem.querySelector(`[data-id="${segElem.dataset.pal}"]`);
    if (hilited !== null) {
        hilited.classList.remove("hilite");
    }
    if (to_hilite === null) {
        console.log(`pal for segment ${segElem.dataset.id} not found`);
        return;
    }
    to_hilite.classList.add("hilite");

    for (let c of pickerElems) {
        c.value = segElem.dataset["color_" + c.id];
    }
}

function onSegDivClick() {
    segHilite(this);
};

async function updateSegments() {
    let response = await fetch("/v1/segments", { headers: HDRS_ACCEPT_JSON });
    /* XXX error handling */
    if (!response.ok)
        throw new Error("Fetch segments: status=" + response.status +
                        " body=" + response.body);
    let segs = await response.json();

    let segWasHilited = false;
    for (let id = 0; id <= MAX_SEGMENT_ID; id++) {
        let segElem = segList.querySelector(`[data-id="${id}"]`);
        let segArray = segs.filter(seg => seg.id == id);
        if (segArray.length > 1) {
            console.log(`${segArray.length} segments with id ${id}`);
            return;
        }
        if (segArray.length === 0) {
            if (id === 0) {
                console.log("Segment ID 0 not returned from segments fetch");
                return;
            }
            if (segElem !== null) {
                segElem.remove();
            }
            continue;
        }
        let seg = segArray[0];
        if (segElem === null) {
            let seg0Elem = segList.querySelector(`[data-id="0"]`);
            if (seg0Elem === null) {
                console.log("Segment element id=0 not found in DOM");
                return;
            }
            segElem = seg0Elem.cloneNode(true);
            segElem.dataset.id = id;
            segList.append(segElem);
        }

        let hilite = false;
        /* Highlight the first selected segment. */
        if (!segWasHilited && seg.sel) {
            hilite = true;
            segWasHilited = true;
            segElem.classList.add("hilite");
        }
        if (!hilite) {
            segElem.classList.remove("hilite");
        }

        segElem.dataset.fx = seg.fx;
        segElem.dataset.pal = seg.pal;
        segElem.dataset.sx = seg.sx;
        segElem.dataset.ix = seg.ix;
        segElem.addEventListener("click", onSegDivClick);

        let selectElem = segElem.querySelector('[name="select"]');
        selectElem.checked = seg.sel;
        selectElem.dataset.id = seg.id;
        selectElem.dataset.type = "sel";
        selectElem.addEventListener("click", onSegCheckboxClick);

        let briElem = segElem.querySelector('[name="bright"]');
        briElem.value = seg.bri;
        briElem.dataset.id = seg.id;
        briElem.addEventListener("change", onSegBriChange);
        briElem.addEventListener("input", onSliderInput);
        updateOutput(briElem);

        let nameElem = segElem.querySelector('[name="name"]');
        nameElem.textContent = seg.name;

        let newNameElem = segElem.querySelector('[name="new-name"]');
        newNameElem.dataset.id = seg.id;
        newNameElem.addEventListener("change", onSegNameChange);

        let startElem = segElem.querySelector('[name="start"]');
        startElem.value = seg.start;

        let lenElem = segElem.querySelector('[name="length"]');
        lenElem.value = seg.len;

        let offElem = segElem.querySelector('[name="offset"]');
        offElem.value = seg.off;

        let grpElem = segElem.querySelector('[name="group"]');
        grpElem.value = seg.grp;

        let spcElem = segElem.querySelector('[name="space"]');
        spcElem.value = seg.spc;

        let applyElem = segElem.querySelector('[name="apply"]');
        applyElem.dataset.id = seg.id;
        applyElem.addEventListener("click", onSegApplyClick);

        let revElem = segElem.querySelector('[name="reverse"]');
        revElem.checked = seg.rev;
        revElem.dataset.id = seg.id;
        revElem.dataset.type = "rev";
        revElem.addEventListener("click", onSegCheckboxClick);

        let miElem = segElem.querySelector('[name="mirror"]');
        miElem.checked = seg.mi;
        miElem.dataset.id = seg.id;
        miElem.dataset.type = "mi";
        miElem.addEventListener("click", onSegCheckboxClick);

        let deleteElem = segElem.querySelector('[name="delete"]');
        if (id === 0) {
            deleteElem.style.display = "none";
        }
        else {
            deleteElem.style.display = "inline";
        }
        deleteElem.dataset.id = seg.id;
        deleteElem.addEventListener("click", onSegDeleteClick);

        for (let c of pickerElems) {
            let rgbw = seg.color[c.id];
            let rgb = [
                clamp(rgbw.r + rgbw.w, 0, 255),
                clamp(rgbw.g + rgbw.w, 0, 255),
                clamp(rgbw.b + rgbw.w, 0, 255),
            ]
            let value = rgb2hex(inv_gamma(rgb));
            segElem.dataset["color_" + c.id] = value;
        }

        if (hilite) {
            segHilite(segElem);
            hilite = false;
        }

        if (seg.id == state.unusedSegId) {
            state.unusedSegId++;
            /* XXX disable new-seg if max segments is reached. */
        }
    }
}

async function onNewSegClick() {
    let id = state.unusedSegId;
    let data = {
        "name": newSegNameElem.value,
        "start": parseInt(newSegStartElem.value),
        "len": parseInt(newSegLenElem.value),
        "bri": 128,
        "sx": 128,
        "ix": 128,
        "color": {
            "pri":  { "r": 255 },
            "sec":  { "g": 255 },
            "tert": { "b": 255 },
        },
    };
    let response = await fetch(`/v1/segments?id=${id}`, post_opts(data));
    /* XXX error handling */
    if (!response.ok) {
        console.log(`new segment ${id}: status=${response.status} ` +
                    `body=${response.body}`);
        return;
    }

    /* XXX check if max segment ID is reached. */
    state.unusedSegId++;
    await updateSegments();

    let seg_to_hilite = segList.querySelector(`[data-id="${id}"]`);
    segHilite(seg_to_hilite);

    newSegNameElem.value = "";
    /* XXX set next start LED to the next highest */
    newSegStartElem.value = "0";
    newSegLenElem.value = "1";
}

/*
 * Presets
 */

const psList = document.querySelector('#ps-list');
const psQlElem = document.querySelector('#ps-ql');
const psQlList = document.querySelector('#ps-ql-list');
const psTemplate = document.querySelector('#ps-template');
const qlBtnTemplate = document.querySelector('#ql-template');

const newPsTitleElem = document.querySelector("#new-ps-title");
const createPsBtnElem = document.querySelector("#create-ps-btn");
const newPsBodyElem = document.querySelector("#new-ps-body");
const newPsSaveBtnElem = document.querySelector("#new-ps-save");
const newPsCancelBtnElem = document.querySelector("#new-ps-cancel");
const newPsNameElem = document.querySelector("#new-ps-name");
const newPsQlElem = document.querySelector("#new-ps-ql");
const newPsBriElem = document.querySelector("#new-ps-bri");
const newPsSbElem = document.querySelector("#new-ps-sb");
const newPsIdElem = document.querySelector("#new-ps-id");

function getPsElements(psElem) {
    return {
	'newName': psElem.querySelector('[name="ps-new-name"]'),
	'ql': psElem.querySelector('[name="ps-ql"]'),
	'bri': psElem.querySelector('[name="ps-bri"]'),
	'sb': psElem.querySelector('[name="ps-sb"]'),
    };
}

async function savePs(id, opts) {
    let response = await fetch(`/v1/presets?id=${id}`, opts);
    /* XXX error handling */
    if (!response.ok)
        console.log(`save ps ${id}: status=${response.status} ` +
                    `body=${response.body}`);
}

async function onPsSaveNewClick() {
    let id = parseInt(newPsIdElem.value);
    let data = {
        "name": newPsNameElem.value,
        "ql": newPsQlElem.value,
        "bri": newPsBriElem.checked,
        "sb": newPsSbElem.checked,
    };
    savePs(id, post_opts(data));
    await updatePresets();
    newPsCancelBtnElem.click();
    newPsNameElem.value = "";
    newPsQlElem.value = "";
}

function onPsSaveChangesClick() {
    let id = this.dataset.id;
    /* Parent node is the row, whose parent node is the ps element. */
    let elements = getPsElements(this.parentNode.parentNode);
    let data = {
        "name": elements["newName"].value,
        "ql": elements["ql"].value,
        "bri": elements["bri"].checked,
        "sb": elements["sb"].checked,
    };
    savePs(id, put_opts(data));
}

function onPsCollapseClick() {
    /* Parent node is the row, whose parent node is the ps element. */
    let psElem = this.parentNode.parentNode;
    let psBody = psElem.querySelector('[name="ps-body"]');
    psBody.classList.toggle("ps-collapsed");
}

async function onPsDeleteClick() {
    let id = this.dataset.id;
    let response = await fetch(`/v1/presets?id=${id}`, { method: "DELETE" });
    /* XXX error handling */
    if (!response.ok) {
        console.log(`delete ps ${id}: status=${response.status} ` +
                    `body=${response.body}`);
        return;
    }
    /*
     * Set display:none to hide the element immediately.
     * updatePresets() will remove it from the DOM.
     */
    this.style.display = "none";
    if (id < state.unusedPsId) {
        state.unusedPsId = id;
    }
    await updatePresets();
}

async function setPreset() {
    /* XXX hiliting? */
    let data = {}
    data['ps'] = parseInt(this.dataset.id);
    let response = await fetch('/v1/global', put_opts(data));
    /* XXX error handling */
    if (!response.ok)
        console.log(`set ps ${this.id}: status=${response.status} ` +
                    `body=${response.body}`);
    await updateSegments();
}

async function updatePresets() {
    let response = await fetch("/v1/presets", { headers: HDRS_ACCEPT_JSON });
    /* XXX error handling */
    if (!response.ok)
        throw new Error("Fetch presets: status=" + response.status +
                        " body=" + response.body);
    let presets = await response.json();

    state.unusedPsId = 0;
    qlsExist = false;
    for (let id = 0; id <= MAX_SEGMENT_ID; id++) {
        let psElem = psList.querySelector(`[data-id="${id}"]`);
        let qlBtnElem = psQlList.querySelector(`[data-id="${id}"]`);
        let psArray = presets.filter(ps => ps.id == id);
        if (psArray.length > 1) {
            console.log(`${psArray.length} segments with id ${id}`);
            return;
        }
        if (psArray.length === 0) {
            if (psElem !== null) {
                psElem.remove();
            }
            if (qlBtnElem !== null) {
                qlBtnElem.remove();
            }
            continue;
        }
        if (psElem === null) {
            psElem = psTemplate.cloneNode(true);
            psElem.dataset.id = id;
            psElem.setAttribute("id", `ps-${id}`);
            psList.append(psElem);
        }

        let ps = psArray[0];
	if (qlBtnElem !== null) {
	    qlsExist = true;
	}
        else if (ps.ql.trim().length !== 0) {
            qlBtnElem = qlBtnTemplate.cloneNode(true);
            qlBtnElem.dataset.id = id;
            qlBtnElem.textContent = ps.ql;
            psQlList.append(qlBtnElem);
            qlBtnElem.setAttribute("id", `ps-ql-${id}`);
            qlBtnElem.classList.remove("ql-template");
            qlBtnElem.addEventListener("click", setPreset);
	    qlsExist = true;
        }

        let nameElem = psElem.querySelector('[name="ps-name"]');
        nameElem.textContent = ps.name;
        nameElem.dataset.id = id;
        nameElem.addEventListener("click", setPreset);

	let btnIdElem = psElem.querySelector('[name="ps-btn-id"]');
	btnIdElem.textContent = ps.id;

	let collapseBtnElem = psElem.querySelector('[name="ps-collapse-btn"]');
	collapseBtnElem.addEventListener("click", onPsCollapseClick);

        let elements = getPsElements(psElem);
        elements["newName"].value = ps.name;
        elements["ql"].value = ps.ql;
        elements["bri"].checked = ps.bri;
        elements["sb"].checked = ps.sb;

        let idElem = psElem.querySelector('[name="ps-id"]');
        idElem.value = ps.id;

        let saveElem = psElem.querySelector('[name="ps-save"]');
        saveElem.dataset.id = id;
        saveElem.addEventListener("click", onPsSaveChangesClick);

        let deleteElem = psElem.querySelector('[name="ps-delete"]');
        deleteElem.dataset.id = id;
        deleteElem.addEventListener("click", onPsDeleteClick);

        if (id == state.unusedPsId) {
            state.unusedPsId++;
        }

        psElem.classList.remove("ps-template");
    }
    /* XXX sort alphabetically */

    if (state.unusedPsId <= MAX_PRESET_ID) {
	newPsIdElem.value = state.unusedPsId;
	newPsSaveBtnElem.disabled = false;
    }
    else {
	newPsSaveBtnElem.disabled = true;
    }
    if (qlsExist) {
	psQlElem.classList.remove("ps-collapsed");
    }
    else {
	psQlElem.classList.add("ps-collapsed");
    }
}

function onCreatePsClick() {
    newPsBodyElem.classList.remove("ps-collapsed");
    createPsBtnElem.classList.add("ps-collapsed");
}

function onNewPsCancelClick() {
    newPsBodyElem.classList.add("ps-collapsed");
    createPsBtnElem.classList.remove("ps-collapsed");
}

/*
 * Update global state
 */

async function updateGlobal() {
    let response = await fetch("/v1/global", { headers: HDRS_ACCEPT_JSON });
    /* XXX error handling */
    if (!response.ok)
        throw new Error("Fetch global: status=" + response.status +
                        " body=" + response.body);
    let data = await response.json();
    state.on = data.on;
    state.nleds = data.nleds;

    briElem.value = data.bri;
    updateOutput(briElem);
    fpsElem.value = data.fps;
    updateOutput(fpsElem);
}

/*
 * Init
 */
async function init() {
    await getFx();
    await getPalettes();
    await updatePresets();
    await updateGlobal();
    await updateSegments();
    pwrElem.addEventListener("click", onPowerBtnClick);
    sxElem.addEventListener("change", onFxSliderChange);
    ixElem.addEventListener("change", onFxSliderChange);
    sxElem.addEventListener("input", onSliderInput);
    ixElem.addEventListener("input", onSliderInput);
    for (const pickerElem of pickerElems) {
        pickerElem.addEventListener("change", onColorSelect);
    }
    for (const globalElem of globalElems) {
        globalElem.addEventListener("change", onGlobalSliderChange);
        globalElem.addEventListener("input", onSliderInput);
    }
    newSegBtnElem.addEventListener("click", onNewSegClick);
    newPsSaveBtnElem.addEventListener("click", onPsSaveNewClick);
    createPsBtnElem.addEventListener("click", onCreatePsClick);
    newPsCancelBtnElem.addEventListener("click", onNewPsCancelClick);
}

if (document.readyState !== "loading") {
    init();
} else {
    document.addEventListener("DOMContentLoaded", init);
}
